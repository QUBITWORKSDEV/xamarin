package md557d90ab63056cf296b1295756553c356;


public class HelloFacebookBroadcastReceiver
	extends com.facebook.FacebookBroadcastReceiver
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onSuccessfulAppCall:(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V:GetOnSuccessfulAppCall_Ljava_lang_String_Ljava_lang_String_Landroid_os_Bundle_Handler\n" +
			"n_onFailedAppCall:(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V:GetOnFailedAppCall_Ljava_lang_String_Ljava_lang_String_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("HelloFacebookSample.HelloFacebookBroadcastReceiver, HelloFacebookSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", HelloFacebookBroadcastReceiver.class, __md_methods);
	}


	public HelloFacebookBroadcastReceiver () throws java.lang.Throwable
	{
		super ();
		if (getClass () == HelloFacebookBroadcastReceiver.class)
			mono.android.TypeManager.Activate ("HelloFacebookSample.HelloFacebookBroadcastReceiver, HelloFacebookSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onSuccessfulAppCall (java.lang.String p0, java.lang.String p1, android.os.Bundle p2)
	{
		n_onSuccessfulAppCall (p0, p1, p2);
	}

	private native void n_onSuccessfulAppCall (java.lang.String p0, java.lang.String p1, android.os.Bundle p2);


	public void onFailedAppCall (java.lang.String p0, java.lang.String p1, android.os.Bundle p2)
	{
		n_onFailedAppCall (p0, p1, p2);
	}

	private native void n_onFailedAppCall (java.lang.String p0, java.lang.String p1, android.os.Bundle p2);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
