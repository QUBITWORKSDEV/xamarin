﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Xamarin.Facebook;
using Plugin.Permissions;
using Android.Support.V7.App;
using Xamarin.Forms.Platform.Android;
using Android.Views;

[assembly: MetaData("com.facebook.sdk.ApplicationId", Value = "@string/app_id")]
namespace Veepe.Droid
{
	[Activity(
		Label = "Veepe",
		Icon = "@drawable/icon", 
		MainLauncher = true, 
		Theme= "@style/VeepeTheme", 
		//LaunchMode = LaunchMode.SingleInstance,
		ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
		WindowSoftInputMode = SoftInput.AdjustPan
	)]
	public class MainActivity : FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.Window.RequestFeature(WindowFeatures.ActionBar);
			//base.SetTheme(global::Android.Resource.Style.ThemeMaterial);
			base.OnCreate (bundle);
			base.ActionBar.Hide();

			// Inicializamos Forms
			global::Xamarin.Forms.Forms.Init (this, bundle);

			// Inicializamos el mapa
			Xamarin.FormsMaps.Init (this, bundle);

			// Inicializamos el sdk de FB
			FacebookSdk.SdkInitialize( this.ApplicationContext );

			// Inicializamos el lector de qr
			global::ZXing.Net.Mobile.Forms.Android.Platform.Init();

			LoadApplication (new App());

			//base.ActionBar.Show();
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			FBLoginButtonRenderer.OnActivityResult (requestCode, resultCode, data);
		}

		public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
		}

		public override void OnBackPressed()
		{
			return;		// evitamos que se use el botón de back
			//base.OnBackPressed();
		}
	}
}

