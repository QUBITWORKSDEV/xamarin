﻿using FunkFramework.FB;
using Veepe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(CrossFBManagerDroid))]
namespace Veepe.Droid
{
	public class CrossFBManagerDroid : CrossFBManager
	{
		public void LogOut()
		{
			Xamarin.Facebook.Login.LoginManager.Instance.LogOut();
		}
	}
}
