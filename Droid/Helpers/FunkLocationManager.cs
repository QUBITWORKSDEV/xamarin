﻿using Android.Locations;
using FunkFramework.Location;
using Veepe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(FunkLocationManager))]
namespace Veepe.Droid
{
	public class FunkLocationManager : CrossLocationManager
	{
		public double[] getLocation()
		{
			Location l = MainApplication.locMgr.GetLastKnownLocation(LocationManager.GpsProvider);
			if ( l == null )
				return new double[] { 19.4326106, -99.1331899 };
			else
				return new double[] { l.Latitude, l.Longitude };
		}

	}
}
