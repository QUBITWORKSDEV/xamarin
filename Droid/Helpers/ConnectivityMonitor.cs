﻿using Android.Net;
using Veepe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ConnectivityMonitor))]
namespace Veepe.Droid
{
	public class ConnectivityMonitor : IConnectivityMonitor
	{
		ConnectivityManager cm;
		NetworkInfo activeConnection;

		public ConnectivityMonitor()
		{
		}

		public bool HasConnectivity()
		{
			if ( cm == null )
				cm = (ConnectivityManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.ConnectivityService);

			activeConnection = cm.ActiveNetworkInfo;

			return activeConnection != null && activeConnection.IsConnected;
		}
	}
}
