﻿using Android.Media;
using FunkFramework.Media;
using Veepe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(FunkAudio))]
namespace Veepe.Droid
{
	public class FunkAudio : IFunkAudio
	{
		public FunkAudio() { }

		private MediaPlayer _mediaPlayer;

		public bool PlayAudioFile(string url)
		{
			//int resourceid = global::Android.App.Application.Context.Resources.GetIdentifier(url, "sounds", global::Android.App.Application.Context.PackageName);
			//_mediaPlayer = MediaPlayer.Create(global::Android.App.Application.Context, resourceid);

			//int resourceid = global::Android.App.Application.Context.Resources.GetIdentifier(url, "sounds", global::Android.App.Application.Context.PackageName);
			_mediaPlayer = MediaPlayer.Create(global::Android.App.Application.Context, Resource.Raw.capisci);
			_mediaPlayer.Start();

			return true;
		}
	}
}
