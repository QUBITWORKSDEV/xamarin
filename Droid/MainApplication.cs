using System;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Plugin.CurrentActivity;
using PushNotification.Plugin;

namespace Veepe.Droid
{
	//You can specify additional application information in this attribute
	[Application]
	public class MainApplication : Application, Application.IActivityLifecycleCallbacks
	{
		public static Context AppContext;
		public static LocationManager locMgr;

		public MainApplication(IntPtr handle, JniHandleOwnership transer) :base(handle, transer)
		{
		}

		#region Push Configurations
		public static void StartPushService()
		{
			AppContext.StartService(new Intent(AppContext, typeof(PushNotificationService)));
			if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat)
			{
				PendingIntent pintent = PendingIntent.GetService(AppContext, 0, new Intent(AppContext, typeof(PushNotificationService)), 0);
				AlarmManager alarm = (AlarmManager)AppContext.GetSystemService(Context.AlarmService);
				alarm.Cancel(pintent);
			}
		}

		public static void StopPushService()
		{
			AppContext.StopService(new Intent(AppContext, typeof(PushNotificationService)));
			if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Kitkat)
			{
				PendingIntent pintent = PendingIntent.GetService(AppContext, 0, new Intent(AppContext, typeof(PushNotificationService)), 0);
				AlarmManager alarm = (AlarmManager)AppContext.GetSystemService(Context.AlarmService);
				alarm.Cancel(pintent);
			}
		}
		#endregion

		public override void OnCreate()
		{
			base.OnCreate();

			RegisterActivityLifecycleCallbacks(this);
			AppContext = this.ApplicationContext;

			//A great place to initialize Xamarin.Insights and Dependency Services
			CrossPushNotification.Initialize<VeepePushNotificationListener>("242871661001");
			StartPushService();
		}

		public override void OnTerminate()
		{
			base.OnTerminate();
			UnregisterActivityLifecycleCallbacks(this);
		}

		public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
		{
			CrossCurrentActivity.Current.Activity = activity;
		}

		public void OnActivityDestroyed(Activity activity)
		{
		}

		public void OnActivityPaused(Activity activity)
		{
		}

		public void OnActivityResumed(Activity activity)
		{
			CrossCurrentActivity.Current.Activity = activity;
			locMgr = GetSystemService(Context.LocationService+"") as LocationManager;
			NotificationManager notificationManager = GetSystemService (Context.NotificationService) as NotificationManager;
			//notificationManager.CancelAll();
		}

		public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
		{
		}

		public void OnActivityStarted(Activity activity)
		{
			CrossCurrentActivity.Current.Activity = activity;
		}

		public void OnActivityStopped(Activity activity)
		{
		}
	}
}