using Veepe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Graphics.Drawables;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkLabel), typeof(FunkLabelRenderer))]
namespace Veepe.Droid
{
	public class FunkLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);

			var label = (Android.Widget.TextView)Control;

			// configuramos la fuente
			if ( this.Element.FontFamily == "OpenSans-Light" )
			{
				label.Typeface = Typeface.CreateFromAsset (Forms.Context.Assets, "Fonts/OpenSans-Light.ttf");
			}
			else if ( this.Element.FontFamily == "BebasNeue" )
			{
				label.Typeface = Typeface.CreateFromAsset (Forms.Context.Assets, "Fonts/BebasNeue.otf");
				if (((int)Android.OS.Build.VERSION.SdkInt) >= 20)
				{
					label.LetterSpacing = 0.13f;
				}
			}
			else
			{
				label.Typeface = Typeface.CreateFromAsset (Forms.Context.Assets, "Fonts/OpenSans-Regular.ttf");
			}
		}

	}
}

