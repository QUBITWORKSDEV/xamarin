﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Widget;
using Veepe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Platform.Android;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkMap), typeof(FunkMapRenderer))]
namespace Veepe.Droid
{
	public class FunkMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter, IOnMapReadyCallback
	{
		GoogleMap map;
		List<CustomPin> customPins;
		bool isDrawn;

		protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				//map.InfoWindowClick -= OnInfoWindowClick; ***
			}

			if (e.NewElement != null)
			{
				var formsMap = (FunkMap)e.NewElement;
				customPins = formsMap.CustomPins;
				((MapView)Control).GetMapAsync(this);
			}
		}

		public void OnMapReady(GoogleMap googleMap)
		{
			map = googleMap;
			//map.InfoWindowClick += OnInfoWindowClick; ***
			map.MarkerClick += Map_MarkerClick;
			map.SetInfoWindowAdapter(this);
			map.Clear();

			if (customPins == null)
				customPins = ((FunkMap)Element).CustomPins;

			if (customPins != null)
			{
				foreach (var pin in customPins)
				{
					var marker = new MarkerOptions();
					marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
					marker.SetTitle(pin.Pin.Label);
					marker.SetSnippet(pin.Pin.Address);
					marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.pin));

					map.AddMarker(marker);
				}
				isDrawn = true;
			}
			// ______________________________________________________
		}

		void Map_MarkerClick(object sender, GoogleMap.MarkerClickEventArgs e)
		{
			var customPin = GetCustomPin(e.Marker);
			if (customPin != null)
			{
				customPin.PerformClick(sender, customPin);
			}
			else
			{
				//throw new Exception("Custom pin not found");
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName.Equals("VisibleRegion") && !isDrawn)
				if (map!=null)
				{
					map.Clear();
					if (customPins == null)
						customPins = ((FunkMap)Element).CustomPins;

					if (customPins != null)
					{
						foreach (var pin in customPins)
						{
							var marker = new MarkerOptions();
							marker.SetPosition(new LatLng(pin.Pin.Position.Latitude, pin.Pin.Position.Longitude));
							marker.SetTitle(pin.Pin.Label);
							marker.SetSnippet(pin.Pin.Address);
							marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.pin));

							map.AddMarker(marker);
						}
						isDrawn = true;
					}
				}
		}

		protected override void OnLayout(bool changed, int l, int t, int r, int b)
		{
			base.OnLayout(changed, l, t, r, b);

			if (changed)
				isDrawn = false;
		}

		void OnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			/*var customPin = GetCustomPin(e.Marker);
			if (customPin == null)
			{
				throw new Exception("Custom pin not found");
			}

			customPin.PerformClick(sender, customPin);*/
		}

		CustomPin GetCustomPin(Marker annotation)
		{
			var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);
			if (customPins == null)
				customPins = ((FunkMap)Element).CustomPins;
			foreach (var pin in customPins)
			{
				if (pin.Pin.Position == position)
					return pin;
			}
			return null;
		}

		public Android.Views.View GetInfoContents(Marker marker)
		{
			marker.Flat = false;

			var inflater = Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) as Android.Views.LayoutInflater;
			if (inflater != null)
			{
				Android.Views.View view;

				var customPin = GetCustomPin(marker);
				if (customPin == null)
				{
					throw new Exception("Custom pin not found");
				}

				view = inflater.Inflate(Resource.Layout.FunkMapInfoWindow, null);

				var infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
				var infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);

				if (infoTitle != null)
				{
					infoTitle.Text = marker.Title;
				}
				if (infoSubtitle != null)
				{
					infoSubtitle.Text = marker.Snippet;
				}

				return view;
			}
			return null;
		}

		public Android.Views.View GetInfoWindow(Marker marker)
		{
			return null;
		}
	}
}
