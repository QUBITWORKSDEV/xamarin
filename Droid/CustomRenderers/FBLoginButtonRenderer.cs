﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Veepe.Droid;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Facebook.Login;
using Org.Json;
using FunkFramework.UI;
using FunkFramework.FB;

[assembly: ExportRenderer(typeof(FBLoginButton), typeof(FBLoginButtonRenderer))]
namespace Veepe.Droid
{
	public class FBLoginButtonRenderer : ButtonRenderer, GraphRequest.IGraphJSONObjectCallback
	{
		// _____________________________________________________________________________________ Atributos privados
		ICallbackManager callbackManager;
		LoginButton loginView;
		Xamarin.Facebook.GraphRequest request;

		// _____________________________________________________________________________________ Atributos públicos
		public static FBLoginButtonRenderer _instance;

		// _____________________________________________________________________________________ Métodos
		public FBLoginButtonRenderer () : base()
		{
			_instance = this;
		}

		public void OnCompleted(JSONObject p0, GraphResponse p1)
		{
			//Console.WriteLine(p0.ToString());
			//Console.WriteLine("email: " + p0.OptString("email"));

			string elId = p0.OptString("id");
			string[] aux = p0.OptString("name").Split(' ');
			string elNombre = aux[0];
			string elApellido = aux.Length > 1 ? aux[1] : "";
			string elEmail = p0.OptString("email");

			// esto dispara un evento que es configurado en la pantalla donde está el botón
			(this.Element as FBLoginButton).triggerLoginIntent(elId, elNombre, elApellido, elEmail);
		}

		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);

			if (this.Control != null)
			{
				callbackManager = CallbackManagerFactory.Create();
				var loginCallBack = new FacebookCallback<LoginResult> {
					HandleSuccess = loginResult =>
					{
						Console.WriteLine("FB Login Succedd: " + loginResult.ToString());

						// 2do: obtener los datos del usuario
						request = GraphRequest.NewMeRequest(AccessToken.CurrentAccessToken, this);
						Bundle parameters = new Bundle();
						parameters.PutString("fields", "id,email,name");
						request.Parameters = parameters;
						request.ExecuteAsync();
					},
					HandleCancel = () =>
					{
						Control.Enabled = true;
						(this.Element as FBLoginButton).triggerLoginIntent_Cancel();
					},
					HandleError = loginError =>
					{
						Control.Enabled = true;
						(this.Element as FBLoginButton).triggerLoginIntent_Error();

						Console.WriteLine("FB Login Error: " + loginError.ToString());
					}
				};
				LoginManager.Instance.RegisterCallback (callbackManager, loginCallBack);

				loginView = new Xamarin.Facebook.Login.Widget.LoginButton(Forms.Context);
				loginView.SetReadPermissions(new List<string> { "public_profile", "email" });
				loginView.LoginBehavior = Xamarin.Facebook.Login.LoginBehavior.WebOnly; // NativeWithFallback;
				//SetNativeControl (loginView);

				global::Android.Widget.Button button = this.Control;
				button.Click += Button_Click;
			}
		}

		public static void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			_instance.callbackManager.OnActivityResult (requestCode, (int)resultCode, data);
		}

		void Button_Click (object sender, EventArgs e)
		{
			Page p = ((App.Current as App).MainPage as NavigationPage).CurrentPage;

			string t = p.GetType().ToString();
			if (t == "Veepe.P_LoginInicio")
			{
				if ((p as P_LoginInicio).canEdit)
				{
					(p as P_LoginInicio).canEdit = false;

					// primero tratamos de desloggear
					try
					{
						CrossFBManagerDroid cfbm = new CrossFBManagerDroid();
						cfbm.LogOut();
					}
					finally
					{
						(this.Element as FBLoginButton).triggerCustomClick();
						loginView.PerformClick();
					}

				}
			}
		}
	} // fin de la clase

	class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
	{
		public Action HandleCancel { get; set; }
		public Action<FacebookException> HandleError { get; set; }
		public Action<TResult> HandleSuccess { get; set; }

		public void OnCancel()
		{
			var c = HandleCancel;
			if (c != null)
				c();
		}

		public void OnError(FacebookException error)
		{
			var c = HandleError;
			if (c != null)
				c(error);
		}

		public void OnSuccess(Java.Lang.Object result)
		{
			System.Action<TResult> c = HandleSuccess;
			if (c != null)
				c(result.JavaCast<TResult>());
		}
	}

} // fin del namespace

