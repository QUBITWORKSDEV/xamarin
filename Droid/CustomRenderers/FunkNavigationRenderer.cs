﻿using Android.App;
using Android.Graphics.Drawables;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Veepe.Droid;
using Android.Text;
using Android.Text.Style;
using Android.Util;
using Android.Graphics;
using FunkFramework;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(FunkNavigationRenderer))]
namespace Veepe.Droid
{
	public class CustomFontSpan : MetricAffectingSpan
	{
		static LruCache typefaceCache = new LruCache(5);

		Typeface typeFace;

		public CustomFontSpan(string typefaceName)
		{
			typeFace = (Typeface)typefaceCache.Get(typefaceName);

			if (typeFace == null)
			{
				typeFace = Typeface.CreateFromAsset(Forms.Context.ApplicationContext.Assets, typefaceName);
				typefaceCache.Put(typefaceName, typeFace);
			}
		}

		public override void UpdateMeasureState(TextPaint p)
		{
			p.SetTypeface(typeFace);
			p.Flags = p.Flags | PaintFlags.SubpixelText;
		}

		public override void UpdateDrawState(TextPaint tp)
		{
			tp.SetTypeface(typeFace);
			tp.Flags = tp.Flags | PaintFlags.SubpixelText;
		}
	}


	public class FunkNavigationRenderer : NavigationRenderer
	{
		protected override void OnLayout(bool changed, int l, int t, int r, int b)
		{
			base.OnLayout(changed, l, t, r, b);

			repurposeActionBar();
		}

		protected override void OnElementChanged(ElementChangedEventArgs<NavigationPage> e)
		{
			base.OnElementChanged (e);

			repurposeActionBar ();
		}

		protected override int[] OnCreateDrawableState(int extraSpace)
		{
			repurposeActionBar();
			return base.OnCreateDrawableState(extraSpace);
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			repurposeActionBar();
		}

		void repurposeActionBar()
		{
			// http://stackoverflow.com/questions/14606294/remove-icon-logo-from-action-bar-on-android
			var actionBar = ((Activity)Context).ActionBar;

			GradientDrawable gd = new GradientDrawable();
			//gd.SetColor( Xamarin.Forms.Color.FromHex("#090A19").ToAndroid() );
			//gd.SetColor(Xamarin.Forms.Color.FromHex("#414042").ToAndroid());

			if (GlobalSettings.isDevelop)
			{
				Xamarin.Forms.Color c = Xamarin.Forms.Color.FromHex("#9B7E3D");
				gd.SetColor(c.ToAndroid());
				actionBar.SetBackgroundDrawable(gd);
			}
			// esto estaba cuando no usábamos temas
			/*else
				gd.SetColor((Xamarin.Forms.Application.Current as App).navbarColor.ToAndroid());
			actionBar.SetBackgroundDrawable(gd);*/

			if (((int)Android.OS.Build.VERSION.SdkInt) >= 21)
			{
				actionBar.Elevation = 0;
			}
			actionBar.SetIcon(new ColorDrawable(Xamarin.Forms.Color.Transparent.ToAndroid()));

			if (actionBar.Title != null)
			{
				var st = new SpannableString(actionBar.Title);
				st.SetSpan(new CustomFontSpan("Fonts/BebasNeue.otf"), 0, st.Length(), SpanTypes.ExclusiveExclusive);
				actionBar.TitleFormatted = st;
			}
		}
	}
}
