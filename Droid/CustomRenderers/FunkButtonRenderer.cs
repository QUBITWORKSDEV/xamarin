﻿using Xamarin.Forms;
using Veepe.Droid;
using FunkFramework.UI;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;

[assembly: ExportRenderer (typeof(FunkButton), typeof(FunkButtonRenderer))]
namespace Veepe.Droid
{
	public class FunkButtonRenderer : ButtonRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);
			var label = (Android.Widget.Button)Control;
			Typeface font = Typeface.CreateFromAsset (Forms.Context.Assets, "Fonts/OpenSans-Regular.ttf");
			label.Typeface = font;
			label.SetAllCaps (false);
		}
	}
}

