﻿using Veepe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Graphics.Drawables;
using Android.Animation;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkActivityIndicator), typeof(FunkActivityIndicatorRenderer))]
namespace Veepe.Droid
{
	public class FunkActivityIndicatorRenderer : ImageRenderer
	{
		AnimationDrawable anim;

		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.SetBackgroundResource( Resource.Drawable.activity_anim );
				if (e.NewElement != null)
				{
					//if ((e.NewElement as Image).Animate)
					//{
						(Control.Background as AnimationDrawable)?.Start();
						//Control.ImageAlpha = 0;
					//}
				}
			}
		}

	}
}

