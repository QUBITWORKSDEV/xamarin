﻿using Veepe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Graphics.Drawables;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkSearch), typeof(FunkSearchRenderer))]
namespace Veepe.Droid
{
	public class FunkSearchRenderer : EntryRenderer
	{
		Xamarin.Forms.Color colorOriginal;

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			var entry = (Android.Widget.TextView)Control;
			colorOriginal = this.Element.BackgroundColor;
			e.NewElement.BackgroundColor = Xamarin.Forms.Color.Transparent;

			// fondo
			drawBackground(colorOriginal);

			// configuramos la fuente
			if (this.Element.FontFamily == "OpenSans-Light")
			{
				entry.Typeface = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/OpenSans-Light.ttf");
			}
			else if (this.Element.FontFamily == "BebasNeue")
			{
				entry.Typeface = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/BebasNeue.otf");
				entry.LetterSpacing = 0.13f;
			}
			else
			{
				entry.Typeface = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/OpenSans-Regular.ttf");
			}

			entry.SetTextColor(this.Element.TextColor.ToAndroid());
			entry.SetHintTextColor(Xamarin.Forms.Color.FromHex("#a3a6a4").ToAndroid());

			entry.SetPadding(0, 2, 0, 0);
			entry.Gravity = Android.Views.GravityFlags.Center;

		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			drawBackground(colorOriginal);
		}

		private void drawBackground(Xamarin.Forms.Color elColor)
		{
			GradientDrawable gd = new GradientDrawable();
			gd.SetColor(elColor.ToAndroid());
			gd.SetCornerRadius(0);

			FunkSearch o = (this.Element as FunkSearch);
			if (!o.IsValid && !o.IsEmpty)
				gd.SetStroke(1, Xamarin.Forms.Color.FromHex("#D0021B").ToAndroid());
			else
				gd.SetStroke(0, Xamarin.Forms.Color.FromHex("#2C2A2D").ToAndroid());

			((Android.Widget.TextView)Control).SetBackground(gd);
		}
	}
}
