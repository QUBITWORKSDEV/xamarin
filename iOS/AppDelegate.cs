﻿using System;
using Veepe;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using UIKit;
using PushNotification.Plugin;
using FunkFramework;

namespace Veepe.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		//const string TAG = "PushNotification-APN";
		string appId = "198067323928185";
		string appName = "Veepe";

		public override bool FinishedLaunching (UIApplication uiApplication, NSDictionary launchOptions)
		{
			// Ask the user for permission to get notifications on iOS 8.0+
			/*if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				var settings = UIUserNotificationSettings.GetSettingsForTypes(
					UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
					new NSSet());
				UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
			}*/

			global::Xamarin.Forms.Forms.Init ();
			global::ZXing.Net.Mobile.Forms.iOS.Platform.Init();
			Xamarin.FormsMaps.Init ();

			// This is false by default,
			// If you set true, you can handle the user profile info once is logged into FB with the Profile.Notifications.ObserveDidChange notification,
			// If you set false, you need to get the user Profile info by hand with a GraphRequest
			Profile.EnableUpdatesOnAccessTokenChange(true);
			Settings.AppID = appId;
			Settings.DisplayName = appName;

 			LoadApplication (new App ());
			//UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.BlackTranslucent; //(UIStatusBarStyle.BlackTranslucent, false);
			//UIApplication.SharedApplication.SetStatusBarHidden (false, false);

			//ZXing.Net.Mobile.Forms.iOS.Platform.Init();

			uiApplication.ApplicationIconBadgeNumber = 0;
			if (!UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
			{
				uiApplication.CancelAllLocalNotifications();
			}

			// Inicializamos las notificaciones
			CrossPushNotification.Initialize<VeepePushNotificationListener>();

			return base.FinishedLaunching (uiApplication, launchOptions);
		}

		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			// We need to handle URLs by passing them to their own OpenUrl in order to make the SSO authentication works.
			return ApplicationDelegate.SharedInstance.OpenUrl(application, url, sourceApplication, annotation);
		}


		#region First level listeners for push notifications
		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
			if (CrossPushNotification.Current is IPushNotificationHandler)
			{
				((IPushNotificationHandler)CrossPushNotification.Current).OnErrorReceived(error);
			}
		}

		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			if (CrossPushNotification.Current is IPushNotificationHandler)
			{
				((IPushNotificationHandler)CrossPushNotification.Current).OnRegisteredSuccess(deviceToken);
			}
		}

		public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
		{
			application.RegisterForRemoteNotifications();
		}

		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			if (CrossPushNotification.Current is IPushNotificationHandler)
			{
				((IPushNotificationHandler)CrossPushNotification.Current).OnMessageReceived(userInfo);
			}
		}

		public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
		{
			if (CrossPushNotification.Current is IPushNotificationHandler)
			{
				((IPushNotificationHandler)CrossPushNotification.Current).OnMessageReceived(userInfo);
			}
		}
		#endregion


	}
}

