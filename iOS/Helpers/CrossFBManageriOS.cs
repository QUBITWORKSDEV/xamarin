﻿using FunkFramework.FB;
using Veepe.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(CrossFBManageriOS))]
namespace Veepe.iOS
{
	public class CrossFBManageriOS : CrossFBManager
	{
		public void LogOut()
		{
			var lm = new Facebook.LoginKit.LoginManager();
			lm.LogOut();
		}
	}
}
