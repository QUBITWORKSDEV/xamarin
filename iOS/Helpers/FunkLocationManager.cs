﻿using System;
using CoreLocation;
using FunkFramework.Location;
using UIKit;
using Veepe.iOS;
using Xamarin.Forms;

[assembly:Dependency(typeof(FunkLocationManager))]
namespace Veepe.iOS
{
	public class FunkLocationManager : CrossLocationManager
	{
		protected CLLocationManager locMgr;

		public FunkLocationManager()
		{
			locMgr = new CLLocationManager();
			locMgr.PausesLocationUpdatesAutomatically = false;

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				locMgr.RequestWhenInUseAuthorization();
			}

			if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
			{
				locMgr.AllowsBackgroundLocationUpdates = true;
			}
		}

		public CLLocationManager LocMgr {
			get { return this.locMgr; }
		}

		public double[] getLocation()
		{
			double[] res = { 19.4326106, -99.1331899 };

			try {
				res = new double[] { locMgr.Location.Coordinate.Latitude, locMgr.Location.Coordinate.Longitude };
			}
			catch (Exception e)
			{
				res = new double[] { 19.4326106, -99.1331899 };
			}

			return res;
		}
	}
}
