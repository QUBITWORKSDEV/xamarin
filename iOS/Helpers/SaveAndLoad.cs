﻿using System;
using System.IO;
using Veepe.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(SaveAndLoad))]
namespace Veepe.iOS
{
	public class SaveAndLoad : ISaveAndLoad
	{
		/*public void SaveText(string filename, string text)
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var filePath = Path.Combine(documentsPath, filename);
			System.IO.File.WriteAllText(filePath, text);
		}*/

		public byte[] LoadBinary(string filename)
		{
			return File.ReadAllBytes( filename );
		}
	}

}

