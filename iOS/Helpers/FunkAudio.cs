﻿using AudioToolbox;
using Foundation;
using FunkFramework.Media;
using Veepe.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(FunkAudio))]
namespace Veepe.iOS
{
	public class FunkAudio : IFunkAudio
	{
		public FunkAudio() { }
		
		public bool PlayAudioFile(string url)
		{
			NSUrl nsurl = NSUrl.FromFilename("Sounds/" + url);
			SystemSound systemSound = new SystemSound(nsurl);
			systemSound.CompletePlaybackIfAppDies = true;
			systemSound.IsUISound = true;

			systemSound.PlayAlertSound();
		
			return true;
		}
	}
}