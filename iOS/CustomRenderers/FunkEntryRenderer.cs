﻿using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkEntry), typeof(FunkEntryRenderer))]
namespace Veepe.iOS
{
	public class FunkEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			// la fuente!
			if (Control != null)
			{
				string laFuente = e.OldElement != null ? e.OldElement.FontFamily : e.NewElement.FontFamily;

				switch (laFuente)
				{
					case "OpenSans":
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "OpenSans-Light":
						Control.Font = UIFont.FromName("OpenSans-Light", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "BebasNeue":
						Control.Font = UIFont.FromName("BebasNeue", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					default:
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;
				}
			}

			// el color de fondo
			if (Control != null)
			{
				Control.BackgroundColor = e.OldElement != null ? e.OldElement.BackgroundColor.ToUIColor() : e.NewElement.BackgroundColor.ToUIColor();

				Control.Layer.CornerRadius = 7;
			}

			/*if (Control != null)
			{
				// customizamos la fuente
				Control.Font = UIFont.FromName("OpenSans", 12f);

				// customizamos el fondo
				Control.BackgroundColor = UIColor.FromRGB(.17f, .16f, .17f);

				Control.Layer.CornerRadius = 7;
			}*/
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			Control.Layer.BorderWidth = 1;
			if (!(Element as FunkEntry).IsValid)
				Control.Layer.BorderColor = Color.FromHex("#D0021B").ToCGColor();
			else
				Control.Layer.BorderColor = Color.Transparent.ToCGColor();
		}
	}
}
