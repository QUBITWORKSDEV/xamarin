﻿using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkPicker), typeof(FunkPickerRenderer))]
namespace Veepe.iOS
{
	public class FunkPickerRenderer : PickerRenderer
	{
		public FunkPickerRenderer()
		{
			if (Control != null)
				Control.Layer.BorderWidth = 2;
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);

			var nativeTextEdit = (UITextField)Control;

			nativeTextEdit.Layer.BorderWidth = 1;
			nativeTextEdit.Layer.CornerRadius = 8;
			nativeTextEdit.Layer.BorderColor = Color.FromHex("#D6D5D6").ToCGColor();
			nativeTextEdit.TextAlignment = UITextAlignment.Center;

			Control.Font = UIFont.FromName("OpenSans-Light", 14);

			// la fuente
			/*if (Control != null && e.OldElement != null)
			{
				//string laFuente = e.OldElement != null ? e.OldElement.FontFamily : e.NewElement.FontFamily;

				switch (laFuente)
				{
					case "OpenSans":
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "OpenSans-Light":
						Control.Font = UIFont.FromName("OpenSans-Light", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "BebasNeue":
						Control.Font = UIFont.FromName("BebasNeue", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					default:
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;
				}
			}*/

			// el borde
			/*Control.Layer.BorderWidth = e.OldElement != null ? (float)e.OldElement.BorderWidth : (float)e.NewElement.BorderWidth; //2;
			Control.Layer.CornerRadius = e.OldElement != null ? (float)e.OldElement.BorderRadius : (float)e.NewElement.BorderRadius;
			Control.Layer.BorderColor = Xamarin.Forms.Color.FromHex("#00BFD6").ToCGColor();

			if (this.Element != null)
			{
				this.Element.WidthRequest = e.OldElement != null ? e.OldElement.WidthRequest : e.NewElement.WidthRequest; // 200f;
				this.Element.HeightRequest = e.OldElement != null ? e.OldElement.HeightRequest : e.NewElement.HeightRequest; //32f;
			}*/
		}

	} // fin de la clase
}



/*namespace Veepe.iOS
{
	public class FunkButtonRenderer : ButtonRenderer
	{

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

			// la fuente
			if (Control != null && e.OldElement != null)
			{
				string laFuente = e.OldElement != null ? e.OldElement.FontFamily : e.NewElement.FontFamily;

				switch (laFuente)
				{
					case "OpenSans":
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "OpenSans-Light":
						Control.Font = UIFont.FromName("OpenSans-Light", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "BebasNeue":
						Control.Font = UIFont.FromName("BebasNeue", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					default:
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;
				}
			}

			// el borde
			Control.Layer.BorderWidth = e.OldElement != null ? (float)e.OldElement.BorderWidth : (float)e.NewElement.BorderWidth; //2;
			Control.Layer.CornerRadius = e.OldElement != null ? (float)e.OldElement.BorderRadius : (float)e.NewElement.BorderRadius;
			Control.Layer.BorderColor = Xamarin.Forms.Color.FromHex("#00BFD6").ToCGColor();

			if (this.Element != null)
			{
				this.Element.WidthRequest = e.OldElement != null ? e.OldElement.WidthRequest : e.NewElement.WidthRequest; // 200f;
				this.Element.HeightRequest = e.OldElement != null ? e.OldElement.HeightRequest : e.NewElement.HeightRequest; //32f;
			}

			/*if (Control != null)
			{
				// customizamos la fuente
				UIFont font = UIFont.FromName("OpenSans", e.OldElement!=null?(float)e.OldElement.FontSize:(float)e.NewElement.FontSize);
				if (font != null)
					Control.Font = font;
			}***
		}
	}
}
*/
  