﻿using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Foundation;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkLabel), typeof(FunkLabelRenderer))]
namespace Veepe.iOS
{
	public class FunkLabelRenderer : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged (e);

			if (Control != null)
			{
				string laFuente = e.OldElement != null ? e.OldElement.FontFamily : e.NewElement.FontFamily;

				switch (laFuente)
				{
					case "OpenSans":
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "OpenSans-Light":
						Control.Font = UIFont.FromName("OpenSans-Light", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;

					case "BebasNeue":
						if (!string.IsNullOrEmpty(Control.Text))
						{
							var at = new NSMutableAttributedString(Control.Text, new UIStringAttributes() { KerningAdjustment = 2f, Font = UIFont.FromName("BebasNeue", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize) });
							Control.AttributedText = at;
						}
						break;
					
					default:
						Control.Font = UIFont.FromName("OpenSans", e.OldElement != null ? (float)e.OldElement.FontSize : (float)e.NewElement.FontSize);
						break;
				}

			}
		}

	} // fin de la clase FunLabelRendere
}

