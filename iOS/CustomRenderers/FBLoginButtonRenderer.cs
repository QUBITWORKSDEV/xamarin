﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Veepe.iOS;
using Facebook.CoreKit;
using Facebook.LoginKit;
using Foundation;
using FunkFramework;
using FunkFramework.UI;
using FunkFramework.Net;

[assembly: ExportRenderer(typeof(FBLoginButton), typeof(FBLoginButtonRenderer))]
namespace Veepe.iOS
{
	public class FBLoginButtonRenderer : ButtonRenderer
	{
		public static FBLoginButtonRenderer _instance;

		// _____________________________________________________________________________________ Atributos privados
		List<string> readPermissions = new List<string> { "public_profile","email" };
		LoginButton loginView;
		string id;
		string firstName;
		string lastName;

		public FBLoginButtonRenderer () : base()
		{
			_instance = this;

			// If was send true to Profile.EnableUpdatesOnAccessTokenChange method
			// this notification will be called after the user is logged in and
			// after the AccessToken is gotten
			Profile.Notifications.ObserveDidChange((sender, e) =>
			{
				if (e.NewProfile == null)
					return;

				id = e.NewProfile.UserID;
				firstName = e.NewProfile.FirstName;
				lastName = e.NewProfile.LastName;

				var request = new GraphRequest("/" + e.NewProfile.UserID, new NSDictionary("fields", "id,email,name"), AccessToken.CurrentAccessToken.TokenString, null, "GET");
				var requestConnection = new GraphRequestConnection();
				requestConnection.AddRequest(request, (connection, result, error) => {
					System.Diagnostics.Debug.WriteLine(result);

					// esto dispara un evento que es configurado en la pantalla donde está el botón
					string email = result.ValueForKey(new NSString("email")).ToString();

					if ((Element as FBLoginButton) != null)
						(Element as FBLoginButton).triggerLoginIntent(id, firstName, string.IsNullOrWhiteSpace(lastName) ? "" : lastName, email);
					else
					{
						// todo: el botón es nulo? qué pasó?
						System.Diagnostics.Debug.WriteLine(id + "::" + firstName + "::" + lastName);
						System.Diagnostics.Debug.WriteLine(result);
						System.Diagnostics.Debug.WriteLine(Element+"");
						Reporter.ReportError("Error al iniciar sesión con FB. el botón es nulo " + id + "::" + firstName + "::" + lastName + "::" + result + "::" + Element);
					}
				});
				requestConnection.Start();
			});

			loginView = new LoginButton() {
				LoginBehavior = LoginBehavior.Web,
				ReadPermissions = readPermissions.ToArray()
			};

			// Handle actions once the user is logged in
			loginView.Completed += (sender, e) => {
				if (e.Error != null)
				{
					// Handle if there was an error
					(this.Element as FBLoginButton).triggerLoginIntent_Error();
				}
				else if (e.Result.IsCancelled)
				{
					// esto dispara un evento que es configurado en la pantalla donde está el botón
					(this.Element as FBLoginButton).triggerLoginIntent_Cancel();
				}
				else
				{
					System.Diagnostics.Debug.WriteLine(e.ToString());
					// Handle your successful login
				}
			};

			// Handle actions once the user is logged out
			loginView.LoggedOut += (sender, e) => {
				// Handle your logout
				Reporter.ReportError("Error al iniciar sesión con FB" + e.ToString());
			};
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
		{
			base.OnElementChanged (e);

			if (this.Control != null)
			{
				Control.TouchUpInside += Button_Click;
			}

		}

		void Button_Click (object sender, EventArgs e)
		{
			// primero tratamos de desloggear
			try
			{
				CrossFBManageriOS cfbm = new CrossFBManageriOS();
				cfbm.LogOut();
			}
			finally
			{
				(this.Element as FBLoginButton).triggerCustomClick();
				loginView.SendActionForControlEvents (UIKit.UIControlEvent.TouchUpInside);
			}
		}
	
	}
}

