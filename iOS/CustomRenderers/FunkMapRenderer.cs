﻿using System.Collections.Generic;
using CoreGraphics;
using MapKit;
using UIKit;
using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using FunkFramework.UI;
using System.Diagnostics;

[assembly: ExportRenderer(typeof(FunkMap), typeof(FunkMapRenderer))]
namespace Veepe.iOS
{
	public class CustomMKAnnotationView : MKAnnotationView
	{
		public string Id { get; set; }
		public string Url { get; set; }
		public string Label { get; set; }
		public CustomMKAnnotationView(IMKAnnotation annotation, string id) : base(annotation, id) {
			this.Id = id;
		}

		public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan(touches, evt);
			foreach (var pin in FunkMapRenderer.customPins)
			{
				if (pin.Pin.Label == this.Id)
					pin.PerformClick(this, pin);
			} // esto teniamos cuando queriamos que fuera inmediato el clic
		}
	}


	public class FunkMapRenderer : MapRenderer
	{
		//UIView customPinView;
		public static List<CustomPin> customPins;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement!=null)
			{
				var nativeMap = Control as MKMapView;
				customPins = (e.NewElement as FunkMap).CustomPins;

				// configuramos el mapa nativo
				nativeMap.GetViewForAnnotation += GetViewForAnnotation;
			}
		}

		// Crea el pin custom
		MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{
			MKAnnotationView annotationView = null;

			if (annotation is MKUserLocation) return null;		// avoids showing annotation for user location

			var customPin = GetCustomPin(annotation as MKPointAnnotation);

			if (customPin==null)
				return null;

			annotationView = mapView.DequeueReusableAnnotation(customPin.Pin.Label);
			if ( annotationView==null )
			{
				annotationView = new CustomMKAnnotationView(annotation, customPin.Pin.Label);
				annotationView.Image = UIImage.FromFile("pin.png");
				annotationView.Image.Scale(new CGSize(128, 128));
			}

			annotationView.Enabled = true;
			annotationView.UserInteractionEnabled = true;
			annotationView.CanShowCallout = false;

			return annotationView;
		}


		// ************ ESTO ES PARA AGREGAR COSAS ADICIONALES AL GLOBO ************
		void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
		}


		void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
		}


		CustomPin GetCustomPin(MKPointAnnotation annotation)
		{
			if (annotation == null)
				return null;

			var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);

			if (customPins == null)
				customPins = ((FunkMap)Element).CustomPins;

			if (customPins != null)
			{
				foreach (var pin in customPins)
				{
					if (pin.Pin.Position == position)
						return pin;
				}
			}
			return null;
		}
	} // fin de la clase
}



/*
 * 
 * esta es la versión vieja, cuando funcionaba haciendo clic en el pin y se mostraba la etiqueta y luego ...ya sabes

using System.Collections.Generic;
using CoreGraphics;
using MapKit;
using UIKit;
using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkMap), typeof(FunkMapRenderer))]
namespace Veepe.iOS
{
	public class CustomMKAnnotationView : MKAnnotationView
	{
		public string Id { get; set; }
		public string Url { get; set; }
		public string Label { get; set; }
		public CustomMKAnnotationView(IMKAnnotation annotation, string id) : base(annotation, id) { }
	}


	public class FunkMapRenderer : MapRenderer
	{
		UIView customPinView;
		List<CustomPin> customPins;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				var nativeMap = Control as MKMapView;
				nativeMap.GetViewForAnnotation = null;
				nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
			}

			if (e.NewElement != null)
			{
				var formsMap = (FunkMap)e.NewElement;
				var nativeMap = Control as MKMapView;
				customPins = formsMap.CustomPins;

				nativeMap.GetViewForAnnotation = GetViewForAnnotation;
				nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;

				nativeMap.ShowsUserLocation = true;
			}
		}


		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if ( ((FunkMap)Element).CustomPins != null )
				customPins = ((FunkMap)Element).CustomPins;
		}

		MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
		{
			MKAnnotationView annotationView = null;

			if (annotation is MKUserLocation)
				return null;

			var anno = annotation as MKPointAnnotation;
			var customPin = GetCustomPin(anno);
			if (customPin == null)
			{
				return null;
			}
			else
			{
				annotationView = mapView.DequeueReusableAnnotation(customPin.Pin.Label);
				if (annotationView == null)
				{
					annotationView = new CustomMKAnnotationView(annotation, customPin.Pin.Label);
					annotationView.Image = UIImage.FromFile("pin.png");
					annotationView.Image.Scale(new CGSize(128, 128));
					annotationView.CalloutOffset = new CGPoint(0, 0);

					((CustomMKAnnotationView)annotationView).Id = customPin.Pin.Label;

					var detailButton = UIButton.FromType(UIButtonType.DetailDisclosure);
					detailButton.SetTitle(customPin.Pin.Label, UIControlState.Normal);
					detailButton.TouchUpInside += (sender, e) => {
						// mostrar detalle de estacionamiento
						System.Diagnostics.Debug.WriteLine("click!");

						var algo = (sender as UIButton).CurrentTitle;

						// buscar el custompin que tiene esta etiqueta
						foreach (var pin in customPins)
						{
							if (pin.Pin.Label == algo)
								pin.PerformClick(this, pin);
						}
					};
					annotationView.RightCalloutAccessoryView = detailButton;
				}
				annotationView.Enabled = true;
				annotationView.UserInteractionEnabled = true;
				annotationView.CanShowCallout = true;

				return annotationView;
			}
		}


		// ************ ESTO ES PARA AGREGAR COSAS ADICIONALES AL GLOBO ************
		void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			/*e.View.BackgroundColor = UIColor.Purple;
			customPinView = new UIView();

			customPinView.Frame = new CGRect(0, 0, 200, 84);
			customPinView.BackgroundColor = UIColor.Red;
			//var image = new UIImageView(new CGRect(0, 0, 200, 84));
			image.Image = UIImage.FromFile("xamarin.png");
			customPinView.AddSubview(image);
			customPinView.Center = new CGPoint(0, -(e.View.Frame.Height));
			e.View.AddSubview(customPinView);* ***

			/*foreach (var pin in customPins)
			{
				if (pin.Pin.Label == (e.View as CustomMKAnnotationView).Id)
				{
					// todo: encontramos el pin e invocamos su método click
					pin.PerformClick(this, pin);
				}
			}* *** // esto teniamos cuando queriamos que fuera inmediato el clic
		}


		void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			if (!e.View.Selected && customPinView!=null)
			{
				customPinView.RemoveFromSuperview();
				customPinView.Dispose();
				customPinView = null;
			}
		}


		CustomPin GetCustomPin(MKPointAnnotation annotation)
		{
			if (annotation == null)
				return null;

			var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);

			if (customPins == null)
				customPins = ((FunkMap)Element).CustomPins;

			if (customPins != null)
			{
				foreach (var pin in customPins)
				{
					if (pin.Pin.Position == position)
						return pin;
				}
			}
			return null;
		}

	} // fin de la clase
}

*/