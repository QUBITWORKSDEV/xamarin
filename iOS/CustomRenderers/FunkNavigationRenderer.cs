﻿using FunkFramework;
using UIKit;
using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(FunkNavigationRenderer))]
namespace Veepe.iOS
{
	public class FunkNavigationRenderer : NavigationRenderer
	{
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);
			repurposeActionBar();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			repurposeActionBar();
		}

		void repurposeActionBar()
		{
			NavigationBar.TintColor = UIKit.UIColor.White;
			if ( GlobalSettings.isDevelop )
				NavigationBar.BarTintColor = UIKit.UIColor.FromRGB(155, 126, 61);
			else
				NavigationBar.BarTintColor = UIKit.UIColor.FromRGB(65, 64, 66);

			// quitamos la sombra
			NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
			NavigationBar.ShadowImage = new UIImage ();

			NavigationBar.BarStyle = UIKit.UIBarStyle.Black;

			//NavigationBar.Items[0].SetLeftBarButtonItem(new UIKit.UIBarButtonItem(), true);

			// http://stackoverflow.com/questions/14606294/remove-icon-logo-from-action-bar-on-android
			//var actionBar = ((Activity)Context).ActionBar;

			/*GradientDrawable gd = new GradientDrawable();
			//gd.SetColor( Xamarin.Forms.Color.FromHex("#090A19").ToAndroid() );
			//gd.SetColor(Xamarin.Forms.Color.FromHex("#414042").ToAndroid());
			gd.SetColor((Xamarin.Forms.Application.Current as App).navbarColor.ToAndroid());
			actionBar.SetBackgroundDrawable(gd);

			actionBar.Elevation = 0;
			actionBar.SetIcon(new ColorDrawable(Xamarin.Forms.Color.Transparent.ToAndroid()));

			if (actionBar.Title != null)
			{
				var st = new SpannableString(actionBar.Title);
				st.SetSpan(new CustomFontSpan("Fonts/BebasNeue.otf"), 0, st.Length(), SpanTypes.ExclusiveExclusive);
				actionBar.TitleFormatted = st;
			}*/
		}
	} // fin de la clase
}
