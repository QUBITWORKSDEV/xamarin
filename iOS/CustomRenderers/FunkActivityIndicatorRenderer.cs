﻿using Veepe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using FunkFramework.UI;

[assembly: ExportRenderer(typeof(FunkActivityIndicator), typeof(FunkActivityIndicatorRenderer))]
namespace Veepe.iOS
{
	public class FunkActivityIndicatorRenderer : ImageRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.AnimationImages = new UIImage[] {
					  UIImage.FromBundle ("Animations/vp_loader_data_00000.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00001.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00002.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00003.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00004.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00005.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00006.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00007.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00008.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00009.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00010.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00011.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00012.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00013.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00014.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00015.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00016.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00017.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00018.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00019.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00020.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00021.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00022.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00023.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00024.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00025.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00026.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00027.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00028.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00029.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00030.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00031.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00032.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00033.png")
					, UIImage.FromBundle ("Animations/vp_loader_data_00034.png")
				};
				Control.AnimationRepeatCount = 0;
				Control.AnimationDuration = 5;
				Control.StartAnimating();
			}

		}


	}// fin de la clase
}

