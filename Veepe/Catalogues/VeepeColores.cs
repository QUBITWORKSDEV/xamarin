﻿using System;
using Xamarin.Forms;

namespace Veepe
{
	public static class VeepeColores
	{
		public static Color DarkSecondaryColor = Color.FromHex("#090A19");
		public static Color DarkPrimaryColor = Color.FromHex("#090A19");
		public static Color AccentColor = Color.FromHex("#090A19");
		public static Color SecondaryText = Color.FromHex("#090A19");
		public static Color Text = Color.FromHex("#090A19");
		public static Color ColorFalse = Color.FromHex("#090A19");
		public static Color DarkTertiaryColor = Color.FromHex("#090A19");
		public static Color AccentColorHover = Color.FromHex("#090A19");
		public static Color DividerColor = Color.FromHex("#090A19");
		public static Color HeaderColor = Color.FromHex("#090A19");
	}
}

