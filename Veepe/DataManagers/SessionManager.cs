using FunkFramework;
using FunkFramework.FB;
using Plugin.Settings;
using Xamarin.Forms;

namespace Veepe
{
	public class SessionManager
	{

		// ___________________________________________________________________ Funciones de usuario

		/// <summary>
		/// Indica si un usuario está loggeado </summary>
		/// <returns>
		/// True si está loggeado, false en caso contrario </returns>
		public static bool isLogged()
		{
			return CrossSettings.Current.GetValueOrDefault<bool>("isLogged", false);
		}

		/// <summary>
		/// Cierra la sesión de un usuario y destruye los datos almacenados localmente </summary>
		/// <returns>
		/// True si se pudo cerrar la sesión, false en caso contrario </returns>
		public static bool logOut()
		{
			bool res = true;

			try
			{
				CrossSettings.Current.Remove("usuario.id");
				CrossSettings.Current.Remove("usuario.correo");
				CrossSettings.Current.Remove("usuario.nombre");
				CrossSettings.Current.Remove("usuario.apellidop");
				CrossSettings.Current.Remove("usuario.apellidom");
				CrossSettings.Current.Remove("usuario.nombreestacionamiento");
				CrossSettings.Current.Remove("usuario.celular");
				CrossSettings.Current.Remove("usuario.veepeid");
				CrossSettings.Current.Remove("usuario.rol");
				CrossSettings.Current.Remove("usuario.idEstacionamiento");
				CrossSettings.Current.Clear();
				CrossSettings.Current.AddOrUpdateValue("isLogged", false);

				if ((Application.Current as App).usuario.Tipo == 3) // es tipo fb
				{
					DependencyService.Get<CrossFBManager>().LogOut();
				}
			}
			catch {
				res = false;
			}

			return res;
		}

		/// <summary>
		/// Regresa el id del estacionamiento al cual pertenece el usuario actual </summary>
		/// <returns>
		/// El <int>id del estacionamiento o -1 si es un usuario final o no se está loggeado </returns>
		public static int getIdEstacionamiento()
		{
			return CrossSettings.Current.GetValueOrDefault<int>("usuario.idEstacionamiento", -1);
		}

		/// <summary>
		/// Regresa el veepeId del usuario actual </summary>
		/// <returns>
		/// El <string>veepeId del usuario actual o cadena vacía si no está loggeado </returns>
		public static string getVeepeIdUsuario()
		{
			return CrossSettings.Current.GetValueOrDefault<string>("usuario.veepeid", "");
		}

		public static int getIdUsuario()
		{
			return CrossSettings.Current.GetValueOrDefault<int>("usuario.id", -1);
		}

		/// <summary>
		/// Guarda en la sesión local la información del usuario loggeado. También guarda la bandera -isLogged- </summary>
		/// <returns>
		/// Regresa True si se pudo almacenar la info, False en caso contrario </returns>
		public static bool registerLogin(Usuario u)
		{
			bool res = true;

			res &= CrossSettings.Current.AddOrUpdateValue<int>("usuario.id", u.idUsuario);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.veepeid", u.veepeId);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.nombre", u.nombre);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.apellidop", u.apellidoPaterno);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.apellidom", u.apellidoMaterno);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.correo", u.correo);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.celular", u.celular);
			res &= CrossSettings.Current.AddOrUpdateValue<int>("usuario.nip", u.nip);
			res &= CrossSettings.Current.AddOrUpdateValue<int>("usuario.rol", u.rol);
			res &= CrossSettings.Current.AddOrUpdateValue<string>("usuario.nombreestacionamiento", u.nombreEstacionamiento);

			if (u.idEstacionamiento != -1)
				res &= CrossSettings.Current.AddOrUpdateValue<int>("usuario.idEstacionamiento", u.idEstacionamiento);

			res &= CrossSettings.Current.AddOrUpdateValue<bool>("isLogged", true);

			return res;
		}

		/// <summary>
		/// Regresa la información del usuario guardado en local </summary>
		/// <returns>
		/// Regresa el <Usuario>usuario almacenado en local o un usuario vacío si no hay info </returns>
		public static Usuario getLocalUser()
		{
			var res = new Usuario()
			{
				idUsuario = CrossSettings.Current.GetValueOrDefault<int>("usuario.id", -1),
				correo = CrossSettings.Current.GetValueOrDefault<string>("usuario.correo", ""),
				nombre = CrossSettings.Current.GetValueOrDefault<string>("usuario.nombre", ""),
				apellidoPaterno = CrossSettings.Current.GetValueOrDefault<string>("usuario.apellidop", ""),
				apellidoMaterno = CrossSettings.Current.GetValueOrDefault<string>("usuario.apellidom", ""),
				veepeId = CrossSettings.Current.GetValueOrDefault<string>("usuario.veepeid", ""),
				rol = CrossSettings.Current.GetValueOrDefault<int>("usuario.rol", 8),
				idEstacionamiento = CrossSettings.Current.GetValueOrDefault<int>("usuario.idEstacionamiento", -1),
				celular = CrossSettings.Current.GetValueOrDefault<string>("usuario.celular", ""),
				nip = CrossSettings.Current.GetValueOrDefault<int>("usuario.nip", -1),
				nombreEstacionamiento = CrossSettings.Current.GetValueOrDefault<string>("usuario.nombreestacionamiento", ""),
			};

			return res;
		}

		public static void setServicioActivo(Operacion operacion)
		{
			CrossSettings.Current.AddOrUpdateValue<bool>("usuario.tieneServicioActivo", true);
		}

		public static bool getTieneServicioActivo()
		{
			return CrossSettings.Current.GetValueOrDefault<bool>("usuario.tieneServicioActivo", false);
		}
	} // fin de SessionManager


}


