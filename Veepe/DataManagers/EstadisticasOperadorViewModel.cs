﻿using System;
using System.ComponentModel;

namespace Veepe
{
	public class EstadisticasOperadorViewModel : INotifyPropertyChanged
	{
		private int _capacidadTotal;
		private int _general;
		private int _pensiones;
		private int _reserva;
		private int _disponibleGeneral;
		private int _ingresoGeneral;
		private int _pensionesDisponibles;
		private int _ingresosPendientes;

		public event PropertyChangedEventHandler PropertyChanged;

		public int CapacidadTotal { 
			get { return _capacidadTotal; }
			set {
				_capacidadTotal = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("CapacidadTotal"));
			}
		}

		public int General {
			get { return _general; }
			set
			{
				_general = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("General"));
			}
		}

		public int Pensiones {
			get { return _pensiones; }
			set
			{
				_pensiones = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Pensiones"));
			}
		}

		public int Reserva {
			get { return _reserva; }
			set
			{
				_reserva = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Reserva"));
			}
		}

		public int DisponibleGeneral {
			get { return _disponibleGeneral; }
			set
			{
				_disponibleGeneral = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("DisponibleGeneral"));
			}
		}

		public int IngresoGeneral {
			get { return _ingresoGeneral; }
			set
			{
				_ingresoGeneral = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IngresoGeneral"));
			}
		}

		public int PensionesDisponibles {
			get { return _pensionesDisponibles; }
			set
			{
				_pensionesDisponibles = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("PensionesDisponibles"));
			}
		}

		public int IngresosPendientes {
			get { return _ingresosPendientes; }
			set
			{
				_ingresosPendientes = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IngresosPendientes"));
			}
		}

	}
}


