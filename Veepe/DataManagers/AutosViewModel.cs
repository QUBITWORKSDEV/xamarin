﻿using System.ComponentModel;

namespace Veepe
{
	public class AutosViewModel : INotifyPropertyChanged
	{
		Auto _auto;

		public event PropertyChangedEventHandler PropertyChanged;

		public Auto auto {
			get {
				return _auto;
			}
			set {
				_auto = value;
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("Modelo"));
					PropertyChanged(this, new PropertyChangedEventArgs("Marca"));
					PropertyChanged(this, new PropertyChangedEventArgs("Color"));
					PropertyChanged(this, new PropertyChangedEventArgs("Placa"));
				}
			}
		}

		public string Marca
		{
			get { return _auto.Marca; }
			set { _auto.Marca = value; }
		}
		public string Submarca
		{
			get { return _auto.Submarca; }
			set { _auto.Submarca = value; }
		}
		public string Modelo
		{
			get { return _auto.Modelo; }
			set { _auto.Modelo = value; }
		}
		public string Color
		{
			get { return _auto.Color; }
			set { _auto.Color = value; }
		}
		public string Placa
		{
			get { return _auto.Placa; }
			set { _auto.Placa = value; }
		}

		public AutosViewModel()
		{
			_auto = new Auto();
		}
	}
}
