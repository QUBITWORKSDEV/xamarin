﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace Veepe
{
	public class AutosOperacionViewModel : INotifyPropertyChanged
	{
		DateTime _horaEstimadaEntrega;
		int _tiempoEspera = 0;
		byte _status = 0;
		public bool _usaPension = false;

		public event PropertyChangedEventHandler PropertyChanged;

		public ObservableCollection<PensionContratada> _pensionesContratadas;
		public ObservableCollection<PensionContratada[]> _pensionesCompartidasContratadas;

		// ______________________________________________ Atributos públicos
		public int idOperacion { get; set; }
		public string Usuario { get; set; }				// el nombre del usuario
		public string Placa { get; set; }				// las placas del auto
		public string Marca { get; set; }				// las placas del auto
		public string Submarca { get; set; }			// las placas del auto
		public DateTime fechaReserva { get; set;}		// la hora en que fue hecha la reserva
		public DateTime Entrada { get; set; }			// la hora de entrada
		public DateTime Salida { get; set; }			// la hora de salida
		public TimeSpan Tiempo {						// el tiempo que lleva en operación
			get {
				return (DateTime.Now - Entrada);
			}
		}
		public string Recibio { get; set; }					// el operador que recibió
		public string Servicios { get; set; }				// la lista de servicios contratados
		public DateTime horaEstimadaEntrega { 
			get { return _horaEstimadaEntrega; } 
			set {
				_horaEstimadaEntrega = value;
			} 
		}   // la hora estimada en que será entregado el auto
		public float TarifaHora { get; set; }				// la tarifa por hora en este estacionamiento


		//public PensionContratada[] pensiones;
		public PensionContratadaResponse pensiones
		{
			set
			{
				// guardamos las pensiones normales
				if (value.normales != null)
				{
					_pensionesContratadas = new ObservableCollection<PensionContratada>();
					for (int i = 0; i < value.normales.Length; i++)
					{
						_pensionesContratadas.Add(value.normales[i] as PensionContratada);
					}
				}

				// guardamos las pensiones compartidas
				if (value.compartidas != null)
				{
					_pensionesCompartidasContratadas = new ObservableCollection<PensionContratada[]>();
					for (int i = 0; i < value.compartidas.Length; i++)
					{
						_pensionesCompartidasContratadas.Add(value.compartidas[i] as PensionContratada[]);
					}
				}
			}
		}


		public byte Status {							// el status (0=null, 1=reservado, 3=ingresado/en operación, 5=solicitado por el usuario, 7=con tiempo aceptado por el operador, 11=entregado, 4=reingresado)
			get {
				return _status;
			}
			set {
				_status = value;
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("tiempoEspera"));
					PropertyChanged(this, new PropertyChangedEventArgs("tieneTiempoDeEntrega"));
					PropertyChanged(this, new PropertyChangedEventArgs("estaSoloSolicitado"));
					PropertyChanged(this, new PropertyChangedEventArgs("SalidaColor"));
				}
			} 
		}
		public int tiempoEspera {						// el tiempo que el operador indicó que tardaría en entregar el auto
			get {
				return _tiempoEspera;
			} 
			set {
				_tiempoEspera = value;
				if (_tiempoEspera > 0)
				{
					//Status = 7;
				}
			}
		}
		public string ReadableStatus
		{
			get {
				string res = "";
				switch (_status)
				{
					case 1:
						res = "Reservado";
						break;
					case 3:
						res = "Aceptación";
						break;
					case 4:	
						res = "Activo";
						break;
					case 5:
						res = "Solicitado";
						break;
					case 7:
						res = "En entrega";
						break;
					case 10:
						res = "Calificando";
						break;
					case 11:
						res = "Entregado";
						break;
				}

				return res;
			}
		}

		// ______________________________________________ Propiedades indirectas (que calculan algo)
		public string readableEntrada
		{
			get
			{
				if (Status == 1)
				{
					// entramos en este cuando es una reservación (y por lo tanto aún no hay fecha de entrada), pero regresamos la fecha estiamda de llegada
					DateTime aux = fechaReserva.AddMinutes(30);
					return "Max: " + aux.ToString("d MMMMM, H:mm");
				}
				else
					return Entrada.ToString("d MMMMM, H:mm");
			}
		}

		public bool estaSoloSolicitado
		{
			get { return (_status == 5); }
		}

		public bool esEntregable
		{
			get { return (_status >= 3 && _status < 10); }
		}

		public bool esOperacionActiva
		{
			get { return (_status > 1 && _status < 10); }
		}

		public bool esReserva
		{
			get { return _status == 1; }
		}

		public bool tieneTiempoDeEntrega
		{
			get { return (_status == 7); }
		}

		public Color SalidaColor
		{
			get {
				if (_status == 7)
					return Color.FromHex("#00BFD6");
				else if (_status == 5)
					return Color.FromHex("#F68672");
				else
					return Color.Transparent;
			}
		}

		public string TiempoTranscurrido
		{
			get {
				if (Status == 1)
				{
					// es reserva ergo, no tiempo
					return "";
				}
				else
				{
					if (Status >= 10)
					{
						if (Salida != null)
						{
							// regresamos la diferencia de tiempo
							TimeSpan diferencia = Salida - Entrada;
							return ((diferencia.Days * 24) + diferencia.Hours).ToString("D2") + ":" + diferencia.Minutes.ToString("D2");
						}
						else
						{
							return null;
						}
					}
					else
						return ((Tiempo.Days * 24) + Tiempo.Hours).ToString("D2") + ":" + Tiempo.Minutes.ToString("D2");
				}
			}
		}

		private bool caeDentro(int hora, int tipo)
		{
			bool res;

			if (tipo == 1)  // de 8 a 20
			{
				res = (hora >= 8 && hora < 20);
			}
			else if (tipo == 3) // de 20 a 8
			{
				res = (hora >= 20 || hora < 8);
			}
			else
				res = true;

			return res;
		}


		/// <summary>
		/// CostoAproximado </summary>
		/// <value>
		/// El costo aproximado de una operación, tomando en cuenta las pensiones</value>
		public float CostoAproximado
		{
			get
			{
				// si tiene reserva, el costo es cero
				if ( esReserva )
					return 0f;


				float res = 0f;

				DateTime now = DateTime.Now;
				TimeSpan t = now - Entrada;
				//System.Diagnostics.Debug.WriteLine(Entrada + " a " + now);

				// calcular el costo tomando en cuenta las pensiones
				if ( (_pensionesContratadas != null && _pensionesContratadas.Count > 0) || (_pensionesCompartidasContratadas != null && _pensionesCompartidasContratadas.Count > 0) )
				{
					int suma = 0;	// la suma mágica

					for (int i = 0; i < _pensionesContratadas?.Count; i++)
					{
						if (_pensionesContratadas[i].idEstacionamiento == (Application.Current as App).usuario.idEstacionamiento)
						{
							suma += _pensionesContratadas[i].horario;
						}
					}
					for (int i = 0; i < _pensionesCompartidasContratadas?.Count; i++)
					{
						for (int j = 0; j < _pensionesCompartidasContratadas[i].Length; j++)
						{
							if (_pensionesCompartidasContratadas[i][j].idEstacionamiento == (Application.Current as App).usuario.idEstacionamiento)
							{
								suma += _pensionesCompartidasContratadas[i][j].horario;
							}
						}
					}

					if (suma >= 4)
					{
						// la combinación de pensiones que tiene le cubren todo el dia, ergo no se cobra nada
						return 0f;
					}
					else
					{
						/*
						 * Breve explicación del método usado para calcular el costo cuando hay pensión
						 *
						 * Se tiene un arreglo con las 24 hrs del día.
						 * 1. Se recorre el tiempo que ha estado el usuario en el estacionamiento.
						 * En cada hora se suman los minutos a la hora correspondiente de modo que el arreglo 
						 * lleva una suma de los minutos que ha estado el usuario. La primera hora y la última 
						 * son las únicas que no son 60.
						 * 2. Luego se recorren todas las pensiones que tiene el usuario en este estacionamiento 
						 * y para cada una de ellas se recorren las horas que cubre; cada una de esas horas se 
						 * pone en cero en el arreglo de modo que esas horas no se cobren.
						 * 3. Finalmente se recorre el arreglo y se van sumando las horas y se calculan las 
						 * fracciones, que nos dan el total que se debe pagar.
						 *
						 **/

						int[] acumuladorHoras = new int[24];
						DateTime contador = Entrada;
						bool falta = true;
						int seguridad = 0;
						bool esElPrimero = true;

						// paso 1: contabilizamos el total bruto
						while (falta && seguridad < 1000)
						{
							if (contador == Entrada)	// es el primero -> tomamos sólo los minutos
							{
								acumuladorHoras[contador.Hour] += (60-contador.Minute);
								if (contador.Year == now.Year && contador.Month == now.Month && contador.Day == now.Day && contador.Hour == now.Hour)
									falta = false;
							}
							else if (contador.Year==now.Year && contador.Month==now.Month && contador.Day==now.Day && contador.Hour==now.Hour)	// es el último -> tomamos sólo los minutos
							{
								if (!esElPrimero)
									acumuladorHoras[contador.Hour] += now.Minute;
								falta = false;
							}
							else
							{
								acumuladorHoras[contador.Hour] += 60;	// es una hora intermedia -> contar completa
							}

							esElPrimero = false;
							seguridad++;
							contador = contador.AddHours(1);
						}
						Debug.WriteLine("**->" + seguridad);

						// paso 2: ahora quitamos las horas cubiertas por las pensiones
						for (int i = 0; i < _pensionesContratadas?.Count; i++)
						{
							if (_pensionesContratadas[i].idEstacionamiento == (Application.Current as App).usuario.idEstacionamiento)
							{
								if (_pensionesContratadas[i].horario == 1)	// es de día
								{
									for (int m = 7; m <= 18; m++)
									{
										_usaPension |= acumuladorHoras[m] != 0;
										acumuladorHoras[m] = 0;
									}
								}
								else if (_pensionesContratadas[i].horario == 3)	// es de noche
								{
									for (int m = 19; m <= 23; m++)
									{
										_usaPension |= acumuladorHoras[m] != 0;
										acumuladorHoras[m] = 0;
									}
									for (int m = 0; m <= 6; m++)
									{
										_usaPension |= acumuladorHoras[m] != 0;
										acumuladorHoras[m] = 0;
									}
								}
							}
						}
						for (int i = 0; i < _pensionesCompartidasContratadas?.Count; i++)
						{
							for (int j = 0; j < _pensionesCompartidasContratadas[i].Length; j++)
							{
								if (_pensionesCompartidasContratadas[i][j].idEstacionamiento == (Application.Current as App).usuario.idEstacionamiento)
								{
									if (_pensionesCompartidasContratadas[i][j].horario == 1) // es de día
									{
										for (int m = 7; m <= 18; m++)
										{
											_usaPension |= acumuladorHoras[m] != 0;
											acumuladorHoras[m] = 0;
										}
									}
									else if (_pensionesCompartidasContratadas[i][j].horario == 3)    // es de noche
									{
										for (int m = 19; m <= 23; m++)
										{
											_usaPension |= acumuladorHoras[m] != 0;
											acumuladorHoras[m] = 0;
										}
										for (int m = 0; m <= 6; m++)
										{
											_usaPension |= acumuladorHoras[m] != 0;
											acumuladorHoras[m] = 0;
										}
									}
								}
							}
						}

						// paso 3: hacemos la suma total
						int total = 0;
						for (int i = 0; i < 24; i++)
						{
							total += acumuladorHoras[i];
						}
						int hrs = (total / 60);
						int min = total - (hrs*60);

						System.Diagnostics.Debug.WriteLine(hrs + ":" + min);

						if (hrs < 1)
							if (min == 0)
								res = 0;
							else
								res = 1 * TarifaHora;
						else
						{
							float fraccion = min<15? .25f : ( min<30? .5f : ( min<45? .75f : 1f ) );
							res = (hrs + fraccion) * TarifaHora;
						}
					}
				}
				else
				{
					if (Status == 1)
						res = 0f;
					else
					{
						if (Status >= 10)
							t = Salida - Entrada;	// si ya es una operación cerrada, calcular con base en la hora de salida

						// no tiene pensiones -> cálculo normal
						float fraccion = 0f;
						if (t.Minutes < 15)
							fraccion = 0.25f;
						else if (t.Minutes < 30)
							fraccion = 0.5f;
						else if (t.Minutes < 45)
							fraccion = 0.75f;
						else
							fraccion = 1f;

						if (t.TotalHours < 1)
							res = 1 * TarifaHora;	// se cobra la primera hora, sí o sí
						else
							res = ((int)t.TotalHours + fraccion) * TarifaHora;
					}
				}

				return res;
			}
		}

		public int tiempoRestante	// el número de segundos que faltan para que se acabe el tiempo de entrega
		{
			get
			{
				System.Diagnostics.Debug.WriteLine(tiempoEspera + ", " + DateTime.Now + ", " + horaEstimadaEntrega + " -- " + (_tiempoEspera * 60) + "->" + (horaEstimadaEntrega - DateTime.Now).TotalSeconds);
				return (_tiempoEspera * 60) - (int)(horaEstimadaEntrega - DateTime.Now).TotalSeconds;
			}
		}
		public string tiempoRestanteReadable
		{
			get {
				return (horaEstimadaEntrega - DateTime.Now).Minutes.ToString("D2") + ":" + (horaEstimadaEntrega - DateTime.Now).Seconds.ToString("D2");
			}
		}


	}
}

