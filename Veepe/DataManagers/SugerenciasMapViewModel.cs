﻿namespace Veepe
{
	public class SugerenciasMapViewModel
	{
		public string Texto { get; set; }
		public double Latitud { get; set; }
		public double Longitud { get; set; }
	}
}

