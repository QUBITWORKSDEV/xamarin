﻿using System;
using System.Text.RegularExpressions;

namespace Veepe
{
	public class LoginViewModel
	{
		public string Email { get; set;}
		public string Password { get; set;}
		const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
		
		public LoginViewModel ()
		{
			Email = "";
			Password = "";
		}

		public bool ValidateLogin()
		{
			bool esValido = true;

			// validamos el username
			if (Email.Length == 0 || !Regex.IsMatch(Email, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)) )
			{
				esValido = false;
			}

			// validamos el username
			if (Password.Length == 0)
			{
				esValido = false;
			}

			return esValido;
		}

	} // fin de la clase LoginViewModel
}

