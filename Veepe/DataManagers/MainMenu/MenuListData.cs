﻿using System.Collections.Generic;

namespace Veepe
{
	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{
			this.Add (new MenuItem () { 
				Title = "Inicio", 
				IconSource = "iconos_menu_inicio.png", 
				TargetType = typeof(P_HomeDetail)
			});

			this.Add (new MenuItem () { 
				Title = "Veepe ID", 
				IconSource = "iconos_menu_id.png", 
				TargetType = typeof(P_VeepeId)
			});

			this.Add (new MenuItem () { 
				Title = "Perfil", 
				IconSource = "iconos_menu_perfil.png", 
				TargetType = typeof(P_Perfil)
			});

			this.Add(new MenuItem()
			{
				Title = "Formas de Pago",
				IconSource = "iconos_menu_fromaspago.png",
				TargetType = typeof(P_MetodosPago)
			});

			this.Add(new MenuItem()
			{
				Title = "Mis Autos",
				IconSource = "iconos_menu_autos.png",
				TargetType = typeof(P_MisAutos)
			});

			this.Add (new MenuItem () { 
				Title = "Pensiones", 
				IconSource = "iconos_menu_servicios.png", 
				TargetType = typeof(P_PensionesContratadas)
			});

			/*this.Add (new MenuItem () {
				Title = "Promociones",
				IconSource = "iconos_menu_promociones.png",
				TargetType = typeof(P_Promociones)
			});*/

			this.Add (new MenuItem () {
				Title = "Historial",
				IconSource = "iconos_menu_historial.png",
				TargetType = typeof(P_Historial)
			});

			/*this.Add (new MenuItem () {
				Title = "Ayuda",
				IconSource = "iconos_menu_ayuda.png",
				TargetType = typeof(P_Ayuda)
			});*/

			/*this.Add (new MenuItem () {
				Title = "Configuración",
				IconSource = "iconos_menu_configuracion.png",
				TargetType = typeof(P_TicketServicio)
			});*/

			/*this.Add(new MenuItem()
			{
				Title = "Dev",
				IconSource = "iconos_menu_configuracion.png",
				TargetType = typeof(P_Dev)
			});*/
		}
	}
}
