﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace Veepe
{
	public class MenuListView : ListView
	{
		public MenuListView ()
		{
			List<MenuItem> data = new MenuListData ();

			ItemsSource = data;

			// Configuración de la apariencia
			this.VerticalOptions = LayoutOptions.StartAndExpand;
			this.BackgroundColor = VeepeColores.DarkSecondaryColor;
			this.IsPullToRefreshEnabled = false;

			var cell = new DataTemplate (typeof(ImageCell));
			cell.SetBinding (TextCell.TextProperty, "Title");
			cell.SetBinding (ImageCell.ImageSourceProperty, "IconSource");

			//cell.SetBinding( BackgroundColorProperty, new Binding("BackgroundColor") );

			cell.SetValue (TextCell.TextColorProperty, Color.FromHex ("#FFFFFF"));

			ItemTemplate = cell;
		}
	}
}
