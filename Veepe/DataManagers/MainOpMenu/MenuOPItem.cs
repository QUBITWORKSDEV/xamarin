﻿using System;

namespace Veepe
{
	public class MenuOPItem {
		public string Title { get; set; }
		public string IconSource { get; set; }
		public Type TargetType { get; set; }
	}
}

