﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Veepe
{
	public class MenuOPListData : List<MenuItem>
	{
		public MenuOPListData ()
		{
			this.Add (new MenuItem () { 
				Title = "Inicio",
				IconSource = "iconos_menu_inicio.png",
				TargetType = typeof( P_OpHomeDetail )
			});

			this.Add (new MenuItem () { 
				Title = "Perfil", 
				IconSource = "iconos_menu_perfil.png", 
				TargetType = typeof(P_Perfil)
			});

			/*this.Add (new MenuItem () { 
				Title = "Control de personal", 
				IconSource = "iconos_menu_servicios.png", 
				TargetType = typeof(P_Servicios)
			});*/

			/*this.Add (new MenuItem () {
				Title = "Historial",
				IconSource = "iconos_menu_historial.png",
				TargetType = typeof( P_HistorialEstacionamiento )
			});*/

			/*this.Add (new MenuItem () {
				Title = "Ayuda",
				IconSource = "iconos_menu_configuracion.png",
				TargetType = typeof(P_Ayuda)
			});*/
		}
	}
}

