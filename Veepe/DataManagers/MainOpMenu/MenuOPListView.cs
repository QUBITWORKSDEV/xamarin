﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Veepe
{
	public class MenuOPListView : ListView
	{
		public MenuOPListView ()
		{
			List<MenuItem> data = new MenuOPListData ();

			ItemsSource = data;

			// Configuración de la apariencia
			this.VerticalOptions = LayoutOptions.StartAndExpand;
			this.BackgroundColor = VeepeColores.DarkSecondaryColor;
			this.IsPullToRefreshEnabled = false;

			var cell = new DataTemplate (typeof(ImageCell));
			cell.SetBinding (TextCell.TextProperty, "Title");
			cell.SetBinding (ImageCell.ImageSourceProperty, "IconSource");

			cell.SetValue (TextCell.TextColorProperty, Color.FromHex ("#FFFFFF"));

			ItemTemplate = cell;
		}
	}
}
