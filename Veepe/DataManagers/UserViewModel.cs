using System;

namespace Veepe
{

	public class UserViewModel
	{
		private Usuario _usuario;

		public string Nombre
		{ 
			get { return _usuario.nombre; }
		}

		public string NombreC
		{
			get { return _usuario.nombre + " " + _usuario.apellidoPaterno; }
		}

		public string ApellidoPaterno
		{
			get { return _usuario.apellidoPaterno; }
		}

		public string ApellidoMaterno
		{
			get { return _usuario.apellidoMaterno; }
		}

		public string Celular
		{
			get { return _usuario.celular; }
		}

		public int Nip
		{
			get { return _usuario.nip; }
		}

		public string VeepeId
		{
			get { return _usuario.veepeId; }
		}

		public UserViewModel(bool loadLocal = false)
		{
			if (loadLocal)
				_usuario = SessionManager.getLocalUser();
		}

	} // fin de UserViewModel
}