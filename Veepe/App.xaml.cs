﻿using Xamarin.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using System;
using FunkFramework;
using PushNotification.Plugin;
using FunkFramework.Net;
using FunkFramework.Media;

namespace Veepe
{
	public partial class App : Application
	{
		public Usuario usuario = null;
		public IConnectivityMonitor cm;
		public NavigationPage np {
			get { return this.MainPage as NavigationPage; }
		}

		public App()
		{
			// *********************************************************************************
			GlobalSettings.currentVersion = "1.0.22";

			GlobalSettings.isDevelop = false;
			GlobalSettings.serverUri = "http://test.veepe.mx/ws";
			GlobalSettings.wsUrl = "http://test.veepe.mx/ws/gateway2.php";
			GlobalSettings.googleWebAkiKey = "AIzaSyAwx1BN1HNhSNp2HRtdbcUoVZxg_AZxZAg";
			// *********************************************************************************

			InitializeComponent();
			cm = DependencyService.Get<IConnectivityMonitor>();

			#region Inicialización
			// Cargamos la info del usuario local, si existe
			if (SessionManager.isLogged())
			{
				usuario = SessionManager.getLocalUser();
			}
			else
				usuario = null;
			#endregion

			// Redirigimos
			if (Device.RuntimePlatform == Device.iOS)
				MainPage = new P_Splash();
			Device.BeginInvokeOnMainThread(async () => {
				await StartupSequence();
			});
		}

		public void playJingle()
		{
			try
			{
				IFunkAudio fa = DependencyService.Get<IFunkAudio>();
				fa.PlayAudioFile("capisci.mp3");
			} catch (Exception ex) {
				Debug.WriteLine(ex.ToString());
			}
		}

		public async Task StartupSequence()
		{
			// 1. validar si el usuario cargado es válido
			bool esValido = false;
			if (usuario != null)
			{
				esValido |= (usuario.rol != -1 && usuario.nombre != "");

				if (usuario.rol == Usuario.ROL_USUARIO_FINAL)
					esValido &= (usuario.nip != -1 && usuario.veepeId != "");
			}

			// 2. proceder de acuerdo a si es necesario iniciar sesión o no
			if (esValido && SessionManager.isLogged())
			{
				if (usuario.rol == Usuario.ROL_OPERADOR || usuario.rol == Usuario.ROL_JEFE_ESTACIONAMIENTO)
					MainPage = new P_OpHome();
				else
				{
					await updateHomePage();
				}
			}
			else
			{
				MainPage = new NavigationPage(new P_LoginInicio());
			}
		}

		public async Task updateHomePage()
		{
			Page p = await getHomeUsuario();

			if (p != null)
			{
				Device.BeginInvokeOnMainThread(() => {
					MainPage = p;
				});
			}
		}

		public async Task<Page> getHomeUsuario()
		{
			Page res = null;
			string currentPage = "";
			if ( MainPage != null )
				currentPage = MainPage.GetType().ToString()=="Veepe.P_Home" ? (MainPage as MasterDetailPage).Detail.GetType().ToString() : MainPage.GetType().ToString();

			if (usuario?.veepeId != null && cm?.HasConnectivity() == true)
			{
				// obtenemos el status del usuario
				WsResponse<int> items = await WsConsumer.Consume<int>("data", "getStatusUsuario", "{\"veepeId\":\"" + usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						(Application.Current as App).usuario.tieneReserva = false;
						(Application.Current as App).usuario.reserva = null;

						// requiere actualizar el pushtoken?
						if (items.mensaje.Contains("actualizarpush"))
						{
							WsResponse<bool> items2 = await WsConsumer.Consume<bool>("data", "registerPushToken", "{\"uid\":\"" + (Application.Current as App).usuario.idUsuario + "\",\"token\":\"" + CrossPushNotification.Current.Token + "\",\"plat\":\"" + (Device.RuntimePlatform==Device.iOS?"ios":"android") + "\"}");
						}

						switch (items.datos)
						{
							// -1->usuario no existe, 0->perfil incompleto, 1->reservado, 3->se registró entrada y se espera aceptación, 5->servicio activo, 7->solicitó su auto, 11->solicitud aprobada por el operador, cualquier otro->sin proceso
							case -1:
								// todo: cerrar sesión, quitar datos, enviar a login
								SessionManager.logOut();
								res = new NavigationPage(new P_LoginInicio());
								break;
								
							case 0:
								if (currentPage != "Veepe.P_MetodosPago")
									res = new P_Home(new P_MetodosPago());
								break;

							case 1:
								// obtenemos la info de la reserva
								WsResponse<Reserva> items4 = await WsConsumer.Consume<Reserva>("operations", "getReserva", "{\"idUsuario\":\"" + (Application.Current as App).usuario.idUsuario +  "\"}");
								if (items4 != null && items4.respuesta == "ok")
								{
									(Application.Current as App).usuario.tieneReserva = true;
									(Application.Current as App).usuario.reserva = items4.datos;
								}
								else
								{
									(Application.Current as App).usuario.tieneReserva = false;
									(Application.Current as App).usuario.reserva = null;
								}

								if (currentPage != "Veepe.P_HomeDetail")
									res = new P_Home(new P_HomeDetail());
								break;

							case 3:
								// hay una operación por aceptar -> obtener info de dicha operacion
								WsResponse<sor> items2 = await WsConsumer.Consume<sor>("operations", "getServicioActivo", "{\"veepeid\":\"" + usuario.veepeId + "\"}");

								if (items2 != null)
								{
									if (items2.respuesta == "ok")
									{
										if (items2.datos != null)
										{
											// hay una nueva operación pendiente!
											if (currentPage != "Veepe.P_TicketServicio")
												res = new NavigationPage(new P_TicketServicio(items2.datos));	// *************************
										}
										else
										{
											// si hay un error -> default a home normal
											if (currentPage != "Veepe.P_HomeDetail")
												res = new P_Home(new P_HomeDetail());
										}
									}
									else
									{
										// si hay un error -> default a home normal
										if (currentPage != "Veepe.P_HomeDetail")
											res = new P_Home(new P_HomeDetail());
									}
								}
								else
								{
									// si hay un error -> default a home normal
									if (currentPage != "Veepe.P_HomeDetail")
										res = new P_Home(new P_HomeDetail());
								}
								break;

							case 5:
								if (currentPage != "Veepe.P_ServicioActivo")
									res = new P_Home(new P_ServicioActivo());
								break;

							case 7:
								if (currentPage != "Veepe.P_ServicioActivo")
								res = new P_Home(new P_ServicioActivo());
								break;

							case 11:
								if (currentPage != "Veepe.P_AutoSolicitado")
								res = new P_Home(new P_AutoSolicitado());
								break;

							case 13:
								WsResponse<sor> items3 = await WsConsumer.Consume<sor>("data", "getOperacionesPendientes", "{\"veepeid\":\"" + usuario.veepeId + "\", \"incluirEvaluacion\":\"true\"}");
								if (currentPage != "Veepe.P_CalificarServicio")
								res = new P_Home(new P_CalificarServicio(items3.datos));
								break;

							default:
								if (currentPage != "Veepe.P_HomeDetail")
									res = new P_Home(new P_HomeDetail());
								break;
						}
					}
					else
					{
						Reporter.ReportError("getStatusUsuario regresó un error: " + items.datos + ", " + items.mensaje + ", " + items.respuesta);
						if (currentPage != "Veepe.P_HomeDetail")
							res = new P_HomeDetail();
					}
				}
				else
				{
					// si sucedió un error de red, defaulteamos al login
					/*if (currentPage != "Veepe.P_HomeDetail")
						res = new P_HomeDetail();*/
					if (currentPage != "Veepe.P_LoginInicio")
						res = new NavigationPage(new P_LoginInicio());
				}
			}
			else
			{
				// ni si quiera está loggeado
				if (currentPage != "Veepe.P_LoginInicio")
					res = new NavigationPage(new P_LoginInicio());
			}

			return res;
		}

		protected override void OnStart()
		{
			CrossPushNotification.Current.Register();
			if (usuario != null && SessionManager.isLogged())
			{
				//CrossPushNotification.Current.Unregister();
			}
		}

		protected override void OnSleep()
		{
		}

		protected override void OnResume()
		{
			if (usuario != null)
			{
				// está loggeado, pero es un uf?
				if (usuario.rol == Usuario.ROL_USUARIO_FINAL)
				{
					Device.BeginInvokeOnMainThread(async () =>
					{
						await updateHomePage();
					});
				}
			}

		} // fin de OnResume

	}
}
