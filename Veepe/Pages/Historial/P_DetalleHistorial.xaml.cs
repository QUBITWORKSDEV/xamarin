﻿using Xamarin.Forms;

namespace Veepe
{
	public partial class P_DetalleHistorial : FunkContentPage
	{
		sor simpleOperation;

		public P_DetalleHistorial (sor pSor)
		{
			InitializeComponent ();
			checkConnectivity();
			simpleOperation = pSor;

			BindingContext = simpleOperation;
			lblWarningNoConnectivity.BindingContext = this;

			// actualizamos las estrellas
			for (int i = 1; i <= simpleOperation.operacion.Rating; i++)
			{
				this.FindByName<Image>("estrella" + i).Source = "estrellas_a.png";
			}
			for (int i = simpleOperation.operacion.Rating+1; i <= 5; i++)
			{
				this.FindByName<Image>("estrella" + i).Source = "estrellas_n.png";
			}
		}
	}
}

