﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_Historial : FunkContentPage
	{
		List<sor> historial;

		public P_Historial ()
		{
			InitializeComponent();
			checkConnectivity();
		}

		async void refresh()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;
				WsResponse<sor[]> items = await WsConsumer.Consume<sor[]>("data", "getHistorial", "{\"veepeId\":\"" + (Application.Current as App).usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						historial = new List<sor>();
						for (int i = 0; i < items.datos.Length; i++)
						{
							historial.Add(items.datos[i]);
						}

						laLista.ItemsSource = historial;
						laLista.IsVisible = !(historial.Count==0);
						laLista_nodata.IsVisible = !laLista.IsVisible;
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener el historial", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			refresh();
		}

		async void OnHistoriaClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				int idOperacion = int.Parse((sender as Button).CommandParameter.ToString());

				// buscamos el historial en cuestión
				int indice = -1;
				for (int i = 0; i < historial.Count; i++)
				{
					if (historial[i].operacion.idOperacion == idOperacion)
					{
						indice = i;
						i = historial.Count;
					}
				}

				if (indice > -1)
					await Navigation.PushAsync(new P_DetalleHistorial(historial[indice]));
			}
		}

	}
}

