﻿using FunkFramework;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_HomeMaster : ContentPage
	{


		public P_HomeMaster ()
		{
			InitializeComponent ();
			txtVersion.Text = "v. " + GlobalSettings.currentVersion;
		}

		public ListView _Menu
		{
			get
			{
				return Menu;
			}
		}
	}

}

