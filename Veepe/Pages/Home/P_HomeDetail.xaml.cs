﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.Generic;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;
using FunkFramework.UI;
using FunkFramework;
using FunkFramework.Location;
using FunkFramework.Json;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_HomeDetail : FunkContentPage
	{
		// ___________________________________________________________________________________________________________________________________________________ Atributos
		Estacionamiento[] estacionamientos;						// arreglo de todos los estacionamientos
		Estacionamiento estacionamientoSeleccionado = null;		// el estacionamiento seleccionado
		FunkMap Mapa;											// el mapa
		double lat = 19.4326106;								// latitud default si no se puede obtener la posición
		double lng = -99.1331899;								// longitud default si no se puede obtener la posición
		double[] pos;											// la posición actual del usuario

		// ___________________________________________________________________________________________________________________________________________________ Constructore
		public P_HomeDetail ()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = (Application.Current as App).usuario;
			lblReservaActiva.BindingContext = (Application.Current as App).usuario;
			lblWarningNoConnectivity.BindingContext = this;

			Device.BeginInvokeOnMainThread( async () => {
				await init();
			});
		}


		// ___________________________________________________________________________________________________________________________________________________ Métodos
		/// <summary>
		/// Inicializa la pantalla. Esto incluye pedir permisos para acceder a la ubicación, cargar el mapa</summary>
		async Task init()
		{
			(Application.Current as App).playJingle();

			PermissionStatus status = PermissionStatus.Unknown;

			// tratamos de obtener los permisos
			try
			{
				status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
				if (status != PermissionStatus.Granted)
				{
					if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
					{
						await DisplayAlert("Permiso requerido", "Necesitas permitir el acceso a tu ubicación.", "Ok");
					}

					var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Location });
					status = results[Permission.Location];
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.ToString());
			}

			try
			{
				if (status == PermissionStatus.Granted)
					pos = DependencyService.Get<CrossLocationManager>().getLocation();
				else
					pos = new double[] { lat, lng };
			}
			catch
			{
				pos = new double[] { lat, lng };
			}

			// creamos el mapa
			Mapa = new FunkMap();
			Mapa.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pos[0], pos[1]), Distance.FromKilometers(1)));
			Mapa.MapType = MapType.Street;
			Mapa.HorizontalOptions = LayoutOptions.FillAndExpand;
			Mapa.VerticalOptions = LayoutOptions.FillAndExpand;
			Mapa.IsShowingUser = true;
			var elGrid = this.FindByName<Grid>("GridMapa");
			elGrid.Children.Add(Mapa, 0, 0);
			MapIndicator.IsEnabled = false;
			MapIndicator.IsVisible = false;
			elGrid.ForceLayout();

			await updateMapList();
		} // fin de init()


		/// <summary>
		/// Obtiene la lista de estacionamientos</summary>
		async Task updateMapList()
		{
			if (isOffline)
				return;
			
			WsResponse<Estacionamiento[]> items = await WsConsumer.Consume<Estacionamiento[]>("data", "getEstacionamientos", "{}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					estacionamientos = items.datos;

					Mapa.Pins.Clear();
					List<CustomPin> listaPins = new List<CustomPin>();
					for (int i = 0; i < estacionamientos.Length; i++)
					{
						CustomPin p = new CustomPin();
						p.Pin = new Pin()
						{
							Type = PinType.Place,
							Position = new Position(estacionamientos[i].Latitud, items.datos[i].Longitud),
							Label = estacionamientos[i].Nombre,
							Address = estacionamientos[i].Direccion
						};
						p.PinClicked += PinClicked;

						listaPins.Add(p);
						Mapa.Pins.Add(p.Pin);
					}
					Mapa.CustomPins = listaPins;
					Mapa.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pos[0], pos[1]), Distance.FromKilometers(getMapDefaultZoom())));
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al obtener la lista de estacionamientos.", "Aceptar");
				}
			}
			else
			{
				await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
			}

			estacionamientoSeleccionado = null;
		} // fin de UpdateMapList


		/// <summary>
		/// Calcula la distancia mínima entre el usuario y el estacionamiento más cercano</summary>
		/// <returns>
		/// Regresa la distancia minima en kilómetros</returns>
		float getMapDefaultZoom()
		{
			float res = 1000;

			// recorremos los estacionamientos y vamos calculando distancias
			foreach (Estacionamiento e in estacionamientos)
			{
				double distancia = Math.Sqrt( Math.Pow(e.Latitud-pos[0], 2) + Math.Pow(e.Longitud-pos[1], 2) );
				if (distancia < res)
					res = (float)distancia;
			}

			return res * 113f;	// porque un grado de lat/long equivale a ~113 km
		}


		/// <summary>
		/// Muestra el detalle de un estacionamiento (click en el pin)</summary>
		/// <param name="sender"> El objeto (pin) que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		public async void PinClicked(object sender, CustomPin pin)
		{
			// obtenemos qué estacionamiento fue clicado
			for (int i = 0; i < estacionamientos.Length; i++)
			{
				if (estacionamientos[i].Nombre == pin.Pin.Label)
					estacionamientoSeleccionado = estacionamientos[i];
			}

			if (estacionamientoSeleccionado != null)
			{
				// Mostramos el detalle del estacionamiento
				await Navigation.PushAsync(new P_DetallesEstacionamiento(estacionamientoSeleccionado));
			}
			else
			{
				Reporter.ReportError("Se hizo clic en un pin que no aparece en la lista de estacionamientos.");
			}
		}


		/// <summary>
		/// Mueve la posición del mapa a la ubicación actual del usuario</summary>
		async void gotoUserPosition()
		{
			PermissionStatus status = PermissionStatus.Unknown;

			// tratamos de obtener los permisos
			try
			{
				status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
				if (status != PermissionStatus.Granted)
				{
					if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
					{
						await DisplayAlert("Permiso requerido", "Necesitas permitir el acceso a tu ubicación.", "Ok");
					}

					var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Location });
					status = results[Permission.Location];
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.ToString());
			}

			if (status == PermissionStatus.Granted)
				pos = DependencyService.Get<CrossLocationManager>().getLocation();
			else
				pos = new double[] { lat, lng };

			Mapa.MoveToRegion(new MapSpan(new Position(pos[0], pos[1]), .015, .015));
		}


		/// <summary>
		/// Actualiza la lista de sugerencias de lugares con base en el texto del campo</summary>
		/// <param name="sender"> El objeto que disparó el evento (el campo de búsqueda)</param>
		/// <param name="e"> Parámetros del evento</param>
		async void UpdateSearch(object sender, TextChangedEventArgs e)
		{
			activityIndicator.IsVisible = true;

			System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
			string strResults = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/autocomplete/json?key="+GlobalSettings.googleWebAkiKey+"&location=19.370280,-99.141756&radius=40000&input=" + e.NewTextValue);
			LocationPredictionResult results = Jsoner.deserialize<LocationPredictionResult>(strResults);
			client.Dispose();

			if (results.status.ToLower() == "ok")
			{
				// mostrar los resultados
				lstSugerencias.RowHeight = 48;
				lstSugerencias.HeightRequest = results.predictions.Count * 48;
				lstSugerencias.ItemsSource = results.predictions;
			}
			else if (results.status.ToLower() == "zero_results")
			{
				// TODO: mostrar que no hay resultados
			}
			else
			{
				// hay un error. notificar el status a devs
				Reporter.ReportError("Hubo un error al obtener las sugerencias de ubicaciones: " + strResults);
			}

			activityIndicator.IsVisible = false;
		} // fin del método UpdateSearch


		// ___________________________________________________________________________________________________________________________________________________ Listeners
		/// <summary>
		/// Manda a la posición actual cuando el usuario presiona el botón de ubicación actual</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		void OnUbicacionActualClicked(object sender, EventArgs e)
		{
			gotoUserPosition();
		}


		/// <summary>
		/// Carga la pantalla P_VeepeId</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		void OnVeepeIdClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				((Application.Current as App).MainPage as MasterDetailPage).Detail = new P_VeepeId();
			}
		}


		/// <summary>
		/// Al hacer tap en una de las sugerencias, mueve la posición del mapa a esa ubicación</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		async void OnSugerenciaTapped(object sender, ItemTappedEventArgs e)
		{
			// obtenemos la info del lugar
			string place_id = (e.Item as LocationPrediction).place_id;

			System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
			string strResults = await client.GetStringAsync("https://maps.googleapis.com/maps/api/place/details/json?key="+GlobalSettings.googleWebAkiKey+"&placeid=" + place_id);
			LocationDetail results = Jsoner.deserialize<LocationDetail>(strResults);
			client.Dispose();

			if (results.status.ToLower() == "ok")
			{
				Pin p = new Pin();
				p.Position = new Position(results.result.geometry.location.lat, results.result.geometry.location.lng);
				p.Label = "-";
				Mapa.Pins.Add(p);

				// obtenemos el zoom que nos muestra el estacionamiento más cercano
				float res = 1000;	// default, si falla
				// recorremos los estacionamientos y vamos calculando distancias
				foreach (Estacionamiento estacionamiento in estacionamientos)
				{
					double distancia = Math.Sqrt( Math.Pow(estacionamiento.Latitud-results.result.geometry.location.lat, 2) + Math.Pow(estacionamiento.Longitud-results.result.geometry.location.lng, 2) );
					if (distancia < res)
						res = (float)distancia;
				}

				Mapa.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(results.result.geometry.location.lat, results.result.geometry.location.lng), Distance.FromKilometers(res*113f)));
			}
			else
			{
			}

			search.Text = "";
			lstSugerencias.ItemsSource = null;
			lstSugerencias.HeightRequest = 0;

			// fix para ios: no pierde el foco automáticamente
			{
				searchGrid.HorizontalOptions = LayoutOptions.Center;
				stack1.VerticalOptions = LayoutOptions.EndAndExpand;
				stack1.BackgroundColor = Color.Transparent;
			}
			search.Unfocus();
			Mapa.Focus();
		}


		/// <summary>
		/// Al obtener el foco, el campo de búsqueda aumenta de tamaño</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		void OnSearchFocused(object sender, FocusEventArgs e)
		{
			// lo hacemos grande
			searchGrid.HorizontalOptions = LayoutOptions.Fill;
			stack1.VerticalOptions = LayoutOptions.Fill;
			stack1.BackgroundColor = Color.FromHex("#080a19");
		}


		/// <summary>
		/// Al perder el foco, el campo de búsqueda regresa a su tamaño original</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		void OnSearchUnfocused(object sender, FocusEventArgs e)
		{
			// lo hacemos chiquito
			searchGrid.HorizontalOptions = LayoutOptions.Center;
			stack1.VerticalOptions = LayoutOptions.EndAndExpand;
			stack1.BackgroundColor = Color.Transparent;
		}


		/// <summary>
		/// Click en el tache del campo de búsqueda, para limpiar la búsqueda</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		void OnLimpiarClicked(object sender, EventArgs e)
		{
			search.Text = "";
			lstSugerencias.ItemsSource = null;
			lstSugerencias.HeightRequest = 0;

			// fix para ios: no pierde el foco automáticamente
			{
				searchGrid.HorizontalOptions = LayoutOptions.Center;
				stack1.VerticalOptions = LayoutOptions.EndAndExpand;
				stack1.BackgroundColor = Color.Transparent;
			}
			search.Unfocus();
			Mapa.Focus();

			List<SugerenciasMapViewModel> lista = new List<SugerenciasMapViewModel>();
			lstSugerencias.ItemsSource = lista;
		}


		/// <summary>
		/// Listener del evento que se dispara cuando se muestra la pantalla</summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;

			// actualizamos el estado del banner de reservaciones
			if ((Application.Current as App).usuario.tieneReserva)
			{
				lblReservaActiva.Text = "Tu reserva en " + (Application.Current as App).usuario.reserva.NombreEstacionamiento + " expira a las " + (Application.Current as App).usuario.reserva.Hora.ToString("HH:mm");
			}
		}

	}
}
