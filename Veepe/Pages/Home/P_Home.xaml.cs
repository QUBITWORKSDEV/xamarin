﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_Home : MasterDetailPage
	{
		public P_Home()
		{
			InitializeComponent();

			pHomeMaster._Menu.ItemSelected += (sender, e) => {
				NavigateTo (e.SelectedItem as MenuItem);
			};
		}

		public P_Home(Page detailPage)
		{
			InitializeComponent();

			pHomeMaster._Menu.ItemSelected += (sender, e) =>
			{
				NavigateTo(e.SelectedItem as MenuItem);
				//((ListView)sender).SelectedItem = null;
			};

			Detail = new NavigationPage(detailPage);
		}

		async void NavigateTo(MenuItem item)
		{
			// es una navigation page o es una push?
			if ((Detail as NavigationPage) != null)
			{
				Type current = (Detail as NavigationPage).CurrentPage?.GetType();
				Type selected = item.TargetType;

				if (current != null && selected != null && current.ToString() == selected.ToString())
				{
					// es la misma pantalla -> sólo esconder el menú
				}
				else
				{
					if (item.Title == "Inicio" && (Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
					{
						(Application.Current as App).MainPage = await (Application.Current as App).getHomeUsuario();
					}
					else
					{
						Page displayPage = (Page)Activator.CreateInstance(item.TargetType);
						//(Application.Current as App).navbarColor = Color.FromHex("#414042");
						Detail = new Veepe.P_Home(displayPage);
					}
				}
			}
			else
			{
				// es una push, como la de veepeid
				if (item.Title == "Inicio" && (Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
				{
					(Application.Current as App).MainPage = await (Application.Current as App).getHomeUsuario();
				}
				else
				{
					Page displayPage = (Page)Activator.CreateInstance(item.TargetType);
					//(Application.Current as App).navbarColor = Color.FromHex("#414042");
					Detail = new Veepe.P_Home(displayPage);
				}
			}

			IsPresented = false;
		}
	}
}
