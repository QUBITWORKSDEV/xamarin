﻿using System;
using System.Collections.ObjectModel;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_OpHomeDetail : FunkContentPage
	{
		// _______________________________________________________________________________ Atributos privados
		Collection<AutosOperacionViewModel> _autos;		// lista de operaciones
		bool canUpdate = true;							// bandera para indicar si es válido refrescar el estado de las operaciones
		bool yaTieneTimer = false;
		int entregasActuales = 0;
		string filtroActual = "todo";					// el filtro actual

		// _______________________________________________________________________________ Atributos públicos
		public ObservableCollection<AutosOperacionViewModel> autos { get; set; }
		public EstadisticasOperadorViewModel estadisticas { get; set; }

		// _______________________________________________________________________________ Constructor
		public P_OpHomeDetail()
		{
			InitializeComponent();
			BindingContext = this;
			lblWarningNoConnectivity.BindingContext = this;

			// si el usuario es jefe de estacionamiento, nostramos los botones
			if ((Application.Current as App).usuario.rol == Usuario.ROL_JEFE_ESTACIONAMIENTO)
			{
				cmdGenerarReporte.IsVisible = true;
				cmdControlPersonal.IsVisible = true;
			}
			lblNombreOperador.Text = (Application.Current as App).usuario.nombreSemicompleto + "  [" + (Application.Current as App).usuario.nombreEstacionamiento + "]";

			// bindeamos la lista de operaciones
			autos = new ObservableCollection<AutosOperacionViewModel>();
			_autos = new Collection<AutosOperacionViewModel>();
			estadisticas = new EstadisticasOperadorViewModel();

			laLista.ItemsSource = autos;

			// ponemos las estadísticas
			txtEstadisticaCapacidadTotal.BindingContext = estadisticas;
			txtEstadisticaPensiones.BindingContext = estadisticas;
			txtEstadisticaReserva.BindingContext = estadisticas;
			txtEstadisticaDisponibleGeneral.BindingContext = estadisticas;
			txtEstadisticaPensionesDisponibles.BindingContext = estadisticas;

			laLista.ItemSelected += LaLista_ItemSelected;
		}

		void LaLista_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			(sender as ListView).SelectedItem = null;
		}

		// _______________________________________________________________________________ Métodos
		void AddCarToModel(AutosOperacionViewModel elCarro)
		{
			_autos.Add(elCarro);
			autos.Add(elCarro);
		}

		public async void actualizar()
		{
			int entregasNuevas = 0;
			if (!canUpdate) return;
			if (isOffline) return;

			WsResponse<dsr> items = await WsConsumer.Consume<dsr>("operations", "getStatusEstacionamiento", "{\"idEstacionamiento\":\"" + (Application.Current as App).usuario.idEstacionamiento + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					// actualizar las estadísticas
						// capacidades totales
					estadisticas.CapacidadTotal = items.datos.estadisticas.CapacidadTotal;
					estadisticas.DisponibleGeneral = items.datos.estadisticas.DisponibleGeneral;
						// pensiones
					estadisticas.Pensiones = items.datos.estadisticas.Pensiones;
					estadisticas.PensionesDisponibles = items.datos.estadisticas.PensionesDisponibles;
						// reservas

					estadisticas.Reserva = items.datos.estadisticas.Reserva;

					// la lista de vehículos y sus status están en itemn.lista
					autos.Clear();
					_autos.Clear();

					for (int i = 0; i < items.datos.operaciones.Length; i++)
					{
						AddCarToModel(items.datos.operaciones[i]);
						if (items.datos.operaciones[i].Status == 5)
							entregasNuevas++;
					}
					FilterCarsInModel(filtroActual);

					// si hay solicitudes nuevas de autos, enviar una notificación local con audio
					if (entregasNuevas > entregasActuales)
					{
						(Application.Current as App).playJingle();
					}

					Toast.MakeText(ApplicationContext, message, ToastLength.Long).Show();

					// si hay operaciones relacionadas a una pensión, mueve las estadísticas
					for (int i = 0; i < autos.Count; i++)
					{
						if (autos[i]._usaPension)
						{
							estadisticas.DisponibleGeneral++;
							estadisticas.PensionesDisponibles--;
						}
					}

					entregasActuales = entregasNuevas;
				}
				else
				{
					await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. (err1)", "Aceptar");

			if (canUpdate && !yaTieneTimer)
			{
				yaTieneTimer = true;
				Device.StartTimer(TimeSpan.FromSeconds(15), () =>
				{
					actualizar();
					return true;
				});
			}

		} // fin del método UpdateData()

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			canUpdate = false;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canUpdate = true;
			canEdit = true;

			actualizar();
		}

		void FilterCarsInModel(string filter)
		{
			canEdit = false;

			autos.Clear();
			foreach (AutosOperacionViewModel a in _autos)
			{
				filtroActual = filter;
				switch (filter)
				{
					case "reserva":
						if (a.Status == 1) autos.Add(a);
						break;

					case "entregados":
						//if (a.Status >= 7) autos.Add(a);
						if (a.Status >= 10) autos.Add(a);
						break;

					case "todo":
						if (a.Status < 10) autos.Add(a);
						//autos.Add(a);
						break;

					default:
						autos.Add(a);
						break;
				}
			}

			canEdit = true;
		} // filterCarsInModel


		void resetBotonesFiltros()
		{
			cmdVerReservas.BackgroundColor = Color.FromHex("##00BFD6");
			cmdVerTodo.BackgroundColor = Color.FromHex("##00BFD6");
			cmdVerEntregas.BackgroundColor = Color.FromHex("##00BFD6");
			cmdVerReservas.TextColor = Color.White;
			cmdVerTodo.TextColor = Color.White;
			cmdVerEntregas.TextColor = Color.White;
		}

		// _______________________________________________________________________________ Listeners
		void OnVerReservasClicked(object sender, EventArgs e)
		{
			// actualizar visual de los botones
			resetBotonesFiltros();
			cmdVerReservas.BackgroundColor = Color.White;
			cmdVerReservas.TextColor = Color.FromHex("##00BFD6");

			FilterCarsInModel("reserva");
		}

		void OnVerTodoClicked(object sender, EventArgs e)
		{
			// actualizar visual de los botones
			resetBotonesFiltros();
			cmdVerTodo.BackgroundColor = Color.White;
			cmdVerTodo.TextColor = Color.FromHex("##00BFD6");

			FilterCarsInModel("todo");
		}

		void OnVerEntregasClicked(object sender, EventArgs e)
		{
			// actualizar visual de los botones
			resetBotonesFiltros();
			cmdVerEntregas.BackgroundColor = Color.White;
			cmdVerEntregas.TextColor = Color.FromHex("##00BFD6");

			FilterCarsInModel("entregados");
		}

		void OnEntregarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				string placa = (sender as Button).CommandParameter.ToString();
				bool encontrado = false;

				foreach (AutosOperacionViewModel a in autos)
				{
					if (a.Placa == placa && !encontrado)
					{
						// este es el que queremos sacar: a
						// Mostramos la pantalla de entrega y le pasamos el auto
						encontrado = true;
						Navigation.PushAsync(new P_OpEntregarAuto(a));
						return;
					}
				}
			}
		}

		void OnTiempoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				string placa = (sender as Button).CommandParameter.ToString();
				bool encontrado = false;
				foreach (AutosOperacionViewModel a in autos)
				{
					if (a.Placa == placa && !encontrado)
					{
						encontrado = true;
						Navigation.PushAsync(new P_OpTiempoEntrega(a));
						return;
					}
				}
			}
		}

		async void OnTicketClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				// mostrar el ticket de servicio
				canEdit = false;
				sor param = null;

				WsResponse<sor> items = await WsConsumer.Consume<sor>("operations", "getServicioActivoById", "{\"idOperacion\":\"" + (sender as Button).CommandParameter.ToString() + "\"}");
				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						param = items.datos;
						await Navigation.PushAsync(new P_TicketServicio(param, true));
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. (err2)", "Aceptar");
			}
		}

		async void OnIngresarAutoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				// mostraos la pantalla de ingreso
				await Navigation.PushAsync(new P_IngresoQRCode());
			}
		}

		async void OnPersonalClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				// abrimos la pantalla de control de personal
				await Navigation.PushAsync(new P_OpManejoPersonal());
			}
		}

		async void OnReporteClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await DisplayAlert("Reporte", "El reporte a la fecha ha sido enviado.", "Cerrar");
				canEdit = true;
			}
		}

		async void OnIngresarReservadoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				int idOperacion = (int)((sender as Button).CommandParameter);
				string nombrePersona = "";
				for (int i = 0; i < autos.Count; i++)
				{
					if (autos[i].idOperacion == idOperacion)
						nombrePersona = autos[i].Usuario;
				}

				// confirmar
				if (await DisplayAlert("", "¿Deseas ingresar el auto de " + nombrePersona, "Ingresar", "Cancelar"))
				{
					canEdit = false;

					// checamos si tiene pagos atrasados
					WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuarioByIdOperacion", "{\"idOperacion\":\"" + idOperacion + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							// checamos si ya está ingresado en otro estacionamiento
							if (items.datos.tieneOperacionActiva)
							{
								await DisplayAlert("", "Este usuario ya tiene una operación activa.", "Cerrar");
							}
							else if (items.datos.tienePagosAtrasados)
							{
								await DisplayAlert("", "Este usuario tienes pagos atrasados. No se puede ingresar.", "Cerrar");
							}
							else
								await Navigation.PushAsync(new P_SeleccionarAuto(idOperacion));
						}
						else
						{
							await DisplayAlert("Error", items.mensaje, "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

					canEdit = true;
				}
			}

		} // fin de OnIngresarReservadoClicked


	} // fin de la clase P_OpHome

}

