﻿using FunkFramework;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_OpHomeMaster : ContentPage
	{
		public P_OpHomeMaster()
		{
			InitializeComponent();
			txtVersion.Text = "v. " + GlobalSettings.currentVersion;
		}

		public ListView _Menu
		{
			get
			{
				return Menu;
			}
		}
	}
}

