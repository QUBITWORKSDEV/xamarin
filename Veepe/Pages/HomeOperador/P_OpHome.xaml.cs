﻿using System;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_OpHome : MasterDetailPage
	{
		public P_OpHome()
		{
			InitializeComponent();

			MasterBehavior = MasterBehavior.Popover;
		
			pOpHomeMaster._Menu.ItemSelected += (sender, e) => {
				NavigateTo(e.SelectedItem as MenuItem);
			};
		}

		void NavigateTo(MenuItem item)
		{
			Page displayPage = (Page)Activator.CreateInstance(item.TargetType);
			Detail = new NavigationPage(displayPage);

			IsPresented = false;
		}

	}
}



