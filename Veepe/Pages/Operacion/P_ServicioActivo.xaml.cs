﻿using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_ServicioActivo : FunkContentPage
	{
		// ___________________________________________________________________________________________________________________________ Atributos
		sor simpleOperation;
		Usuario u;
		bool shouldPool = true;		// variable que indica si debe checar con el servidor si hay cambios cada x tiempo. se pone en false cuando ya no queremos que se pregunte al servidor

		// ___________________________________________________________________________________________________________________________ Constructores
		public P_ServicioActivo()
		{
			InitializeComponent();
			checkConnectivity();

			BindingContext = this;
			cmdSolicitarAuto.BindingContext = this;

			// obtenemos la info
			getServicioActivo();
		}

		// ___________________________________________________________________________________________________________________________ Métodos
		/// <summary>
		/// Obtiene la información del servicio activo actual</summary>
		async void getServicioActivo()
		{
			if (isOffline)
				return;

			u = SessionManager.getLocalUser();
			WsResponse<Usuario> ite = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"idUsuario\":\"" + u.idUsuario + "\"}");
			if (ite != null)
			{
				if (ite.respuesta == "ok")
				{
					u = ite.datos;
				}
			}

			WsResponse<sor> items = await WsConsumer.Consume<sor>("operations", "getServicioActivo", "{\"veepeid\":\"" + u.veepeId + "\"}");
			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					simpleOperation = items.datos;

					// bindings
					txtNombreEstacionamiento.BindingContext = simpleOperation.estacionamiento;
					txtHoraEntrada.Text = simpleOperation.operacion.fechaIngreso.ToString("dd-MMM-yyyy H:mm");
					txtCostoHora.Text = "MXN " + simpleOperation.estacionamiento.TarifaHora + " / HR";
					txtTiempoEstimadoEntrega.Text = simpleOperation.operacion.tiempoAproximadoEntrega + " MIN";

					TimeSpan t = DateTime.Now - simpleOperation.operacion.fechaIngreso;
					txtTiempoTranscurrido.Text = ((t.Days*24)+t.Hours).ToString("D2") + ":" + t.Minutes.ToString("D2");
					txtCostoAproximado.Text = "MXN " + simpleOperation.operacion.getCostoAproximado( simpleOperation.estacionamiento.TarifaHora, u._pensionesContratadas, u._pensionesCompartidasContratadas ).ToString("N");

					if (simpleOperation.operacion.status == 5)
					{
						// fue solicitado pero no ha sido aceptado por el operador -> como no puede ser solicitado nuevamente:
						canEdit = false;
						esperarConfirmacionOperador();
					}

					Device.StartTimer(TimeSpan.FromSeconds(30), OnTimerTick);
				}
				else
				{
					await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
		}


		// ____________________________________________________________ esperando a que el operador conteste
		/// <summary>
		/// Inicia el loop de espera de confirmación del operador</summary>
		void esperarConfirmacionOperador()
		{
			Device.StartTimer(TimeSpan.FromSeconds(30), () =>
			{
				checarSiYaAceptoElOperador();
				return false;
			});
		}

		/// <summary>
		/// Core del loop de la espera de confirmación del operador</summary>
		async void checarSiYaAceptoElOperador()
		{
			if (isOffline)
				return;

			activityIndicator.IsVisible = true;

			WsResponse<sor> items = await WsConsumer.Consume<sor>("operations", "getOperacion", "{\"idOperacion\":\"" + simpleOperation.operacion.idOperacion + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					if (items.datos.operacion.status == 7)
					{
						(Application.Current.MainPage as P_Home).Detail = new NavigationPage(new P_AutoSolicitado());
					}
					else
					{
						// lo volvemos a intentar
						esperarConfirmacionOperador();
					}
				}
				else
				{
					await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

			activityIndicator.IsVisible = false;
		}


		// ___________________________________________________________________________________________________________________________ Listeners
		/// <summary>
		/// Al mostrarse la pantalla, se permite que se esté checando el tiempo</summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			shouldPool = true;
			canEdit = true;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			shouldPool = false;
		}

		async void OnVerTicketServicioClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_TicketServicio(simpleOperation, true));
			}
		}

		bool OnTimerTick()
		{
			TimeSpan t = DateTime.Now - simpleOperation.operacion.fechaIngreso;

			txtTiempoTranscurrido.Text = ((t.Days * 24) + t.Hours).ToString("D2") + ":" + t.Minutes.ToString("D2");
			txtCostoAproximado.Text = "MXN " + simpleOperation.operacion.getCostoAproximado(simpleOperation.estacionamiento.TarifaHora, u._pensionesContratadas).ToString("N");

			return shouldPool;
		}

		async void OnSolicitarAutoClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (await DisplayAlert("", "¿Deseas solicitar tu auto en este momento?", "Solicitar", "Cancelar"))
				{
					// enviamos al ws la solicitud de salida
					WsResponse<int> items = await WsConsumer.Consume<int>("operations", "solicitarAuto", "{\"idOperacion\":\"" + simpleOperation.operacion.idOperacion + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							switch (items.datos)
							{
								case -1:	// sucedió un error
									canEdit = true;
									break;
								
								case 0:		// no tiene método de pago
									await DisplayAlert("", "Para poder solicitar tu auto por favor registra un método de pago.", "Aceptar");
									await Navigation.PushAsync(new P_AgregarMetodoPago());
									break;

								case 1:		// alles gut
									await DisplayAlert("", "Su solicitud ha sido enviada. Por favor espere la respuesta del operador.", "Aceptar");
									break;
							}
						}
						else
						{
							await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

					//canEdit = true;	// en este caso no rehabilitamos porque nos quedamos esperando la respuesta del operador
					activityIndicator.IsVisible = false;
				}
				else
				{
					canEdit = true;
					activityIndicator.IsVisible = false;
				}
			}
		} // fin de OnSolicitarAutoClicked

	}
}

