using System;
using System.Threading.Tasks;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_AutoSolicitado : FunkContentPage
	{
		sor simpleOperation;
		private bool shouldPool = true;

		public P_AutoSolicitado()
		{
			InitializeComponent();
			checkConnectivity();
		}

		async Task actualizar()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				WsResponse<sor> items = await WsConsumer.Consume<sor>("operations", "getServicioActivo", "{\"veepeid\":\"" + (Application.Current as App).usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						simpleOperation = items.datos;

						// bindings
						txtNombreEstacionamiento.BindingContext = simpleOperation.estacionamiento;
						txtTarifaEstacionamiento.BindingContext = simpleOperation.estacionamiento;
						txtHoraEntrada.Text = simpleOperation.operacion.fechaIngreso.ToString("dd-MMM-yyyy H:mm");

						// calculamos el tiempo y el costo
						// tiempo para que se recoja
						TimeSpan ts = simpleOperation.operacion.HoraEstimadaEntrega - DateTime.Now;
						txtTiempoEspera.Text = ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");

						// tiempo dentro del estacionamiento
						ts = DateTime.Now - simpleOperation.operacion.fechaIngreso;
						txtTiempoTranscurrido.Text = ((ts.Days * 24) + ts.Hours).ToString("D2") + ":" + ts.Minutes.ToString("D2") + " HRS";
						txtCostoEstimado.Text = "MXN " + simpleOperation.operacion.getCostoAproximado(simpleOperation.estacionamiento.TarifaHora, (Application.Current as App).usuario._pensionesContratadas, (Application.Current as App).usuario._pensionesCompartidasContratadas);

						Device.StartTimer(TimeSpan.FromSeconds(1), OnTimerTick);
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
			
				canEdit = true;
			}
		}


		bool OnTimerTick()
		{
			// tiempo para que se recoja
			TimeSpan ts = simpleOperation.operacion.HoraEstimadaEntrega - DateTime.Now;
			if (ts.TotalSeconds > 0)
				txtTiempoEspera.Text = ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
			else
			{
				txtTiempoEspera.Text = "00:00";
				reingresarAuto();
			}

			// tiempo dentro del estacionamiento
			TimeSpan ts2 = DateTime.Now - simpleOperation.operacion.fechaIngreso;
			txtTiempoTranscurrido.Text = ((ts2.Days * 24) + ts2.Hours).ToString("D2") + ":" + ts2.Minutes.ToString("D2") + " HRS";
			txtCostoEstimado.Text = "MXN " + simpleOperation.operacion.getCostoAproximado( simpleOperation.estacionamiento.TarifaHora ).ToString("N");

			return shouldPool;
		}


		async void reingresarAuto()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "reingresarAuto", "{\"idOperacion\":\"" + simpleOperation.operacion.idOperacion + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos == true)
						{
							await (Application.Current as App).StartupSequence();
							//(Application.Current as App).MainPage = new P_CalificarServicio(items.datos);
						}
						else
							await DisplayAlert("", "Sucedió un error al reingresart el auto. Err 1", "Cerrar");
					}
					else
						await DisplayAlert("", "Sucedió un error al reingresart el auto. Err 2", "Cerrar");
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
			}

			canEdit = true;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			shouldPool = true;
			canEdit = true;

			Device.BeginInvokeOnMainThread(async () =>
			{
				await actualizar();
			});

		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			shouldPool = false;
		}


		// _________________________________________ DUMMIES

		/// <summary>
		/// Actualiza el estado del usuario, por si recibe una nueva notificación. Es un método TEMPORAL </summary>
		/*async void refreshStatus_killMe()
		{
			if (isOffline)
				return;
			
			WsResponse<sor> items = await WsConsumer.Consume<sor>("operations", "getDummieAutoEntregado", "{\"veepeid\":\"" + (Application.Current as App).usuario.veepeId + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					if (items.datos != null)
					{
						if (items.datos.operacion.status == 10)
						{
							// ya se entregó y sólo falta que se califique el servicio
							shouldPool = false;
							(Application.Current as App).MainPage = new P_CalificarServicio(items.datos);
						}
					}
					else
					{
						Device.StartTimer(TimeSpan.FromSeconds(30), () =>
						{
							refreshStatus_killMe();
							return false;
						});
					}
				}
				else
				{
					//await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					Device.StartTimer(TimeSpan.FromSeconds(30), () =>
					{
						refreshStatus_killMe();
						return false;
					});
				}
			}
			else
			{
				//await DisplayAlert("Error de red", "EL SERVIDOR NISIQUIERA RESPONDIO", "Aceptar");
				Device.StartTimer(TimeSpan.FromSeconds(30), () =>
				{
					refreshStatus_killMe();
					return false;
				});
			}
		}*/

	}
}

