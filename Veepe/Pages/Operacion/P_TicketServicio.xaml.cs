﻿using System;
using System.Threading.Tasks;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_TicketServicio : FunkContentPage
	{
		// ___________________________________________________________________________________________________________________________ Atributos
		sor simpleOperationResponse;
		bool canRefresh = true;

		// ___________________________________________________________________________________________________________________________ Constructores
		public P_TicketServicio() { }

		public P_TicketServicio(sor pSor, bool soloVisualizacion = false)
		{
			simpleOperationResponse = pSor;

			InitializeComponent();
			checkConnectivity();

			txtNombreEstacionamiento.BindingContext = simpleOperationResponse.estacionamiento;

			txtEntrada.BindingContext = simpleOperationResponse.operacion;
			txtLentes.BindingContext = simpleOperationResponse.operacion;
			txtLaptop.BindingContext = simpleOperationResponse.operacion;
			txtTablet.BindingContext = simpleOperationResponse.operacion;
			txtCelular.BindingContext = simpleOperationResponse.operacion;
			txtValores.BindingContext = simpleOperationResponse.operacion;
			txtObservaciones.BindingContext = simpleOperationResponse.operacion;

			txtPrecio.BindingContext = simpleOperationResponse.estacionamiento;
			txtTarifaEstacionamiento.BindingContext = simpleOperationResponse.estacionamiento;

			txtMarca.BindingContext = simpleOperationResponse.auto;
			txtSubmarca.BindingContext = simpleOperationResponse.auto;
			txtColor.BindingContext = simpleOperationResponse.auto;
			txtPlaca.BindingContext = simpleOperationResponse.auto;

			// las fotos
			if (simpleOperationResponse.operacion._fotoIzquierda != null) fotoIzquierda.Source = new UriImageSource { Uri = new Uri(GlobalSettings.serverUri + (simpleOperationResponse.operacion._fotoIzquierda).Replace(".jpg", "_thumb.jpg")), CachingEnabled = false };
			if (simpleOperationResponse.operacion._fotoDelantera != null) fotoDelantera.Source = new UriImageSource { Uri = new Uri(GlobalSettings.serverUri + (simpleOperationResponse.operacion._fotoDelantera).Replace(".jpg", "_thumb.jpg")), CachingEnabled = false };
			if (simpleOperationResponse.operacion._fotoDerecha != null) fotoDerecha.Source = new UriImageSource { Uri = new Uri(GlobalSettings.serverUri + (simpleOperationResponse.operacion._fotoDerecha).Replace(".jpg", "_thumb.jpg")), CachingEnabled = false };
			if (simpleOperationResponse.operacion._fotoTrasera != null) fotoTrasera.Source = new UriImageSource { Uri = new Uri(GlobalSettings.serverUri + (simpleOperationResponse.operacion._fotoTrasera).Replace(".jpg", "_thumb.jpg")), CachingEnabled = false };

			if (pSor.operacion.status != 3) 
				cmdAceptar.IsVisible = false;

			if (soloVisualizacion)
			{
				// sólo estamos visualizando el ticket
				lblDisclaimer.IsVisible = false;
				cmdAceptar.IsVisible = false;
			}
			else
			{
				// visualizamos y aprobamos el ticket
				lblDisclaimer.IsVisible = true;
				cmdCerrar.IsVisible = false;
				validarExpiracion();
			}
		}


		// ___________________________________________________________________________________________________________________________ Métodos
		/// <summary>
		/// checa si ya pasaron 5 minutos desde que se generó el ticket. regresa true si ya pasaron 5 minutos o más -> autoaceptar</summary>
		void validarExpiracion()
		{
			TimeSpan ts = DateTime.Now - simpleOperationResponse.operacion.fechaIngreso;

			if (ts.TotalMinutes >= 5f)
			{
				// ya se pasó -> pasar a la siguiente etapa automáticamente
				Task.Run(async () => await aceptarTicket());
			}
			else
			{
				// volvemos a checar en un minuto, si seguimos vivos
				Device.StartTimer(TimeSpan.FromMinutes(1), () =>
				{
					if ( canRefresh )
						validarExpiracion();
					return false;
				});
			}
		}


		/// <summary>
		/// Acepta el ticket (ya sea por tiempo o manualmente</summary>
		async Task aceptarTicket()
		{
			SessionManager.setServicioActivo(simpleOperationResponse.operacion);

			canEdit = false;
			WsResponse<int> items = await WsConsumer.Consume<int>("operations", "aceptarTicket", "{\"idOperacion\":\"" + simpleOperationResponse.operacion.idOperacion + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					switch (items.datos)
					{
						case -1:
							await DisplayAlert("", "Lo sentimos, sucedió un error al procesar tu ticket. Sin embargo tu servicio está activo y puedes continuar con el proceso.", "Cerrar");
							break;

						case 0:
							await DisplayAlert("", "Gracias. Disfruta tu estancia.\nNo olvides dar de alta un método de pago ya que es necesario para poder retirar tu auto.", "Aceptar");
							await (Application.Current as App).updateHomePage();
							break;

						case 1:
							await DisplayAlert("", "Gracias. Disfruta tu estancia.", "Aceptar");
							await (Application.Current as App).updateHomePage();
							break;
					}
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
					canEdit = true;
				}
			}
			else
			{
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				canEdit = true;
			}

			//Application.Current.MainPage = new P_Home(new P_ServicioActivo());
		} // fin de aceptarTicket()


		// ___________________________________________________________________________________________________________________________ Listeners

		/// <summary>
		/// Al mostrarse la pantalla, se permite que se esté checando el tiempo</summary>
		protected override void OnAppearing()
		{
			base.OnAppearing();
			canRefresh = true;
			canEdit = true;
		}


		/// <summary>
		/// Al ocultarse la pantalla, se detiene que se esté checando el tiempo</summary>
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			canRefresh = false;
			canEdit = false;
		}


		/// <summary>
		/// Clic en terminos del estacionamiento</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		async void OnTerminosEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				//await Navigation.PushAsync(new P_TerminosEstacionamiento(simpleOperationResponse.estacionamiento));
				await Navigation.PushModalAsync(new P_TerminosEstacionamiento(simpleOperationResponse.estacionamiento, true));
			}
		}


		/// <summary>
		/// Clic en aceptar</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		async void OnAceptarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await aceptarTicket();
			}
		}


		/// <summary>
		/// Clic en cerrar</summary>
		/// <param name="sender"> El objeto que disparó el evento</param>
		/// <param name="e"> Parámetros del evento</param>
		async void OnCerrarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopAsync();
			}
		}

		void OnImagenClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				string url = "";

				switch ((sender as Button).CommandParameter.ToString().ToLower())
				{
					case "izquierda":
						url = GlobalSettings.serverUri + simpleOperationResponse.operacion._fotoIzquierda;
						break;
					case "delantera":
						url = GlobalSettings.serverUri + simpleOperationResponse.operacion._fotoDelantera;
						break;
					case "derecha":
						url = GlobalSettings.serverUri + simpleOperationResponse.operacion._fotoDerecha;
						break;
					case "trasera":
						url = GlobalSettings.serverUri + simpleOperationResponse.operacion._fotoTrasera;
						break;
				}

				Navigation.PushAsync(new P_VerFoto(url));
			}
		}

	}
}
