using System;
using Xamarin.Forms;
using FunkFramework.UI;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_DetallesEstacionamiento : FunkContentPage
	{
		readonly Estacionamiento estacionamiento;

		public P_DetallesEstacionamiento(Estacionamiento pEstacionamiento)
		{
			estacionamiento = pEstacionamiento;

			InitializeComponent();
			checkConnectivity();

			BindingContext = estacionamiento;
			lblWarningNoConnectivity.BindingContext = this;
			btnPensiones.BindingContext = estacionamiento;
			btnReservar.BindingContext = this;

			// mostramos ocultamos según los servicios que tiene este estacionamiento
			for (int i = 0; i < estacionamiento.Servicios.Length; i++)
			{
				if (estacionamiento.Servicios[i].idServicio != 5)
				{
					if (estacionamiento.Servicios[i].idServicio != 1 && estacionamiento.Servicios[i].idServicio != 2)
					{
						// cremamos el cuadro
						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Start;
						leyaut.BackgroundColor = Color.Transparent;
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						// creamos la imagen
						var imagen = new Image();
						imagen.Source = "ico_servicios_pension_n.png";
						imagen.WidthRequest = 48;
						imagen.HeightRequest = 48;
						imagen.HorizontalOptions = LayoutOptions.Start;

						// creamos el nombre
						var nombre = new FunkLabel();
						nombre.Text = estacionamiento.Servicios[i].nombre;
						nombre.TextColor = Color.FromHex("#A3A6A4");
						nombre.FontSize = 12;
						nombre.FontFamily = "BebasNeue";
						nombre.HorizontalOptions = LayoutOptions.Start;
						nombre.VerticalOptions = LayoutOptions.Center;

						// creamos el costo
						var costo = new FunkLabel();
						costo.Text = "MXN " + estacionamiento.Servicios[i].monto;
						costo.TextColor = Color.FromHex("#A3A6A4");
						costo.FontSize = 12;
						costo.FontFamily = "BebasNeue";
						costo.Margin = new Thickness(0, 0, 8, 0);
						costo.HorizontalOptions = LayoutOptions.EndAndExpand;
						costo.VerticalOptions = LayoutOptions.Center;

						leyaut.Children.Add(imagen);
						leyaut.Children.Add(nombre);
						leyaut.Children.Add(costo);

						ContenedorServicios.Children.Add(leyaut);
					}
					else
					{
						// cremamos el cuadro
						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Start;
						leyaut.BackgroundColor = Color.Transparent;
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						// creamos la imagen
						var imagen = new Image();

						if (estacionamiento.Servicios[i].idServicio == 1)
						{
							imagen.Source = "ico_servicios_pension_n.png";
						}
						else if (estacionamiento.Servicios[i].idServicio == 2)
						{
							imagen.Source = "ico_servicios_pensioncompartida_n.png";
						}

						imagen.WidthRequest = 48;
						imagen.HeightRequest = 48;
						imagen.HorizontalOptions = LayoutOptions.Start;

						// creamos el nombre
						var nombre = new FunkLabel();
						nombre.Text = estacionamiento.Servicios[i].nombre;
						nombre.TextColor = Color.FromHex("#A3A6A4");
						nombre.FontSize = 12;
						nombre.FontFamily = "BebasNeue";
						nombre.HorizontalOptions = LayoutOptions.Start;
						nombre.VerticalOptions = LayoutOptions.Center;

						leyaut.Children.Add(imagen);
						leyaut.Children.Add(nombre);

						ContenedorServicios.Children.Add(leyaut);
					}
				}
			}

			checkReserva();
		}

		void checkReserva()
		{
			if ( (Application.Current as App).usuario.tieneReserva )
				btnReservar.IsEnabled = false;
			else
			{
				// se puede hacer reserva en este momento?
				DateTime now = DateTime.Now;
				string[] aux = estacionamiento.HorarioServicio.Split('-');
				// el formato del horario es "7:00 - 17:00"
				DateTime horaIni = DateTime.Parse(aux[0].Replace(" ", ""));
				DateTime horaFin = DateTime.Parse(aux[1].Replace(" ", ""));
				horaFin = horaFin.AddMinutes(-30);
				horaFin = new DateTime(horaIni.Year, horaIni.Month, horaIni.Day, horaFin.Hour, horaFin.Minute, 0);
				btnReservar.IsEnabled = (now >= horaIni && now <= horaFin);
			}
		}

		async void OnReservarLugarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				activityIndicator2.IsVisible = true;

				if (await DisplayAlert("Confirmación", "Al reservar tendrás media hora para ingresar al estacionamiento seleccionado. ¿Deseas reservar?", "Reservar lugar", "Cancelar"))
				{
					WsResponse<Reserva> items = await WsConsumer.Consume<Reserva>("operations", "reservarEstacionamiento", "{\"veepeId\":\"" + (Application.Current as App).usuario.veepeId + "\", \"idEstacionamiento\":\"" + estacionamiento.idEstacionamiento + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							// si items.datos != null, se pudo hacer la reserva; si no, checar items.mensaje
							if (items.datos != null)
							{
								(Application.Current as App).usuario.tieneReserva = true;
								(Application.Current as App).usuario.reserva = items.datos;

								await Navigation.PushAsync(new P_TicketReserva(estacionamiento, (Application.Current as App).usuario.reserva.Hora));
							}
							else
							{
								(Application.Current as App).usuario.tieneReserva = false;
								(Application.Current as App).usuario.reserva = null;
								await DisplayAlert("", items.mensaje, "Aceptar");
							}
						}
						else
						{
							//await DisplayAlert("Error", "Sucedió un error al solicitar la reservación.", "Aceptar");
							(Application.Current as App).usuario.tieneReserva = false;
							(Application.Current as App).usuario.reserva = null;
							await DisplayAlert("", items.mensaje+"", "Aceptar");
						}
					}
					else
					{
						(Application.Current as App).usuario.tieneReserva = false;
						(Application.Current as App).usuario.reserva = null;
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
					}

				}
				canEdit = true;
				activityIndicator2.IsVisible = false;
			}
		}

		async void OnTerminosEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_TerminosEstacionamiento(estacionamiento));
			}
		}

		void OnNavegarWazeClicked(object sender, EventArgs e)
		{
			var address = estacionamiento.Direccion;

			try
			{
				Device.OpenUri(new Uri("waze://?ll=" + estacionamiento.Latitud + "," + estacionamiento.Longitud + "&navigate=yes"));
			}
			catch
			{
				//if ( Device.RuntimePlatform == Device.iOS )
				if ( Device.RuntimePlatform == Device.iOS )
					Device.OpenUri(new Uri("http://itunes.apple.com/us/app/id323229106"));
				//else if ( Device.RuntimePlatform == Device.Android )
				else if ( Device.RuntimePlatform == Device.Android )
				{
					try
					{
						Device.OpenUri(new Uri("market://details?id=com.waze"));
					}
					catch
					{
						Device.OpenUri(new Uri("http://play.google.com/store/apps/details?id=com.waze"));
					}
				}
			}
		}

		void OnNavegarMapsClicked(object sender, EventArgs e)
		{
			var address = estacionamiento.Direccion;

			//switch (Device.RuntimePlatform)
			switch (Device.RuntimePlatform)
			{
				case Device.iOS:
					//Device.OpenUri(new Uri("http://maps.apple.com/?q=" + WebUtility.UrlEncode(address)));
					Device.OpenUri(new Uri("http://maps.apple.com/?daddr=" + estacionamiento.Latitud + "," + estacionamiento.Longitud));
					break;
				case Device.Android:
					Device.OpenUri(new Uri("https://maps.google.com?saddr=Current+Location&daddr="+estacionamiento.Latitud+","+estacionamiento.Longitud));
					break;
			}
		}

		async void OnContratarPensionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_Pensiones(estacionamiento));
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}


	}

}
