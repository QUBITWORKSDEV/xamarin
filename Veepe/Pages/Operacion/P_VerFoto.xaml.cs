﻿using System;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_VerFoto : FunkContentPage
	{
		public P_VerFoto()
		{
			InitializeComponent();
		}

		public P_VerFoto(string url)
		{
			InitializeComponent();

			imgPreview.Source = new UriImageSource { Uri = new Uri(url), CachingEnabled = false };
		}
	}
}
