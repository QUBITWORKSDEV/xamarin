﻿using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_CalificarServicio : FunkContentPage
	{
		int rating = 5;
		sor simpleOp;

		public P_CalificarServicio(sor simpleOperation)
		{
			simpleOp = simpleOperation;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			// ponemos los datos
			txtNombreEstacionamiento.Text = simpleOperation.estacionamiento.Nombre;
			txtCosto.Text = simpleOperation.operacion.monto.ToString("N"); // F2
			txtFechaEntrada.Text = simpleOperation.operacion.fechaIngreso.ToString("dd-MMM-yyyy H:mm");
		}

		async void OnTerminarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				// lo pasamos a status 11 y le ponemos el rating
				WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "finalizarOperacion", "{\"idoperacion\":\"" + simpleOp.operacion.idOperacion + "\", \"rating\":\"" + rating + "\"}");    // 2do: datos dummie! también debe enviar credenciales

				if (items != null)
				{
					if (items.respuesta == "ok" && items.datos == true)
					{
						await (Application.Current as App).StartupSequence();
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al calificar el servicio", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
			}
		}

		void OnEstrellaClicked(object sender, EventArgs e)
		{
			rating = int.Parse((sender as Button).CommandParameter.ToString());
			System.Diagnostics.Debug.WriteLine("->"+rating);

			// ponemos las estrellas que van
			for (int i = 1; i <= rating; i++)
			{
				this.FindByName<Image>("estrella" + i).Source = "estrellas_a.png";
			}

			// limpiamos las demás estrellas
			for (int i = rating + 1; i <= 5; i++)
			{
				this.FindByName<Image>("estrella" + i).Source = "estrellas_n.png";
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}
	}
}

