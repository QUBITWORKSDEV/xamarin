﻿using System;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_TicketReserva : FunkContentPage
	{
		Estacionamiento estacionamiento;
		DateTime hora;

		public P_TicketReserva() {
			InitializeComponent();
		}

		public P_TicketReserva(Estacionamiento pEstacionamiento, DateTime pHora)
		{
			estacionamiento = pEstacionamiento;
			hora = pHora;
			InitializeComponent();

			checkConnectivity();
			BindingContext = estacionamiento;

			lblWarningNoConnectivity.BindingContext = this;
			txtHoraLlegada.Text = hora.ToString();
		}

		async void OnAceptarReservaClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopToRootAsync();
			}
		}

		/*async void OnTerminosEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				// TODO: cargar el estacionamiento relacionado para poder pasarlo a la pantalla de terminos
				await Navigation.PushAsync(new P_TerminosEstacionamiento(null));
			}
		}*/

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

		void OnNavegarWazeClicked(object sender, EventArgs e)
		{
			var address = estacionamiento.Direccion;

			try
			{
				Device.OpenUri(new Uri("waze://?ll=" + estacionamiento.Latitud + "," + estacionamiento.Longitud + "&navigate=yes"));
			}
			catch
			{
				//if ( Device.RuntimePlatform == Device.iOS )
				if ( Device.RuntimePlatform == Device.iOS )
					Device.OpenUri(new Uri("http://itunes.apple.com/us/app/id323229106"));
				//else if ( Device.RuntimePlatform == Device.Android )
				else if ( Device.RuntimePlatform == Device.Android )
				{
					try
					{
						Device.OpenUri(new Uri("market://details?id=com.waze"));
					}
					catch
					{
						Device.OpenUri(new Uri("http://play.google.com/store/apps/details?id=com.waze"));
					}
				}
			}
		}

		void OnNavegarMapsClicked(object sender, EventArgs e)
		{
			var address = estacionamiento.Direccion;

			//switch (Device.RuntimePlatform)
			switch (Device.RuntimePlatform)
			{
				case Device.iOS:
					//Device.OpenUri(new Uri("http://maps.apple.com/?q=" + WebUtility.UrlEncode(address)));
					Device.OpenUri(new Uri("http://maps.apple.com/?daddr=" + estacionamiento.Latitud + "," + estacionamiento.Longitud));
					break;
				case Device.Android:
					Device.OpenUri(new Uri("https://maps.google.com?saddr=Current+Location&daddr="+estacionamiento.Latitud+","+estacionamiento.Longitud));
					break;
			}
		}

	}
}

