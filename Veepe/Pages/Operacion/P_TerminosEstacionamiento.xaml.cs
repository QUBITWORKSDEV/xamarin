﻿using System;

namespace Veepe
{
	public partial class P_TerminosEstacionamiento : FunkContentPage
	{
		bool esModal = false;

		public P_TerminosEstacionamiento(Estacionamiento pEstacionamiento, bool pEsModal = false)
		{
			esModal = pEsModal;

			InitializeComponent();
			checkConnectivity();

			BindingContext = pEstacionamiento;
		}

		void OnCerrarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				if (esModal)
					Navigation.PopModalAsync();
				else
					Navigation.PopAsync();
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

	}
}