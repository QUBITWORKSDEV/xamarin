﻿using System;
using System.Threading.Tasks;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_OpEntregarAuto : FunkContentPage
	{
		AutosOperacionViewModel auto;
		bool tieneMetodoPago = false;

		public P_OpEntregarAuto(AutosOperacionViewModel pAuto)
		{
			auto = pAuto;

			InitializeComponent();
			checkConnectivity();
			BindingContext = auto;
			lblWarningNoConnectivity.BindingContext = this;

			txtCosto.Text = auto.CostoAproximado.ToString("N");
			txtMonto.Text = "MXN " + auto.TarifaHora.ToString("F");

			Task.Run(async () => {
				await checarTieneMetodoPago();
			});
		}

		async Task checarTieneMetodoPago()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "getTieneMetodoPagoOperacion", "{\"idOperacion\":\"" + auto.idOperacion + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						tieneMetodoPago = items.datos;
						if (!tieneMetodoPago)
						{
							// mostramos una etiqueta que avise al operador
							lblFaltaMetodo.IsVisible = true;
						}
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				cmdAceptar.IsVisible = tieneMetodoPago;
				txtNip.IsVisible = tieneMetodoPago;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			if (tieneMetodoPago)
				txtNip.Focus();
		}

		async void OnAceptarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				WsResponse<int> items = await WsConsumer.Consume<int>("operations", "registrarSalida", "{\"idOperacion\":\"" + auto.idOperacion + "\", \"nip\":\"" + txtNip.Text + "\", \"monto\":\"" + auto.CostoAproximado + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						// 0=nip inválido, 1=todo bien, 3=no pasó el pago, 5=error general
						switch (items.datos)
						{
							case 0:
								// nip inválido
								await DisplayAlert("", "El NIP proporcionado es inválido.", "Aceptar");
								cmdAceptar.IsVisible = tieneMetodoPago;
								txtNip.IsVisible = tieneMetodoPago;
								break;

							case 1:
								// alles gut
								txtNip.IsVisible = false;
								cmdAceptar.IsVisible = false;

								lblExito.IsVisible = true;
								lblExito.Text = "¡Gracias por su preferencia!\nVuelva pronto.";
								break;

							case 3:
								// no pasó el pago
								await DisplayAlert("", "El pago no pudo ser procesado. Se intentará automáticamente más adelante.", "Aceptar");
								txtNip.IsVisible = false;
								cmdAceptar.IsVisible = false;
								//cmdAceptar.IsVisible = tieneMetodoPago;
								//txtNip.IsVisible = tieneMetodoPago;

								lblExito.IsVisible = true;
								lblExito.Text = "¡Gracias por su preferencia!\nVuelva pronto.";
								break;

							case 5:
								// error general
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
								cmdAceptar.IsVisible = tieneMetodoPago;
								txtNip.IsVisible = tieneMetodoPago;
								break;
						} // fin del switch
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						cmdAceptar.IsVisible = tieneMetodoPago;
						txtNip.IsVisible = tieneMetodoPago;
					}
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
					cmdAceptar.IsVisible = tieneMetodoPago;
					txtNip.IsVisible = tieneMetodoPago;
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}

		} // fin de OnAceptarClicked


		void OnNipChanged(object sender, EventArgs e)
		{
			string _text = (sender as Entry).Text;
			if (_text.Length > 4)
			{
				_text = _text.Remove(_text.Length-1); // remove last char
				(sender as Entry).Text = _text;
			}
		}

		void OnNipCompleted(object sender, EventArgs e)
		{
			OnAceptarClicked(sender, e);
		}

	}
}