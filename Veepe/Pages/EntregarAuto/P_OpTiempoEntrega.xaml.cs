﻿using System;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_OpTiempoEntrega : FunkContentPage
	{
		AutosOperacionViewModel auto;
		int tiempoSeleccionado = 0;

		public P_OpTiempoEntrega(AutosOperacionViewModel pAuto)
		{
			auto = pAuto;
			InitializeComponent();
			checkConnectivity();

			BindingContext = auto;
			lblWarningNoConnectivity.BindingContext = this;
		}

		void unselectButtons()
		{
			stack5.BackgroundColor = Color.FromHex("#2C2A2D");
			t15.TextColor = Color.FromHex("#3EBED6");
			t25.TextColor = Color.FromHex("#A3A6A4");

			stack10.BackgroundColor = Color.FromHex("#2C2A2D");
			t110.TextColor = Color.FromHex("#3EBED6");
			t210.TextColor = Color.FromHex("#A3A6A4");

			stack15.BackgroundColor = Color.FromHex("#2C2A2D");
			t115.TextColor = Color.FromHex("#3EBED6");
			t215.TextColor = Color.FromHex("#A3A6A4");

			stack20.BackgroundColor = Color.FromHex("#2C2A2D");
			t120.TextColor = Color.FromHex("#3EBED6");
			t220.TextColor = Color.FromHex("#A3A6A4");
		}


		void OnTiempoClicked(object sender, EventArgs e)
		{
			unselectButtons();
			tiempoSeleccionado = int.Parse( (sender as Button).CommandParameter.ToString() );

			GridOpciones.FindByName<StackLayout>("stack" + tiempoSeleccionado).BackgroundColor = Color.FromHex("#3EBED6");
			GridOpciones.FindByName<Label>("t1" + tiempoSeleccionado).TextColor = Color.White;
			GridOpciones.FindByName<Label>("t2" + tiempoSeleccionado).TextColor = Color.White;
		}

		async void OnIngresarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				// registramos el tiempo
				if (tiempoSeleccionado == 0)
					await DisplayAlert("", "Debes seleccionar un tiempo estimado de entrega.", "Cerrar");
				else
				{
					activityIndicator.IsVisible = true;
					//WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "aceptarSolicitudSalidaPlaca", "{\"placa\":\"" + auto.Placa + "\",\"tiempo\":\"" + tiempoSeleccionado + "\"}");
					WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "aceptarSolicitudSalida", "{\"idOperacion\":\"" + auto.idOperacion + "\",\"tiempo\":\"" + tiempoSeleccionado + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("", "Sucedió un error al aceptar la solicitud. Por favor inténtalo nuevamente.", "Aceptar");
								Reporter.ReportError("Error al aceptar la solicitud. Items.respuesta!=ok. Placa: " + auto.Placa);
								await Navigation.PopAsync();
							}
						}
						else
						{
							await DisplayAlert("", "Sucedió un error al aceptar la solicitud. Por favor inténtalo nuevamente.", "Aceptar");
							Reporter.ReportError("Error2 al aceptar la solicitud. Items.respuesta!=ok. Placa: " + auto.Placa);
							await Navigation.PopAsync();
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin de OnIngresarClicked

	}
}