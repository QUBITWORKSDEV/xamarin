﻿using System;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_VerAuto : FunkContentPage
	{
		public Auto auto;

		public P_VerAuto(Auto pAuto)
		{
			auto = pAuto;
			InitializeComponent();
			checkConnectivity();

			BindingContext = auto;
		}

		/*public void update(Auto pAuto)
		{
			auto = pAuto;
		}*/

		async void OnEliminarClicked(object Sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				if (await DisplayAlert("", "¿De verdad deseas eliminar este auto?", "Si", "No"))
				{
					canEdit = false;

					WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "bajaAuto", "{\"idAuto\":\"" + auto.idAuto + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								// regresamos a la anterior y recargamos
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", "Sucedió un error al obtener la lista de autos. Por favor inténtalo más tarde.", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al obtener la lista de autos. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

					canEdit = true;
				}
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

		async void OnEditarClicked(object Sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_EditarAuto(auto));
			}
		}
	}
}

