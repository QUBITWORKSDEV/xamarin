﻿using System;
using System.Text.RegularExpressions;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_AgregarAuto : FunkContentPage
	{
		Usuario usuario;

		public P_AgregarAuto()
		{
			usuario = SessionManager.getLocalUser();

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;
		}

		async void OnGuardarClicked(object Sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (validarFormulario())
				{
					string marca = string.IsNullOrWhiteSpace(txtMarca.Text) ? "-" : txtMarca.Text;
					string modelo = string.IsNullOrWhiteSpace(txtModelo.Text) ? "-" : txtModelo.Text;

					WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "altaAuto", "{\"veepeId\":\"" + usuario.veepeId + "\", \"marca\":\"" + marca + "\", \"submarca\":\"" + txtSubmarca.Text + "\", \"modelo\":\"" + modelo + "\", \"placa\":\"" + txtPlaca.Text + "\", \"color\":\"" + txtColor.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", "Sucedió un error al agregar el auto. Por favor inténtalo más tarde.", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al agregar el auto. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					await DisplayAlert("", "Debes llenar todos los campos", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		bool validarFormulario()
		{
			bool res;

			res = txtSubmarca.IsValid && txtPlaca.IsValid && txtColor.IsValid;

			if ( !string.IsNullOrWhiteSpace(txtModelo.Text) )
				res &= Regex.IsMatch((txtModelo.Text ?? ""), @"^[0-9]+$", RegexOptions.IgnoreCase);

			return res;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

		void OnModeloChanged(object sender, TextChangedEventArgs e)
		{
			if (e.NewTextValue.Length > 4)
				(sender as Entry).Text = e.NewTextValue.Substring(0, 4);
		}

	} // fin de P_AgregarAuto
}
