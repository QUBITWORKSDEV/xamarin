﻿using System;
using System.Threading.Tasks;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_MisAutos : FunkContentPage
	{
		public P_MisAutos()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = this;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			actualizar();
		}

		public async void actualizar()
		{
			if (!isOffline)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				WsResponse<Auto[]> items = await WsConsumer.Consume<Auto[]>("data", "getAutos", "{\"veepeId\":\"" + (App.Current as App).usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						listaAutos.ItemsSource = items.datos;
						if (items.datos.Length == 0)
						{
							listaAutos.IsVisible = false;
							listaAutos_nodata.IsVisible = true;
						}
						else
						{
							listaAutos.IsVisible = true;
							listaAutos_nodata.IsVisible = false;
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener la lista de autos. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		async void OnAutoSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_VerAuto(e.SelectedItem as Auto));
			}
		}

		async void OnAgregarClicked(object Sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_AgregarAuto());
			}
		}
	}
}