﻿using System;
using FunkFramework.Net;

/********************************************************
 * //TODO:
 * Que le padre se actualice con un update
 * ****************************************************** */

namespace Veepe
{
	public partial class P_EditarAuto : FunkContentPage
	{
		Auto auto;

		public P_EditarAuto(Auto pAuto)
		{
			auto = pAuto;
			InitializeComponent();
			checkConnectivity();

			txtMarca.Text = auto.Marca;
			txtSubmarca.Text = auto.Submarca;
			txtModelo.Text = auto.Modelo+"";
			txtPlaca.Text = auto.Placa;
			txtColor.Text = auto.Color;
		}

		async void OnGuardarClicked(object Sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				if (validarFormulario())
				{
					canEdit = false;
					activityIndicator.IsVisible = true;

					string marca = string.IsNullOrWhiteSpace(txtMarca.Text) ? "-" : txtMarca.Text;
					string modelo = string.IsNullOrWhiteSpace(txtModelo.Text) ? "-" : txtModelo.Text;

					WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "cambiarAuto", "{\"idAuto\":\"" + auto.idAuto + "\", \"marca\":\"" + marca + "\", \"submarca\":\"" + txtSubmarca.Text + "\", \"modelo\":\"" + modelo + "\", \"placa\":\"" + txtPlaca.Text + "\", \"color\":\"" + txtColor.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								// todo: será?
								for (var counter = 1; counter < 2; counter++)
								{
									Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
								}
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", "Sucedió un error al obtener la lista de autos. Por favor inténtalo más tarde.", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al obtener la lista de autos. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					await DisplayAlert("", "Debes llenar todos los campos.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		bool validarFormulario()
		{
			return txtSubmarca.IsValid && txtPlaca.IsValid && txtColor.IsValid;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}
	}
}