using System;
using System.Text.RegularExpressions;
using FunkFramework.Net;
using FunkFramework.UI;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_PerfilEditarCampo : FunkContentPage
	{
		string campotxt = "";
		Usuario usuario;

		public P_PerfilEditarCampo(string elCampo, Usuario pUsuario)
		{
			campotxt = elCampo;
			usuario = pUsuario;

			InitializeComponent();
			checkConnectivity();

			BindingContext = this;

			switch (elCampo)
			{
				case "nombre":
					controlesTelefono.IsVisible = false;
					controlesContrasena.IsVisible = false;
					controlesNIP.IsVisible = false;
					txtCampoNombre.Text = usuario.nombre;
					txtCampoApellidoPaterno.Text = usuario.apellidoPaterno;
					txtCampoApellidoMaterno.Text = usuario.apellidoMaterno;
					break;
					
				case "telefono":
					controlesNombre.IsVisible = false;
					controlesContrasena.IsVisible = false;
					controlesNIP.IsVisible = false;
					txtCampoTelefono.Text = usuario.celular;
					break;
					
				case "contrasena":
					controlesNombre.IsVisible = false;
					controlesTelefono.IsVisible = false;
					controlesNIP.IsVisible = false;
					break;
					
				case "nip":
					controlesNombre.IsVisible = false;
					controlesTelefono.IsVisible = false;
					controlesContrasena.IsVisible = false;
					break;
			}
		}

		async void OnGuardarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				// bloqueamos la ui
				canEdit = false;
				activityIndicator.IsVisible = true;

				switch (campotxt)
				{
					//Editar nombre
					case "nombre":
						// validamos que todos los campos estén llenos
						if (txtCampoNombre.IsValid && txtCampoApellidoPaterno.IsValid)
						{
							string apeMaterno = string.IsNullOrWhiteSpace(txtCampoApellidoMaterno.Text) ? "" : txtCampoApellidoMaterno.Text;

							WsResponse<bool> items;
							if ((Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
								items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarNombre", "{\"veepeId\":\"" + usuario.veepeId + "\", \"nombre\":\"" + txtCampoNombre.Text + "\", \"apellidoPaterno\":\"" + txtCampoApellidoPaterno.Text + "\", \"apellidoMaterno\":\"" + apeMaterno + "\"}");
							else
								items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarNombre", "{\"idUsuario\":\"" + usuario.idUsuario + "\", \"nombre\":\"" + txtCampoNombre.Text + "\", \"apellidoPaterno\":\"" + txtCampoApellidoPaterno.Text + "\", \"apellidoMaterno\":\"" + apeMaterno + "\"}");

							if (items != null)
							{
								if (items.respuesta == "ok" && items.datos == true)
								{
									await Navigation.PopAsync();
								}
								else
								{
									await DisplayAlert("Error", "Sucedió un error al actualizar el nombre. Inténtalo más tarde.", "Aceptar");
								}
							}
							else
							{
								await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Por favor asegúrate de estar conectado a la red", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("", "Debes llenar el nombre y el apellido paterno.", "Aceptar");
						}
						break;

					// Editar Teléfono
					case "telefono":
						if (txtCampoTelefono.IsValid)
						{
							WsResponse<bool> items;
							if ((Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
								items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarTelefono", "{\"veepeId\":\"" + usuario.veepeId + "\", \"telefono\":\"" + txtCampoTelefono.Text + "\"}");
							else
								items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarTelefono", "{\"idUsuario\":\"" + usuario.idUsuario + "\", \"telefono\":\"" + txtCampoTelefono.Text + "\"}");

							if (items != null)
							{
								if (items.respuesta == "ok" && items.datos == true)
								{
									await Navigation.PopAsync();
								}
								else
								{
									await DisplayAlert("Error", "Sucedió un error al actualizar el teléfono. Inténtalo más tarde.", "Aceptar");
								}
							}
							else
							{
								await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Por favor asegúrate de estar conectado a la red", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("", "Debes proporcionar un teléfono.", "Aceptar");
						}
						break;

					case "contrasena":
						if (txtCampoContraseniaActual.IsValid && txtCampoContraseniaNueva.IsValid && txtCampoContraseniaConfirmar.IsValid)
						{
							if (txtCampoContraseniaNueva.Text == txtCampoContraseniaConfirmar.Text)
							{
								WsResponse<bool> items;

								if ((Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
									items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarContrasenha", "{\"veepeId\":\"" + usuario.veepeId + "\",\"c1\":\"" + txtCampoContraseniaActual.Text + "\",\"c2\":\"" + txtCampoContraseniaNueva.Text + "\"}");
								else
									items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarContrasenha", "{\"idUsuario\":\"" + usuario.idUsuario + "\",\"c1\":\"" + txtCampoContraseniaActual.Text + "\",\"c2\":\"" + txtCampoContraseniaNueva.Text + "\"}");

								if (items != null)
								{
									if (items.respuesta == "ok")
									{
										if (items.datos == true)
										{
											await Navigation.PopAsync();
										}
										else
										{
											await DisplayAlert("Error", "La contraseña anterior es inválida. Por favor inténtalo de nuevo.", "Aceptar");
										}
									}
									else
									{
										await DisplayAlert("Error", "Sucedió un error al actualizar la contraseña. Por favor inténtalo más tarde.", "Aceptar");
									}
								}
								else
								{
									await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Por favor asegúrate de estar conectado a la red", "Aceptar");
								}
							}
							else
								await DisplayAlert("", "Las contraseñas deben coincidir.", "Aceptar");
						}
						else
							await DisplayAlert("", "Debes llenar todos los campos.", "Aceptar");

						canEdit = true;
						activityIndicator.IsVisible = false;
						break;

					case "nip":
						if (txtCampoNipActual.IsValid && txtCampoNipNuevo.IsValid && txtCampoNipConfirmar.IsValid)
						{
							if (txtCampoNipNuevo.Text.Length != 4)
							{
								await DisplayAlert("", "El NIP debe ser un número de 4 dígitos.", "Aceptar");
							}
							else if (txtCampoNipNuevo.Text != txtCampoNipConfirmar.Text)
							{
								await DisplayAlert("", "Los NIPs deben coincidir.", "Aceptar");
							}
							else
							{
								WsResponse<bool> items;
								if ((Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
									items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarNip", "{\"veepeId\":\"" + usuario.veepeId + "\", \"n1\":\"" + txtCampoNipActual.Text + "\", \"n2\":\"" + txtCampoNipNuevo.Text + "\"}");
								else
									items = await WsConsumer.Consume<bool>("umanagement", "perfilEditarNip", "{\"idUsuario\":\"" + usuario.idUsuario + "\", \"n1\":\"" + txtCampoNipActual.Text + "\", \"n2\":\"" + txtCampoNipNuevo.Text + "\"}");

								if (items != null)
								{
									if (items.respuesta == "ok")
									{
										if (items.datos == true)
										{
											await Navigation.PopAsync();
										}
										else
										{
											await DisplayAlert("Error", "El NIP anterior es inválido. Por favor inténtalo nuevamente.", "Aceptar");
										}
									}
									else
									{
										await DisplayAlert("Error", "Sucedió un error al actualizar el NIP. Inténtalo más tarde.", "Aceptar");
									}
								}
								else
								{
									await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Por favor asegúrate de estar conectado a la red", "Aceptar");
								}
							}
						}
						else
						{
							await DisplayAlert("", "Debes llenar todos los campos.", "Aceptar");
						}
						break;
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		async void OnCancelarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopAsync();
			}
		}

		public void OnNipChanged(object sender, TextChangedEventArgs e)
		{
			if (Regex.IsMatch(e.NewTextValue, @"(\D)+"))
			{
				// tiene algo que no es número -> no aceptar
				(sender as FunkEntry).Text = e.OldTextValue;
			}

			if (e.NewTextValue.Length > 4)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 4);
		}

		async void OnRecuperarNipClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				bool r = await DisplayAlert("Confirmación", "Se enviará un email a tu dirección de correo para recuperar el NIP.", "Recuperar", "Cancelar");

				if (r)
				{
					WsResponse<bool> respuesta;
					respuesta = await WsConsumer.Consume<bool>("umanagement", "recoverNip", "{\"veepeId\":\"" + usuario.veepeId + "\"}");

					if (respuesta != null)
					{
						if (respuesta.respuesta == "ok")
						{
							await DisplayAlert("", "El correo fue enviado. Sigue las instrucciones para recuperar tu NIP.", "Aceptar");
							await Navigation.PopAsync();
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al enviar el correo de recuperación.", "Aceptar");
						}
					}
				}

				canEdit = true;
			}

		} // fin de OnRecuperarNipClicked


	}
}

