using System;
using Xamarin.Forms;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_Perfil : FunkContentPage
	{
		public P_Perfil ()
		{
			InitializeComponent ();
			checkConnectivity();

			BindingContext = (Application.Current as App).usuario;
			lblWarningNoConnectivity.BindingContext = this;

			// si lo está viendo el operador, hay ciertas cosas que ocultar
			if ((Application.Current as App).usuario.rol != Usuario.ROL_USUARIO_FINAL)
			{
				StackVeepeId.IsVisible = false;
				GridNIP.IsVisible = false;
			}
		}

		public async void actualizaPerfil()
		{
			if (isOffline)
				return;
				
			canEdit = false;

			WsResponse<Usuario> items;
			if ( (Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL )
				items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeId\":\"" + (Application.Current as App).usuario.veepeId + "\"}");
			else
				items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"idUsuario\":\"" + (Application.Current as App).usuario.idUsuario + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					SessionManager.registerLogin( items.datos );
					(Application.Current as App).usuario = items.datos;
					BindingContext = items.datos;
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

			canEdit = true;
		}

		async void OnNombreClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_PerfilEditarCampo("nombre", (Application.Current as App).usuario));
			}
		}

		async void OnTelefonoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_PerfilEditarCampo("telefono", (Application.Current as App).usuario));
			}
		}

		async void OnContrasenaClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_PerfilEditarCampo("contrasena", (Application.Current as App).usuario));
			}
		}

		async void OnNIPClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_PerfilEditarCampo("nip", (Application.Current as App).usuario));
			}
		}

		async void OnCerrarSesionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				var salir = await DisplayAlert("Confirmación", "¿De verdad deseas cerrar la sesión?", "Cerrar sesión", "Cancelar");

				if (salir)
				{
					int uid = (Application.Current as App).usuario.idUsuario;

					if (SessionManager.logOut())
					{
						(Application.Current as App).usuario = null;
						WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "unregisterPushToken", "{\"uid\":\"" + uid + "\"}");

						// vamos al home
						(Application.Current as App).MainPage = new NavigationPage(new P_LoginInicio());
					}
				}

				canEdit = true;
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			actualizaPerfil();
		}

	}
}

