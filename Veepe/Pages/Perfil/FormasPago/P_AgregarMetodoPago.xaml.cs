﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using FunkFramework;
using FunkFramework.Net;
using FunkFramework.UI;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_AgregarMetodoPago : FunkContentPage
	{
		public P_AgregarMetodoPago()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = this;
		}

		async void OnGuardarFormaPagoClick(object Sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (validarFormulario())
				{
					Usuario u = SessionManager.getLocalUser();

					WsResponse<bool> items = await WsConsumer.Consume<bool>("umanagement", "altaMetodoPago", "{\"veepeId\":\"" + u.veepeId +
																										"\", \"nombreCuenta\":\"" + txtNombreCuenta.Text +
																										"\", \"numeroTarjeta\":\"" + txtNumeroTarjeta.Text +
																										"\", \"mesExp\":\"" + txtMesExp.Text +
																										"\", \"anhoExp\":\"" + txtAnhoExp.Text +
																										"\", \"cvv\":\"" + txtCvv.Text +
																										"\", \"nombreTitular\":\"" + txtNombre.Text +
																										"\", \"direccionTitular\":\"" + txtDireccion.Text +
																										"\", \"cpTitular\":\"" + txtCP.Text +
																										"\", \"emailTitular\":\"" + txtEmail.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								// regresamos
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", items.mensaje, "Cerrar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al agregar el método de pago. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		private bool validarFormulario()
		{
			bool res = false;

			// primero checamos que todos estén completos
			bool completos = (txtNombreCuenta.IsValid && txtNombre.IsValid && txtNumeroTarjeta.IsValid && txtMesExp.IsValid && txtAnhoExp.IsValid && txtCvv.IsValid);
			if (!completos)
			{
				DisplayAlert("", "Debes proporcionar todos los datos", "Cerrar");
			}
			else
			{
				// ahora checamos que sean válidos
				// número de tarjeta
				if (!isValidCreditNumber(txtNumeroTarjeta.Text))
				{
					DisplayAlert("", "El número de tarjeta es inválido.", "Cerrar");
					return res;
				}
				else if (txtCvv.Text.Length < 3)
				{
					DisplayAlert("", "El CVV es inválido.", "Cerrar");
					return res;
				}
					// mes
				else if (int.Parse(txtMesExp.Text) < 1 || int.Parse(txtMesExp.Text) > 12)
				{
					DisplayAlert("", "Debes proporcionar un mes válido", "Cerrar");
					return res;
				}
					// año
				else if (int.Parse(txtAnhoExp.Text) < 10 || int.Parse(txtAnhoExp.Text) > 30)
				{
					DisplayAlert("", "Debes proporcionar un año válido", "Cerrar");
					return res;
				}
					// cp
				else if (txtCP.Text.Length != 5)
				{
					DisplayAlert("", "Debes proporcionar un CP válido", "Cerrar");
					return res;
				}
				else
					res = true;
			}

			return res;
		}

		bool isValidCreditNumber(string number)
		{
			//http://www.regular-expressions.info/creditcard.html
			bool res =Regex.IsMatch(number, @"^(?:4[0-9]{15}|(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13})$");

			Debug.WriteLine(number + "::" + res);
			return res;
		}

		void OnNumTarjetaChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir números
			if (Regex.IsMatch(e.NewTextValue, @"(\D)+"))
			{
				// tiene algo que no es número -> no aceptar
				(sender as FunkEntry).Text = e.OldTextValue;
			}

			if (e.NewTextValue.Length > 16)
				(sender as FunkEntry).Text = e.OldTextValue;

			// ahora checamos la longitud. es variable, ya que cambia por banco. Quitamos Discover y JCB porque no son soportadas por Banwire
			//if (e.NewTextValue.Length == 16)
			if (isValidCreditNumber(e.NewTextValue))
				txtMesExp.Focus();

		}

		void OnMesExpChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir 2 números
			if (e.NewTextValue.Length > 2)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 2);
		
			if (e.NewTextValue.Length == 2)
				txtAnhoExp.Focus();
		}

		void OnAnhoExpChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir 2 números
			if (e.NewTextValue.Length > 2)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 2);
		
			if (e.NewTextValue.Length == 2)
				txtCvv.Focus();
		}

		void OnCvvChanged(object sender, TextChangedEventArgs e)
		{
			int tam = txtNumeroTarjeta.Text.Substring(0, 1) == "3" ? 4 : 3;		// si la tarjeta es amex, el cvv es de 4 dígitos
			
			// sólo debemos permitir 3 números
			if (e.NewTextValue.Length > tam)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, tam);
		
			if (e.NewTextValue.Length == tam)
				txtNombre.Focus();
		}
	}
}

