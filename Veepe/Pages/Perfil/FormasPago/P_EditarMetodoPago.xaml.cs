﻿using System;
using System.Text.RegularExpressions;
using FunkFramework;
using FunkFramework.Net;
using FunkFramework.UI;
using Xamarin.Forms;

/********************************************************
 * //TODO:
 * Que la actualización del método de pago se haga a través de un update en la página padre
 * ****************************************************** */

namespace Veepe
{
	public partial class P_EditarMetodoPago : FunkContentPage
	{
		MetodoPago metodoPago;
		P_MetodoPago padre;

		public P_EditarMetodoPago(MetodoPago pMetodoPago, P_MetodoPago pPadre)
		{
			padre = pPadre;
			metodoPago = pMetodoPago;

			InitializeComponent();
			checkConnectivity();

			txtNombreCuenta.Text = metodoPago.NombreCuenta;
			txtNombreTitular.Text = metodoPago.NombreTarjeta;
		}

		async void OnGuardarClick(object Sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				// validamos el formulario
				if (validarFormulario())
				{
					WsResponse<bool> items = await WsConsumer.Consume<bool>("umanagement", "cambioMetodoPago", "{\"idMetodo\":\"" + metodoPago.idMetodoPago + "\", \"nombreCuenta\":\"" + txtNombreCuenta.Text + "\", \"numeroTarjeta\":\"" + txtNumeroTarjeta.Text + "\", \"mes\":\"" + txtMesExp.Text +
																"\", \"anho\":\"" + txtAnhoExp.Text + "\", \"cvv\":\"" + txtCvv.Text + "\", \"nombreTitular\":\"" + txtNombreTitular.Text + "\", \"direccionTitular\":\"" + txtDireccion.Text + "\", \"cpTitular\":\"" + txtCP.Text + "\", \"emailTitular\":\"" + txtEmail.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								// regresamos y actualizamos
								await Navigation.PopAsync();
								padre.metodoPago = this.metodoPago;
							}
							else
							{
								await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		bool validarFormulario()
		{
			bool res = false;

			// primero checamos que todos estén completos
			bool completos = (txtNombreCuenta.IsValid && txtNombreTitular.IsValid && txtNumeroTarjeta.IsValid && txtMesExp.IsValid && txtAnhoExp.IsValid && txtCvv.IsValid);
			if (!completos)
			{
				DisplayAlert("", "Debes proporcionar todos los datos", "Aceptar");
			}
			else
			{
				// ahora checamos que sean válidos
				if (txtNumeroTarjeta.Text.Length != 16)
				{
					DisplayAlert("", "El número de tarjate debe ser de 16 dígitos", "Aceptar");
					return res;
				}
				else if (int.Parse(txtMesExp.Text) < 1 || int.Parse(txtMesExp.Text) > 12)
				{
					DisplayAlert("", "Debes proporcionar un mes válido", "Aceptar");
					return res;
				}
				else if (txtCP.Text.Length != 5)
				{
					DisplayAlert("", "Debes proporcionar un CP válido", "Aceptar");
					return res;
				}
				else
					res = true;
			}

			return res;
		}

		void OnNumTarjetaChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir números
			if (Regex.IsMatch(e.NewTextValue, @"(\D)+"))
				// tiene algo que no es número -> no aceptar
				(sender as FunkEntry).Text = e.OldTextValue;

			if ( e.NewTextValue.Length > 16 )
				// tiene más de 16 caracteres -> no aceptar
				(sender as FunkEntry).Text = e.OldTextValue;
		}

		void OnMesExpChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir 2 números
			if (e.NewTextValue.Length > 2)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 2);
		}

		void OnAnhoExpChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir 2 números
			if (e.NewTextValue.Length > 2)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 2);
		}

		void OnCvvChanged(object sender, TextChangedEventArgs e)
		{
			// sólo debemos permitir 3 números
			if (e.NewTextValue.Length > 3)
				(sender as FunkEntry).Text = e.NewTextValue.Substring(0, 3);
		}

	}
}

