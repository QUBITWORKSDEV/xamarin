using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_MetodosPago : FunkContentPage
	{
		Usuario usuario;

		public P_MetodosPago()
		{
 			usuario = SessionManager.getLocalUser();
			InitializeComponent();
			checkConnectivity();
		}

		public async void actualizar()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeId\":\"" + usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						usuario = items.datos as Usuario;
						if (usuario._metodosPago.Count < 1)
						{
							listaMetodos.IsVisible = false;
							listaMetodos_nodata.IsVisible = true;
							await DisplayAlert("", "Para poder user Veepe necesitas al menos un método de pago.", "Aceptar");
						}
						else
						{
							listaMetodos.IsVisible = true;
							listaMetodos_nodata.IsVisible = false;
							listaMetodos.ItemsSource = usuario._metodosPago;
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener los métodos de pago. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		async void OnMetodoSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (e.SelectedItem == null)
				{
					return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
				}

				await Navigation.PushAsync(new P_MetodoPago(e.SelectedItem as MetodoPago));
				((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
			}
		}

		async void OnAgregarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_AgregarMetodoPago());
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
			actualizar();
		}

	}
}

