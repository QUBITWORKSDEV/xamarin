﻿using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_MetodoPago : FunkContentPage
	{
		public MetodoPago metodoPago;

		public P_MetodoPago(MetodoPago pMetodoPago)
		{
			metodoPago = pMetodoPago;

			InitializeComponent();
			checkConnectivity();
			BindingContext = metodoPago;

			cmdEliminar.BindingContext = this;
			//cmdEditar.BindingContext = this;
			lblWarningNoConnectivity.BindingContext = this;
		}

		async void OnEditarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_EditarMetodoPago(metodoPago, this));
			}
		}

		async void OnEliminarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				var answer = await DisplayAlert("Confirmación", "¿De verdad desea eliminar este método de pago?", "Si", "No");
				if (answer == true)
				{
					// contestó que si -> eliminar
					canEdit = false;

					WsResponse<bool> items = await WsConsumer.Consume<bool>("umanagement", "bajaMetodoPago", "{\"idMetodo\":\"" + metodoPago.idMetodoPago + "\", \"idUsuario\":\"" + (Application.Current as App).usuario.idUsuario + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								// regresamos y actualizamos
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin del método OnEliminarClicked

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

	} // fin de la clase p_MetodoPago
}
