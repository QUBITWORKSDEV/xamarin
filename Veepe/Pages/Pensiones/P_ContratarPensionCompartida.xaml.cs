using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;
using FunkFramework.Net;


/*
 
     La pensión compartida funciona de la sig manera:

    - Para poder contratar Minimo se deben seleccionar dos estacionamientos, el que ya está por default y otro
    - Los dos primeros van en contenedores separados
    - A partir del tercero se van agregando en un contenedor aparte, y cada que se actualiza la pantalla se pintan.
    - Se pueden seleccionar tantos estacionamientos como se desea, pero solo se pueden agregar aquellos que pertenecen a la misma empresa y que tienen el servicio de pensión compartida
    
     */



namespace Veepe
{
	public partial class P_ContratarPensionCompartida : FunkContentPage
	{
		Estacionamiento estacionamiento;
		Usuario usuario;

		//----primer estacionamiento---------------------------------
		public int pensionSeleccionada = -1;
		public string periodoSelecionado = "";
		public int periodoSeleccionadoN = -1;
		public string horarioSeleccionado = null;
		public float costoHorario = 0.0f;

		//----segundo estacionamiento---------------------------------
		public int pensionSeleccionadaSegunda = -1;
		public string nombreSegundoEstacionamiento = "";
		public string horarioSeleccionadoSegunda = null;
		public float costoHorarioSegunda = 0.0f;

		//---------------------------------------------------------

		//demás estacionamientos----------------
		public int pensionSeleccionadaComodin = -1;

		//----------------------

		public float subtotal = 0.0F;

		//------------------------------------------------

		public MetodoPago metodoPagoSeleccionado = null;

		public Estacionamiento primerEstacionamiento;
		public Estacionamiento segundoEstacionamiento;
		public Estacionamiento estacionamientoActualSeleccionado;

		public Estacionamiento[] arregloEstacionamientosParaSeleccionar;
		public List<Estacionamiento> listaEstacionamientosParaSeleccionar;
		public int totalEstacionamientosDisponibles;

		//------------NUMERO Y BANDERA PARA EL NUMERO DE ESTACIONAMIENTOS QUE SE PUEDEN AGREGAR--------------------------
		public int estacionamientosParaAgregar;
		public bool modificarEstacionamientosParaAgregar = true;
		//-----------------------------------------

		public List<Estacionamiento> estacionamientosSeleccionados = null; //la lista de estacionamientos a partir del tercero

		public List<EstacionamientoSeleccionado> estacionamientosHorarioSeleccionado = null;

		public List<int> pensionesSeleccionadas;  //el id de las pensiones seleccionadas

		public P_ContratarPensionCompartida(Estacionamiento pEstacionamiento)
		{
			usuario = SessionManager.getLocalUser();
			pensionesSeleccionadas = new List<int>();

			arregloEstacionamientosParaSeleccionar = new Estacionamiento[0];
			listaEstacionamientosParaSeleccionar = new List<Estacionamiento>();
			estacionamientosSeleccionados = new List<Estacionamiento>();
			estacionamientosHorarioSeleccionado = new List<EstacionamientoSeleccionado>();
			estacionamiento = pEstacionamiento;
			InitializeComponent();

			checkConnectivity();
			lblWarningNoConnectivity.BindingContext = this;
			BindingContext = this;

			lblNombrePension.Text = estacionamiento.Nombre;
			lblDireccionPension.Text = estacionamiento.Direccion;

			//------------seteamos el primer estacionamiento
			primerEstacionamiento = new Estacionamiento();
			primerEstacionamiento.idEstacionamiento = estacionamiento.idEstacionamiento;
			primerEstacionamiento.Nombre = estacionamiento.Nombre;

			//------------instanciamos el objeto del segundo estacionamiento
			segundoEstacionamiento = new Estacionamiento();
			//-------------instanciamos el objeto del estacionamiento que tendrá al que se selecciona actual
			estacionamientoActualSeleccionado = new Estacionamiento();

			getEstacionamientos(null);

		}




        /* Este método sirve para checar que el usuario tiene al menos un método de pago agregado*/
		public async void metodoDePago()
		{
			WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "getTieneMetodoPago", "{\"uid\":\"" + usuario.idUsuario + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					if (items.datos == true)
					{
						//await DisplayAlert("", "La contratación fue exitosa. Puedes consultarla en la sección Pensiones.", "Cerrar");
						//await Navigation.PopToRootAsync();
					}
					else
					{
						//await DisplayAlert("Error", "No tienes metodos de pago asociados, debes agregar al menos uno para poder continuar", "Aceptar");
						lblMetodoPago.Text = "Agregar método de pago";
						await Navigation.PushAsync(new P_AgregarMetodoPago());
					}
				}
				else
				{
					await DisplayAlert("Error", "No tienes metodos de pago asociados.", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

		}


        /*Este método nos tra los estacionamientos que están dados de alta por la empresa*/
		public async void getEstacionamientos(Estacionamiento pestacionamiento = null)
		{

			int idEmpresa = estacionamiento.idEmpresa;

			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				WsResponse<Estacionamiento[]> items = await WsConsumer.Consume<Estacionamiento[]>("data", "getestacionamientos", "{\"idEmpresa\":\"" + idEmpresa + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{

						if (pestacionamiento == null)
						{
                            totalEstacionamientosDisponibles = numeroEstacionamientosParaSeleccionar();

							arregloEstacionamientosParaSeleccionar = items.datos;

							listaEstacionamientosParaSeleccionar = arregloEstacionamientosParaSeleccionar.ToList();
						}

						if (listaEstacionamientosParaSeleccionar.Count > 0)
						{

							for (int i = listaEstacionamientosParaSeleccionar.Count - 1; i >= 0; i--)
							{

								if (pestacionamiento == null)
								{
									if (listaEstacionamientosParaSeleccionar[i].idEstacionamiento == estacionamiento.idEstacionamiento || listaEstacionamientosParaSeleccionar[i].TienePension == false || listaEstacionamientosParaSeleccionar[i].TienePensionCompartida == false)
									{
										listaEstacionamientosParaSeleccionar.Remove(listaEstacionamientosParaSeleccionar[i]);
									}
								}
								else
								{
									if (listaEstacionamientosParaSeleccionar[i].idEstacionamiento == pestacionamiento.idEstacionamiento || listaEstacionamientosParaSeleccionar[i].TienePension == false || listaEstacionamientosParaSeleccionar[i].TienePensionCompartida == false)
									{
										listaEstacionamientosParaSeleccionar.Remove(listaEstacionamientosParaSeleccionar[i]);
									}
								}
							}
						}


						if (modificarEstacionamientosParaAgregar)
						{
							estacionamientosParaAgregar = listaEstacionamientosParaSeleccionar.Count() - 1;
							modificarEstacionamientosParaAgregar = false;
						}

						arregloEstacionamientosParaSeleccionar = listaEstacionamientosParaSeleccionar.ToArray();

					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
			}
			canEdit = true;
		}


        public int numeroEstacionamientosParaSeleccionar()
        {
            int numero = 0;

            if (arregloEstacionamientosParaSeleccionar.Length > 0)
            {

                List<Estacionamiento> estacionamientosSeleccionar;
                estacionamientosSeleccionar = new List<Estacionamiento>();
                bool cumpleCondicion = false;

                for (int i = 0; i < arregloEstacionamientosParaSeleccionar.Length; i++)
                {
                    cumpleCondicion = false;
                    for (int j = 0; j < arregloEstacionamientosParaSeleccionar[i].Pensiones.Length; j++)
                    {
                        if (arregloEstacionamientosParaSeleccionar[i].Pensiones[j].periodo == periodoSeleccionadoN)
                        {
                            if (arregloEstacionamientosParaSeleccionar[i].Pensiones[j].costoDiaCompartida != 0 || arregloEstacionamientosParaSeleccionar[i].Pensiones[j].costoNocheCompartida != 0 || arregloEstacionamientosParaSeleccionar[i].Pensiones[j].costoTodoDiaCompartida != 0)
                            {
                                cumpleCondicion = true;
                            }
                        }
                    }

                    if (cumpleCondicion)
                    {
                        estacionamientosSeleccionar.Add(arregloEstacionamientosParaSeleccionar[i]);
                    }
                }

                numero = estacionamientosSeleccionar.Count;
            }

            return numero;
        }


		protected override void OnAppearing()
		{
			metodoDePago();

			activityIndicator.IsVisible = false;

			canEdit = false;

			subtotal = 0.0f;
			pensionesSeleccionadas.Clear();

            totalEstacionamientosDisponibles = numeroEstacionamientosParaSeleccionar();

			//---------------------------Seteamos el periodo del primer estacionamiento--------------------------
			if (pensionSeleccionada != -1) //si ya seleccionamos el periodo del primer estacionamiento 
			{
				pensionesSeleccionadas.Add(pensionSeleccionada); //agregamos el id de la primera pensión a la lista de pensiones seleccionadas

				for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
				{
					if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
					{

						if (estacionamiento.Pensiones[i].periodo == 1)
						{
							periodoSelecionado = estacionamiento.Pensiones[i].periodo.ToString() + " Mes";
						}
						else
						{
							periodoSelecionado = estacionamiento.Pensiones[i].periodo.ToString() + " Meses";
						}

						periodoSeleccionadoN = estacionamiento.Pensiones[i].periodo;
					}
				}

				lblPeriodo.Text = periodoSelecionado.ToString();
			}


			//------------------------Seteamos el horario del primer estacionamiento----------------------------
			if (horarioSeleccionado != null)
			{
				lblHorario.Text = horarioSeleccionado;

				switch (horarioSeleccionado)
				{
					case "Día":

						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = estacionamiento.Pensiones[i].costoDiaCompartida;
								subtotal += costoHorario; // * periodoSeleccionadoN; ***

                                lblHorario.Text += " (7:00 - 19:00) MXN " + estacionamiento.Pensiones[i].costoDiaCompartida.ToString("F");

							}
						}

						break;

					case "Noche":

						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = estacionamiento.Pensiones[i].costoNocheCompartida;
								subtotal += costoHorario; // * periodoSeleccionadoN; ***

                                lblHorario.Text += " (19:00 - 7:00) MXN " + estacionamiento.Pensiones[i].costoNocheCompartida.ToString("F");
							}
						}

						break;

					case "Todo el día":

						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = estacionamiento.Pensiones[i].costoTodoDiaCompartida;
								subtotal += costoHorario; // * periodoSeleccionadoN; ***

                                lblHorario.Text += " MXN " + estacionamiento.Pensiones[i].costoTodoDiaCompartida.ToString("F");
							}
						}

						break;
				}

			}
			else
			{
				lblHorario.Text = "Elegir horario";
				lblSubtotal.Text = "MXN / Mes";
			}

			//---------------------------------------------------------
			//-------------------segundo estacionamiento--------------
			//-------------------------------------------------------

			if (pensionSeleccionadaSegunda != -1)
			{
				pensionesSeleccionadas.Add(pensionSeleccionadaSegunda);//agregamos el id de la segunda pensión a la lista de pensiones seleccionadas
			}

			//------------------------Seteamos el horario del segundo estacionamiento----------------------------
			if (horarioSeleccionadoSegunda != null)
			{
				lblHorarioSegundaPension.Text = horarioSeleccionadoSegunda;

				switch (horarioSeleccionadoSegunda)
				{
					case "Día":

						for (int i = 0; i < segundoEstacionamiento.Pensiones.Length; i++)
						{
							if (segundoEstacionamiento.Pensiones[i].idPension == pensionSeleccionadaSegunda)
							{
								costoHorarioSegunda = segundoEstacionamiento.Pensiones[i].costoDiaCompartida;
								subtotal += costoHorarioSegunda; // * periodoSeleccionadoN; ***

                                lblHorarioSegundaPension.Text += " (7:00 - 19:00) MXN " + segundoEstacionamiento.Pensiones[i].costoDiaCompartida.ToString("F");
							}
						}

						break;

					case "Noche":

						for (int i = 0; i < segundoEstacionamiento.Pensiones.Length; i++)
						{
							if (segundoEstacionamiento.Pensiones[i].idPension == pensionSeleccionadaSegunda)
							{
								costoHorarioSegunda = segundoEstacionamiento.Pensiones[i].costoNocheCompartida;
								subtotal += costoHorarioSegunda; // * periodoSeleccionadoN; ***

                                lblHorarioSegundaPension.Text += " (19:00 - 7:00) MXN " + segundoEstacionamiento.Pensiones[i].costoNocheCompartida.ToString("F");
							}
						}

						break;

					case "Todo el día":

						for (int i = 0; i < segundoEstacionamiento.Pensiones.Length; i++)
						{
							if (segundoEstacionamiento.Pensiones[i].idPension == pensionSeleccionadaSegunda)
							{
								costoHorarioSegunda = segundoEstacionamiento.Pensiones[i].costoTodoDiaCompartida;
								subtotal += costoHorarioSegunda; // * periodoSeleccionadoN; ***

                                lblHorarioSegundaPension.Text += " MXN " + segundoEstacionamiento.Pensiones[i].costoTodoDiaCompartida.ToString("F");
							}
						}

						break;
				}

			}
			else
			{
				lblHorarioSegundaPension.Text = "Elegir horario";
				lblSubtotal.Text = "MXN / Mes";
			}

			//--------Seteamos el subtotal---------------------------------------------

			if (costoHorario != 0.0f)
			{
				lblSubtotal.Text = "MXN " + subtotal.ToString("N") + " / Mes";
			}
			else
			{
				lblSubtotal.Text = "MXN / Mes";
			}

			//---------------------------------------------

			if (nombreSegundoEstacionamiento != "")
			{
				lblEstacionamientoSegundaPension.Text = nombreSegundoEstacionamiento;
			}
            else
            {
                lblEstacionamientoSegundaPension.Text = "Elegir estacionamiento";
            }

			//---------------metodo de pago--------------
			if (metodoPagoSeleccionado != null)
			{
				lblMetodoPago.Text = metodoPagoSeleccionado.NombreCuenta;
			}
			else
			{
				lblMetodoPago.Text = "Elegir método de pago";
			}

			//-----------------mostramos la lista de estacionamientos seleccionados
            //---- Si hay estacionbamientos seleccionados a partir del tercero se hace lo siguiente.
			if (estacionamientosSeleccionados.Count > 0)
			{
				string horarioSeleccionado = "";
				contenedorEstacionamientos.Children.Clear();

				foreach (Estacionamiento estacionamiento in estacionamientosSeleccionados)
				{
					if (estacionamiento != segundoEstacionamiento && estacionamiento != primerEstacionamiento)
					{

                        string complementoHorario = "";

						if (estacionamientosHorarioSeleccionado.Count > 0)
						{

							if ((estacionamientosHorarioSeleccionado.Find(x => x.idEstacionamiento == estacionamiento.idEstacionamiento)) != null)
							{

								horarioSeleccionado = estacionamientosHorarioSeleccionado.Find(x => x.idEstacionamiento == estacionamiento.idEstacionamiento).horarioSeleccionado;

								var pensiones = estacionamientosHorarioSeleccionado.Find(x => x.idEstacionamiento == estacionamiento.idEstacionamiento).Pensiones;

								switch (horarioSeleccionado)
								{
									case "Día":

										for (int i = 0; i < pensiones.Length; i++)
										{
											if (pensiones[i].idPension == pensionSeleccionadaComodin)
											{
												costoHorario = estacionamiento.Pensiones[i].costoDiaCompartida;
												subtotal += costoHorario; // * periodoSeleccionadoN;***

												foreach (var tom in estacionamientosHorarioSeleccionado.Where(w => w.idEstacionamiento == estacionamiento.idEstacionamiento))
												{
													tom.idPensionSeleccionada = pensiones[i].idPension;
													tom.costoHorarioSeleccionado = pensiones[i].costoDiaCompartida;
												}


                                                complementoHorario = " (7:00 - 19:00) MXN " + pensiones[i].costoDiaCompartida.ToString("F");


											}
										}

										break;

									case "Noche":

										for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
										{
											if (pensiones[i].idPension == pensionSeleccionadaComodin)
											{
												costoHorario = estacionamiento.Pensiones[i].costoNocheCompartida;
												subtotal += costoHorario; // * periodoSeleccionadoN; ***

												foreach (var tom in estacionamientosHorarioSeleccionado.Where(w => w.idEstacionamiento == estacionamiento.idEstacionamiento))
												{
													tom.idPensionSeleccionada = pensiones[i].idPension;
													tom.costoHorarioSeleccionado = pensiones[i].costoNocheCompartida;
												}

                                                complementoHorario = " (19:00 - 7:00) MXN " + pensiones[i].costoNocheCompartida.ToString("F");

											}
										}

										break;

									case "Todo el día":

										for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
										{
											if (pensiones[i].idPension == pensionSeleccionadaComodin)
											{
												costoHorario = estacionamiento.Pensiones[i].costoTodoDiaCompartida;
												subtotal += costoHorario; // * periodoSeleccionadoN; ***

												foreach (var tom in estacionamientosHorarioSeleccionado.Where(w => w.idEstacionamiento == estacionamiento.idEstacionamiento))
												{
													tom.idPensionSeleccionada = pensiones[i].idPension;
													tom.costoHorarioSeleccionado = pensiones[i].costoTodoDiaCompartida;
												}

                                                complementoHorario = " MXN " + pensiones[i].costoTodoDiaCompartida.ToString("F");

											}
										}

										break;
								}
							}
						}

						//----------------------------------------------------------------------------
                        /* Pintamos los estacionamientos seleccionados a partir del tercero */
						var grid = new Grid();
						var leyautNuevaPension = new StackLayout();
						leyautNuevaPension.HeightRequest = 64f;
						leyautNuevaPension.HorizontalOptions = LayoutOptions.Fill;
						leyautNuevaPension.BackgroundColor = Color.FromHex("#090A19");
						leyautNuevaPension.Orientation = StackOrientation.Horizontal;
						leyautNuevaPension.Margin = new Thickness(0, 24, 0, 0);

						var imagen = new Image();
						imagen.Source = "ico_servicios_pensioncompartida_a.png";
						imagen.WidthRequest = 40f;
						imagen.HeightRequest = 40f;
						imagen.VerticalOptions = LayoutOptions.Center;
						imagen.Margin = new Thickness(16, 0, 0, 0);

						var label = new Label();
						label.Text = estacionamiento.Nombre;
						label.TextColor = Color.White;
						label.FontSize = 14f;
						label.VerticalOptions = LayoutOptions.Center;
						label.Margin = new Thickness(8, 0, 0, 0);

						var ir = new Image();
						ir.Source = "b_ir_n.png";
						ir.WidthRequest = 40f;
						ir.HeightRequest = 40f;
						ir.HorizontalOptions = LayoutOptions.EndAndExpand;

						var botonSegundaPension = new Button();
						botonSegundaPension.VerticalOptions = LayoutOptions.FillAndExpand;
						botonSegundaPension.HorizontalOptions = LayoutOptions.FillAndExpand;
						botonSegundaPension.BackgroundColor = Color.Transparent;
						botonSegundaPension.Clicked += delegate
						{
							if (canEdit)
							{
								Navigation.PushAsync(new P_SeleccionarEstacionamiento(estacionamiento, this, 0));
							}
						};


						//----------------------------------------------------------------------

						var gridHorario = new Grid();
						var leyautNuevoHorario = new StackLayout();
						leyautNuevoHorario.HeightRequest = 64f;
						leyautNuevoHorario.HorizontalOptions = LayoutOptions.Fill;
						leyautNuevoHorario.BackgroundColor = Color.FromHex("#090A19");
						leyautNuevoHorario.Orientation = StackOrientation.Horizontal;
						leyautNuevoHorario.Margin = new Thickness(0, 8, 0, 0);

						var horario = new Image();
						horario.Source = "ico_pensiones_horario_n.png";
						horario.WidthRequest = 40f;
						horario.HeightRequest = 40f;
						horario.VerticalOptions = LayoutOptions.Center;
						horario.Margin = new Thickness(16, 0, 0, 0);

						var labelHorario = new Label();

						if (horarioSeleccionado != "")
						{
                            labelHorario.Text = horarioSeleccionado + complementoHorario;
						}
						else
						{
							labelHorario.Text = "Elegir horario";
						}

						labelHorario.TextColor = Color.White;
						labelHorario.FontSize = 14f;
						labelHorario.VerticalOptions = LayoutOptions.Center;
						labelHorario.Margin = new Thickness(8, 0, 0, 0);

						var irHorario = new Image();
						irHorario.Source = "b_ir_n.png";
						irHorario.WidthRequest = 40f;
						irHorario.HeightRequest = 40f;
						irHorario.HorizontalOptions = LayoutOptions.EndAndExpand;

						var botonHorario = new Button();
						botonHorario.VerticalOptions = LayoutOptions.FillAndExpand;
						botonHorario.HorizontalOptions = LayoutOptions.FillAndExpand;
						botonHorario.BackgroundColor = Color.Transparent;
						botonHorario.Clicked += delegate
						{

							if (canEdit)
							{
								if (comprobarNombreEstacionamiento() == true)
								{
									Navigation.PushAsync(new P_SeleccionarHorarioCompartida(estacionamiento, this, pensionSeleccionada, 0));
								}
								else
								{
									DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar el estacionamiento.", "Aceptar");
									canEdit = true;
								}
							}

						};

						//------------------------------------------------------------------

						leyautNuevaPension.Children.Add(imagen);
						leyautNuevaPension.Children.Add(label);
						leyautNuevaPension.Children.Add(ir);

						grid.Children.Add(leyautNuevaPension);
						grid.Children.Add(botonSegundaPension);

						leyautNuevoHorario.Children.Add(horario);
						leyautNuevoHorario.Children.Add(labelHorario);
						leyautNuevoHorario.Children.Add(irHorario);
						gridHorario.Children.Add(leyautNuevoHorario);
						gridHorario.Children.Add(botonHorario);

						contenedorEstacionamientos.Children.Add(grid);
						contenedorEstacionamientos.Children.Add(gridHorario);

					}
				}
			}
            else
            {
                contenedorEstacionamientos.Children.Clear();
            }


			if (costoHorario != 0.0f)
			{
				lblSubtotal.Text = "MXN " + subtotal.ToString("N") + " / Mes";
			}
			else
			{
				lblSubtotal.Text = "MXN / Mes";
			}
			canEdit = true;
		}

		async void OnCancelarContratacionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopAsync();
			}
		}

		async void OnElegirPeriodoPrimerEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_SeleccionarPeriodoCompartida(estacionamiento, this, 1));

			}
		}

		async void OnElegirPeriodoSegundoEstacionamientoClicked(object sender, EventArgs e)
		{
			//if (canEdit)
			//{
			//	canEdit = false;

			if (horarioSeleccionado != null)
			{
				await Navigation.PushAsync(new P_SeleccionarPeriodoCompartida(estacionamiento, this, 2));
			}
			else
			{
				await DisplayAlert("Error", "Debes seleccionar todas las opciones del primer estacionamiento: " + estacionamiento.Nombre + " para poder continuar", "Aceptar");
			}
			//}
		}


		async void OnElegirHorarioPrimerEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (pensionSeleccionada != -1)
				{
					await Navigation.PushAsync(new P_SeleccionarHorarioCompartida(estacionamiento, this, pensionSeleccionada, 1));
				}
				else
				{
					await DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar un periodo.", "Aceptar");
					canEdit = true;
				}
			}
		}


		async void OnElegirHorarioSegundoEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (segundoEstacionamiento.Nombre != null && segundoEstacionamiento.Nombre != "")
				{
					await Navigation.PushAsync(new P_SeleccionarHorarioCompartida(segundoEstacionamiento, this, pensionSeleccionada, 2));
				}
				else
				{
					await DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar el segundo estacionamiento.", "Aceptar");
					canEdit = true;
				}

			}
		}


		async void OnElegirPeriodoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_SeleccionarPeriodoCompartida(estacionamiento, this, 0));

			}
		}

		async void OnElegirHorarioClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;


				if (pensionSeleccionada != -1)
				{

					await Navigation.PushAsync(new P_SeleccionarHorarioCompartida(estacionamiento, this, pensionSeleccionada, 1));
				}
				else
				{
					await DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar un periodo.", "Aceptar");
					canEdit = true;
				}
			}
		}

		async void OnElegirFormaPagoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_SeleccionarFormaPagoCompartida(this));
			}

		}

		async void OnAgregarEstacionamientoClicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new P_SeleccionarEstacionamiento(estacionamiento, this, 0));
		}


		async void OnAgregarSegundoEstacionamientoClicked(object sender, EventArgs e)
		{
			if (horarioSeleccionado != null)
			{
				await Navigation.PushAsync(new P_SeleccionarEstacionamiento(estacionamiento, this, 1));
			}
			else
			{
				await DisplayAlert("Error", "Debes seleccionar todas las opciones del primer estacionamiento: " + estacionamiento.Nombre + " para poder continuar", "Aceptar");
			}
		}


        /*Método para agregar nuevos estacionamientos a partir del tercero*/
		void OnAgregarPensionClicked(object sender, EventArgs e)
		{

			int numeroCasillasAgregadas = contenedorEstacionamientos.Children.Count();
			numeroCasillasAgregadas = numeroCasillasAgregadas / 2;

			if (segundoEstacionamiento.Nombre != null)
			{

				if (horarioSeleccionadoSegunda != null)
				{

                    //if (contenedorEstacionamientos.Children.Count + 2 <= totalEstacionamientosDisponibles - 1)
                    //{

                    if (contenedorEstacionamientos.Children.Count + 1 <= totalEstacionamientosDisponibles)
					{

						if (listaEstacionamientosParaSeleccionar.Count() > 0)
						{

							if (numeroCasillasAgregadas < estacionamientosParaAgregar)
							{

								//----------------------------------------------------------------------------
								var grid = new Grid();
								var leyautNuevaPension = new StackLayout();
								leyautNuevaPension.HeightRequest = 64f;
								leyautNuevaPension.HorizontalOptions = LayoutOptions.Fill;
								leyautNuevaPension.BackgroundColor = Color.FromHex("#090A19");
								leyautNuevaPension.Orientation = StackOrientation.Horizontal;
								leyautNuevaPension.Margin = new Thickness(0, 24, 0, 0);

								var imagen = new Image();
								imagen.Source = "ico_servicios_pensioncompartida_a.png";
								imagen.WidthRequest = 40f;
								imagen.HeightRequest = 40f;
								imagen.VerticalOptions = LayoutOptions.Center;
								imagen.Margin = new Thickness(16, 0, 0, 0);

								var label = new Label();
								label.Text = "Elegir estacionamiento";
								label.TextColor = Color.White;
								label.FontSize = 14f;
								label.VerticalOptions = LayoutOptions.Center;
								label.Margin = new Thickness(8, 0, 0, 0);

								var ir = new Image();
								ir.Source = "b_ir_n.png";
								ir.WidthRequest = 40f;
								ir.HeightRequest = 40f;
								ir.HorizontalOptions = LayoutOptions.EndAndExpand;

								var botonSegundaPension = new Button();
								botonSegundaPension.VerticalOptions = LayoutOptions.FillAndExpand;
								botonSegundaPension.HorizontalOptions = LayoutOptions.FillAndExpand;
								botonSegundaPension.BackgroundColor = Color.Transparent;
								botonSegundaPension.Clicked += delegate
								{
									if (canEdit)
									{
										Navigation.PushAsync(new P_SeleccionarEstacionamiento(estacionamientoActualSeleccionado, this, 0));

									}
								};


								var gridHorario = new Grid();
								var leyautNuevoHorario = new StackLayout();
								leyautNuevoHorario.HeightRequest = 64f;
								leyautNuevoHorario.HorizontalOptions = LayoutOptions.Fill;
								leyautNuevoHorario.BackgroundColor = Color.FromHex("#090A19");
								leyautNuevoHorario.Orientation = StackOrientation.Horizontal;
								leyautNuevoHorario.Margin = new Thickness(0, 8, 0, 0);

								var horario = new Image();
								horario.Source = "ico_pensiones_horario_n.png";
								horario.WidthRequest = 40f;
								horario.HeightRequest = 40f;
								horario.VerticalOptions = LayoutOptions.Center;
								horario.Margin = new Thickness(16, 0, 0, 0);

								var labelHorario = new Label();
								labelHorario.Text = "Elegir horario";
								labelHorario.TextColor = Color.White;
								labelHorario.FontSize = 14f;
								labelHorario.VerticalOptions = LayoutOptions.Center;
								labelHorario.Margin = new Thickness(8, 0, 0, 0);

								var irHorario = new Image();
								irHorario.Source = "b_ir_n.png";
								irHorario.WidthRequest = 40f;
								irHorario.HeightRequest = 40f;
								irHorario.HorizontalOptions = LayoutOptions.EndAndExpand;

								var botonHorario = new Button();
								botonHorario.VerticalOptions = LayoutOptions.FillAndExpand;
								botonHorario.HorizontalOptions = LayoutOptions.FillAndExpand;
								botonHorario.BackgroundColor = Color.Transparent;
								botonHorario.Clicked += delegate
								{
									if (canEdit)
									{
										if (comprobarNombreEstacionamiento() == true)
										{

											Navigation.PushAsync(new P_SeleccionarHorarioCompartida(estacionamientoActualSeleccionado, this, pensionSeleccionada, 0));
										}
										else
										{
											DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar el estacionamiento.", "Aceptar");
											canEdit = true;
										}
									}
								};



								//------------------------------------------------------------------

								leyautNuevaPension.Children.Add(imagen);
								leyautNuevaPension.Children.Add(label);
								leyautNuevaPension.Children.Add(ir);

								grid.Children.Add(leyautNuevaPension);
								grid.Children.Add(botonSegundaPension);

								leyautNuevoHorario.Children.Add(horario);
								leyautNuevoHorario.Children.Add(labelHorario);
								leyautNuevoHorario.Children.Add(irHorario);
								gridHorario.Children.Add(leyautNuevoHorario);
								gridHorario.Children.Add(botonHorario);

								contenedorEstacionamientos.Children.Add(grid);
								contenedorEstacionamientos.Children.Add(gridHorario);
							}
							else
							{
								DisplayAlert("Error", "No existen más estacionamientos con pensiones para poder seleccionar.", "Aceptar");
							}
						}
						else
						{
							DisplayAlert("Error", "No existen más estacionamientos con pensiones para poder seleccionar.", "Aceptar");
						}
					}


					else
					{
						DisplayAlert("Error", "No existen más estacionamientos con pensiones para poder seleccionar.", "Aceptar");
					}
				}
				else
				{
					DisplayAlert("Error", "Para poder agregar más estacionamientos primero debes seleccionar el segundo horario.", "Aceptar");
				}
			}
			else
			{
				DisplayAlert("Error", "Para poder agregar más estacionamientos primero debes seleccionar el segundo estacionamiento.", "Aceptar");
			}
		}


		bool comprobarNombreEstacionamiento()
		{
			bool respuesta = false;

			int numeroEstacionamientosAgregados = 0;

			foreach (Estacionamiento estacionamiento in estacionamientosSeleccionados)
			{
				if (estacionamiento != segundoEstacionamiento && estacionamiento != primerEstacionamiento)
				{
					numeroEstacionamientosAgregados++;

					if (estacionamiento.Nombre != null)
					{
						respuesta = true;
					}
				}
			}

			if (numeroEstacionamientosAgregados > 0 && respuesta)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		async void OnConfirmarContratacionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (pensionSeleccionada != -1)
				{
					if (horarioSeleccionado != null)
					{

						if (segundoEstacionamiento.Nombre != null)
						{

							if (horarioSeleccionadoSegunda != null)
							{

								if (metodoPagoSeleccionado != null)
								{

									activityIndicator.IsVisible = true;

									await Navigation.PushAsync(new P_ConfirmarPagoPensionCompartida(this));

								}
								else
								{
									await DisplayAlert("Error", "Para poder continuar debes seleccionar un método de pago.", "Aceptar");
								}

							}
							else
							{
								await DisplayAlert("Error", "Para poder continuar debes seleccionar un horario del segundo estacionamiento .", "Aceptar");
							}

						}
						else
						{
							await DisplayAlert("Error", "Para poder continuar debes seleccionar el segundo estacionamiento .", "Aceptar");
						}

					}
					else
					{
						await DisplayAlert("Error", "Para poder continuar debes seleccionar un horario del primer estacionamiento.", "Aceptar");
					}
				}
				else
				{
					await DisplayAlert("Error", "Para poder continuar debes seleccionar un periodo.", "Aceptar");
				}

				canEdit = true;
			}
		}
	}


	public class EstacionamientoSeleccionado
	{
		public int idEstacionamiento { get; set; }
		public int idPensionSeleccionada { get; set; }
		public string horarioSeleccionado { get; set; }
		public int idPeriodo { get; set; }
		public float costoHorarioSeleccionado { get; set; }
		public Pension[] Pensiones { get; set; }

	}
}
