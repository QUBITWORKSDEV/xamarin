using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_DetallesPension : FunkContentPage
	{
		PensionContratada pensionContratada;
		PensionContratadaCompartida pensionContratadaCompartida;
		int tipoPension = -1;

		public P_DetallesPension(PensionContratada pc)
		{
			pensionContratada = pc;

			InitializeComponent();
			checkConnectivity();

			BindingContext = pensionContratada;
			lblWarningNoConnectivity.BindingContext = this;
		}


		public P_DetallesPension(PensionContratadaCompartida pc)
		{
			pensionContratada = new PensionContratada();
			pensionContratada.tipo = 3;
			pensionContratadaCompartida = pc;

			InitializeComponent();
			checkConnectivity();

			BindingContext = pensionContratada;
			lblWarningNoConnectivity.BindingContext = this;
		}

		async void actualizar()
		{
			if (isOffline)
				return;

			canEdit = false;

			if (pensionContratada.tipo == 1)
			{
				tipoPension = 1;

				WsResponse<Estacionamiento> items = await WsConsumer.Consume<Estacionamiento>("data", "getEstacionamiento", "{\"idEstacionamiento\":\"" + pensionContratada.idEstacionamiento + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						txtNombre.Text = items.datos.Nombre;


						//---- sobre el período
						int periodo = -1;
						Pension[] pensiones = new Pension[0];
						pensiones = items.datos.Pensiones;

						for (int i = 0; i < pensiones.Length; i++)
						{
							if (pensiones[i].idPension == pensionContratada.idPension)
							{
								periodo = pensiones[i].periodo;
								break;
							}
						}

						/*DateTime ahora = DateTime.Now;
						var final = ahora.AddMonths(periodo);
						lblPeriodo.Text = ahora.ToString("dd-MM-yy") + " al " + final.ToString("dd-MM-yy");*/
						var final = pensionContratada.fechaContratacion.AddMonths(periodo);
						lblPeriodo.Text = pensionContratada.fechaContratacion.ToString("dd-MM-yy") + " al " + final.ToString("dd-MM-yy");
						//---------------------


						if (pensionContratada.horario == 1)
						{
							txtHorario.Text = "7:00 - 19:00";
						}
						else if (pensionContratada.horario == 3)
						{
							txtHorario.Text = "19:00 - 7:00";
						}
						else if (pensionContratada.horario == 5)
						{
							txtHorario.Text = "Todo el día";
						}

						txtDireccion.Text = items.datos.Direccion;
						txtEmpresa.Text = items.datos.Operadora;

						imgPensionCompartida.IsVisible = false;
						contenedorEstacionamientos.IsVisible = false;

						if (pensionContratada.tipo == 1)
						{
							txtTipo.Text = "Pensión normal";
						}
						else if (pensionContratada.tipo == 3)
						{
							txtTipo.Text = "Pensión compartida";
						}

						if (pensionContratada.status == 7)
						{
							btnCancelarPension.IsVisible = false;
							lblParcialmenteCancelada.IsVisible = true;
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener la lista de estacionamientos.", "Aceptar");
					}
				}
				else
				{
					await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
				}
			}
			else if (pensionContratada.tipo == 3)
			{
				tipoPension = 3;

				WsResponse<Estacionamiento[]> items2 = await WsConsumer.Consume<Estacionamiento[]>("data", "getEstPensionCompartida", "{\"idCompartida\":\"" + pensionContratadaCompartida.idCompartida + "\"}");

				if (items2 != null)
				{
					if (items2.respuesta == "ok")
					{
						var total = 0.0f;

						txtNombre.Text = "";
						txtNombre.IsVisible = false;
						imgPensionNormal.IsVisible = false;
						lblHorario.IsVisible = false;
						lblDireccion.IsVisible = false;



						//---- sobre el período
						int periodo = -1;
						Pension[] pensiones = new Pension[0];
						pensiones = items2.datos[0].Pensiones;

						for (int i = 0; i < pensiones.Length; i++)
						{
							if (pensiones[i].idPension == pensionContratadaCompartida.pensionados[0].idPension)
							{
								periodo = pensiones[i].periodo;
								break;
							}
						}

						/*DateTime ahora = DateTime.Now;
						var final = ahora.AddMonths(periodo);
						lblPeriodo.Text = ahora.ToString("dd-MM-yy") + " al " + final.ToString("dd-MM-yy");*/
						var final = pensionContratadaCompartida.pensionados[0].fechaContratacion.AddMonths(periodo);
						lblPeriodo.Text = pensionContratadaCompartida.pensionados[0].fechaContratacion.ToString("dd-MM-yy") + " al " + final.ToString("dd-MM-yy");
						//---------------------


						for (int i = 0; i < pensionContratadaCompartida.pensionados.Length; i++)
						{
							total += pensionContratadaCompartida.pensionados[i].costo;

							var leyautEstacionamiento = new StackLayout();
							leyautEstacionamiento.HeightRequest = 48f;
							leyautEstacionamiento.HorizontalOptions = LayoutOptions.Fill;
							leyautEstacionamiento.BackgroundColor = Color.FromHex("#2C2A2D");
							leyautEstacionamiento.Orientation = StackOrientation.Horizontal;
							leyautEstacionamiento.Padding = new Thickness(8, 0, 8, 0);

							var imagen = new Image();
							imagen.Source = "ico_servicios_pensioncompartida_a.png";
							imagen.WidthRequest = 48f;
							imagen.HeightRequest = 48f;

							var label = new Label();
							label.Text = pensionContratadaCompartida.pensionados[i].nombreEstacionamiento;
							label.TextColor = Color.White;
							label.FontSize = 14f;
							label.VerticalOptions = LayoutOptions.Center;
							label.LineBreakMode = LineBreakMode.WordWrap;

							var horario = new Label();

							if (pensionContratadaCompartida.pensionados[i].horario == 1)
							{
								horario.Text = "Día";
							}
							else if (pensionContratadaCompartida.pensionados[i].horario == 3)
							{
								horario.Text = "Noche";
							}
							else if (pensionContratadaCompartida.pensionados[i].horario == 5)
							{
								horario.Text = "Todo el día";
							}

							horario.TextColor = Color.White;
							horario.FontSize = 14f;
							horario.VerticalOptions = LayoutOptions.Center;
							horario.HorizontalOptions = LayoutOptions.EndAndExpand;
							//horario.LineBreakMode = LineBreakMode.WordWrap;

							leyautEstacionamiento.Children.Add(imagen);
							leyautEstacionamiento.Children.Add(label);
							leyautEstacionamiento.Children.Add(horario);

							contenedorEstacionamientos.Children.Add(leyautEstacionamiento);


							if (pensionContratadaCompartida.pensionados[i].status == 7)
							{
								btnCancelarPension.IsVisible = false;


								lblParcialmenteCancelada.IsVisible = true;
							}


						}

						costo.Text = total.ToString("N");

						if (pensionContratada.tipo == 1)
						{
							txtTipo.Text = "Pensión";
						}
						else if (pensionContratada.tipo == 3)
						{
							txtTipo.Text = "Pensión compartida";
						}

					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener la lista de estacionamientos.", "Aceptar");
					}
				}
				else
				{
					await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
				}
			}

			canEdit = true;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			actualizar();
		}

		async void OnCancelarClicked(object sender, EventArgs e)
		{
			bool r = await DisplayAlert("", "¿De verdad deseas cancelar esta pensión?", "Cancelar Pensión", "Regresar (No cancelar pensión)");
			if (r == true)
			{
				if (isOffline)
					return;

				canEdit = false;


				string idPensionado = "";

				if (tipoPension == 1)
				{
					idPensionado = pensionContratada.idPensionado.ToString();
				}
				else if (tipoPension == 3)
				{
					idPensionado = pensionContratadaCompartida.idCompartida;
				}

				WsResponse<int> items = await WsConsumer.Consume<int>("operations", "cancelarPension", "{\"idPensionado\":\"" + idPensionado + "\", \"tipoPension\":\"" + tipoPension + "\"}");



				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos == 1)
						{
							// todo: regresamos a la pantalla anterior
							await Navigation.PopAsync();
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al cancelar la pensión.", "Aceptar");
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener la lista de estacionamientos.", "Aceptar");
					}
				}
				else
				{
					await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
				}

				canEdit = true;
			}
		}

		async void OnTerminosClicked(object sender, EventArgs e)
		{

		}

	}
}