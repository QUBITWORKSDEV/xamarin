﻿using System;
using System.Linq;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;


namespace Veepe
{

	public partial class P_ConfirmarPagoPensionCompartida : FunkContentPage
	{
		P_ContratarPensionCompartida padre;
		Usuario usuario;

		int idMetodopago = -1;
		//int horario = 0;
		//float costo = 0;

		public P_ConfirmarPagoPensionCompartida(P_ContratarPensionCompartida pPadre)
		{
			usuario = SessionManager.getLocalUser();

			padre = pPadre;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			lblCostoPension.Text = padre.subtotal.ToString("F");
			lblMesesContratados.Text = padre.periodoSelecionado;
			lblTarjeta.Text = padre.metodoPagoSeleccionado.NombreCuenta;

			idMetodopago = padre.metodoPagoSeleccionado.idMetodoPago;

			contenedorEstacionamientos.Children.Clear();

			//primer estacionamiento------------------
			var pleyautEstacionamiento = new StackLayout();
			pleyautEstacionamiento.HeightRequest = 48f;
			pleyautEstacionamiento.HorizontalOptions = LayoutOptions.Fill;
			pleyautEstacionamiento.BackgroundColor = Color.Transparent; // FromHex("#2C2A2D");
			pleyautEstacionamiento.Orientation = StackOrientation.Horizontal;
			pleyautEstacionamiento.Margin = new Thickness(20, 8, 20, 0);

			var pimagen = new Image();
			pimagen.Source = "ico_servicios_pensioncompartida_a.png";
			pimagen.WidthRequest = 48f;
			pimagen.HeightRequest = 48f;

			var plabel = new Label();
			plabel.Text = padre.primerEstacionamiento.Nombre;
			plabel.TextColor = Color.White;
			plabel.FontSize = 14f;
			plabel.VerticalOptions = LayoutOptions.Center;
			plabel.LineBreakMode = LineBreakMode.WordWrap;

			pleyautEstacionamiento.Children.Add(pimagen);
			pleyautEstacionamiento.Children.Add(plabel);

			contenedorEstacionamientos.Children.Add(pleyautEstacionamiento);

			//segundo estacionamiento------------------
			var sleyautEstacionamiento = new StackLayout();
			sleyautEstacionamiento.HeightRequest = 48f;
			sleyautEstacionamiento.HorizontalOptions = LayoutOptions.Fill;
			sleyautEstacionamiento.BackgroundColor = Color.Transparent; // FromHex("#2C2A2D");
			sleyautEstacionamiento.Orientation = StackOrientation.Horizontal;
			sleyautEstacionamiento.Margin = new Thickness(20, 8, 20, 0);

			var simagen = new Image();
			simagen.Source = "ico_servicios_pensioncompartida_a.png";
			simagen.WidthRequest = 48f;
			simagen.HeightRequest = 48f;

			var slabel = new Label();
			slabel.Text = padre.segundoEstacionamiento.Nombre;
			slabel.TextColor = Color.White;
			slabel.FontSize = 14f;
			slabel.VerticalOptions = LayoutOptions.Center;
			slabel.LineBreakMode = LineBreakMode.WordWrap;

			sleyautEstacionamiento.Children.Add(simagen);
			sleyautEstacionamiento.Children.Add(slabel);

			contenedorEstacionamientos.Children.Add(sleyautEstacionamiento);

			//otros estacionamientos-------------------

			if (padre.estacionamientosSeleccionados.Count > 0)
			{
				foreach (Estacionamiento est in padre.estacionamientosSeleccionados)
				{

					if (est != padre.segundoEstacionamiento && est != padre.primerEstacionamiento)
					{
						var leyautEstacionamiento = new StackLayout();
						leyautEstacionamiento.HeightRequest = 48f;
						leyautEstacionamiento.HorizontalOptions = LayoutOptions.Fill;
						leyautEstacionamiento.BackgroundColor = Color.Transparent; // FromHex("#2C2A2D");
						leyautEstacionamiento.Orientation = StackOrientation.Horizontal;
						leyautEstacionamiento.Margin = new Thickness(20, 8, 20, 0);

						var imagen = new Image();
						imagen.Source = "ico_servicios_pensioncompartida_a.png";
						imagen.WidthRequest = 48f;
						imagen.HeightRequest = 48f;

						var label = new Label();
						label.Text = est.Nombre;
						label.TextColor = Color.White;
						label.FontSize = 14f;
						label.VerticalOptions = LayoutOptions.Center;
						label.LineBreakMode = LineBreakMode.WordWrap;

						leyautEstacionamiento.Children.Add(imagen);
						leyautEstacionamiento.Children.Add(label);

						contenedorEstacionamientos.Children.Add(leyautEstacionamiento);

					}
				}
			}
			padre.estacionamientosSeleccionados.Add(padre.primerEstacionamiento);
			padre.estacionamientosSeleccionados.Add(padre.segundoEstacionamiento);
		}



		async void OnConfirmarPagoClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				//--------------------------------------------------------

				string estacionamientos = "";
				string horarios = "";
				string costos = "";

				//----------------Pensiones seleccionadas para el primer y segundo estacionamiento
				int ultimo = padre.pensionesSeleccionadas.Last();
				foreach (int idPension in padre.pensionesSeleccionadas)
				{
					estacionamientos += idPension.ToString();

					if (!idPension.Equals(ultimo))
					{
						estacionamientos += ",";
					}

				}


				//------------------
				if (padre.estacionamientosHorarioSeleccionado.Count() > 0) { 
					foreach (EstacionamientoSeleccionado ests2 in padre.estacionamientosHorarioSeleccionado)
					{
						estacionamientos += ",";
						estacionamientos += ests2.idPensionSeleccionada.ToString();
					}
				}

				//-------Horarios--------------------

				horarios += getHorario(padre.horarioSeleccionado.ToString()); 
				horarios += ",";
				horarios += getHorario(padre.horarioSeleccionadoSegunda.ToString());

				if (padre.estacionamientosHorarioSeleccionado.Count() > 0)
				{
					foreach (EstacionamientoSeleccionado ests3 in padre.estacionamientosHorarioSeleccionado)
					{
						horarios += ",";
						horarios += getHorario(ests3.horarioSeleccionado.ToString());
					}
				}

				//-----Costos------------------------

				costos += padre.costoHorario.ToString();
				costos += ",";
				costos += padre.costoHorarioSegunda.ToString();

				if (padre.estacionamientosHorarioSeleccionado.Count() > 0)
				{
					foreach (EstacionamientoSeleccionado ests4 in padre.estacionamientosHorarioSeleccionado)
					{
						costos += ",";
						costos += ests4.costoHorarioSeleccionado;
					}
				}

				//--------Generar el id_compartida-----------------------
				string idCompartida = "";
				idCompartida += padre.primerEstacionamiento.idEstacionamiento.ToString();
				idCompartida += padre.primerEstacionamiento.Nombre[0].ToString();
				idCompartida += padre.segundoEstacionamiento.Nombre[0].ToString();
				idCompartida += usuario.idUsuario.ToString();
				idCompartida += new Random().Next(0, 1000).ToString();

				//------------------------------

				WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "contratarpensioncompartida", "{\"idUsuario\":\"" + usuario.idUsuario + "\", \"veepeId\":\"" + usuario.veepeId + "\", \"listaEstacionamientos\":\"" + estacionamientos + "\", \"idMetodopago\":\"" + idMetodopago + "\", \"horarios\":\"" + horarios + "\", \"idCompartida\":\"" + idCompartida + "\", \"costo\":\"" + costos + "\", \"numeroPeriodos\":\"" + padre.periodoSeleccionadoN + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos == true)
						{
							//await Navigation.PushAsync(new P_ContratacionExitosa());
							await DisplayAlert("", "La contratación fue exitosa. Puedes consultarla en la sección Pensiones.", "Cerrar");
							await Navigation.PopToRootAsync();
						}
						else
						{
							await DisplayAlert("Error", "Sucedió un error al contratar la pensión. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al contratar la pensión. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		void OnTerminosEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				// TODO: mostrar los términos del estacionamiento
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

		int getHorario(string horario)
		{

			int res = 0;
			switch (horario)
			{
				case "Día":
					res = 1;
					break;


				case "Noche":
					res = 3;
					break;


				case "Todo el día":
					res = 5;
					break;
			}

			return res;
		}

	}
}
