using System;
using Xamarin.Forms;
using FunkFramework.UI;

namespace Veepe
{
	public partial class P_SeleccionarPeriodoCompartida : FunkContentPage
	{

		readonly Estacionamiento estacionamiento;
		P_ContratarPensionCompartida padre;

		public P_SeleccionarPeriodoCompartida(Estacionamiento pEstacionamiento, P_ContratarPensionCompartida pPadre, int numeroEstacionamiento)
		{
			estacionamiento = pEstacionamiento;
			padre = pPadre;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			if (estacionamiento.Pensiones.Length > 0)
			{

				for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
				{

					if (estacionamiento.Pensiones[i].costoDiaCompartida != 0 || estacionamiento.Pensiones[i].costoNocheCompartida != 0 || estacionamiento.Pensiones[i].costoTodoDiaCompartida != 0)//Verificamos que el periodo tenga al menos un horario configurado
					{

						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Center;
						leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						//---creamos el nombre
						var nombre = new FunkButton();

						if (estacionamiento.Pensiones[i].periodo == 1)
						{
							nombre.Text = estacionamiento.Pensiones[i].periodo + " Mes";
						}
						else
						{
							nombre.Text = estacionamiento.Pensiones[i].periodo + " Meses";
						}

						nombre.TextColor = Color.FromHex("#00BFD6");
						nombre.FontSize = 16;
						nombre.FontFamily = "BebasNeue";
						nombre.HorizontalOptions = LayoutOptions.FillAndExpand;
						nombre.VerticalOptions = LayoutOptions.Center;
						nombre.BackgroundColor = Color.Transparent;
						nombre.CommandParameter = estacionamiento.Pensiones[i].idPension;

						nombre.Clicked += delegate
						{
							if (canEdit)
							{

								if (numeroEstacionamiento == 1)
								{
									padre.pensionSeleccionada = Convert.ToInt32(nombre.CommandParameter);
									padre.horarioSeleccionado = null;

									padre.pensionSeleccionadaSegunda = -1;
									padre.nombreSegundoEstacionamiento = "";
									padre.horarioSeleccionadoSegunda = null;
									padre.costoHorarioSegunda = 0.0f;

									padre.segundoEstacionamiento.Nombre = null;

									padre.pensionesSeleccionadas.Clear();
									padre.estacionamientosSeleccionados.Clear();

									padre.getEstacionamientos();  
								 
								}
								else if (numeroEstacionamiento == 2)
								{
									padre.pensionSeleccionadaSegunda = Convert.ToInt32(nombre.CommandParameter);
									padre.horarioSeleccionadoSegunda = null;
								}

								canEdit = false;
								Navigation.PopAsync();

							}
						};

						leyaut.Children.Add(nombre);

						ContenedorPeriodos.Children.Add(leyaut);

					}
				}
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}
	}
}
