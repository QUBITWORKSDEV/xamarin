using Xamarin.Forms;
using System;
using System.Diagnostics;
using FunkFramework.UI;

namespace Veepe
{
	public partial class P_SeleccionarHorario : FunkContentPage
	{
		readonly Estacionamiento estacionamiento;
		P_ContratarPension padre;
		int pensionSeleccionada;

		public P_SeleccionarHorario(Estacionamiento pEstacionamiento, P_ContratarPension pPadre, int pPensionSeleccionada)
		{
			estacionamiento = pEstacionamiento;
			padre = pPadre;
			pensionSeleccionada = pPensionSeleccionada;

			InitializeComponent();
			checkConnectivity();

			BindingContext = this;
			refresh();
		}

		void refresh()
		{
			for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
			{
				Debug.WriteLine(estacionamiento.Pensiones[i].idPension + "::" + pensionSeleccionada);
				if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
				{
					if (estacionamiento.Pensiones[i].costoDia > 0f)
					{
						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Center;
						leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						leyaut.Children.Add(crearYConfigurarBoton("Día (7:00 - 19:00) " + "MXN " + estacionamiento.Pensiones[i].costoDia.ToString("N"), "1|"+estacionamiento.Pensiones[i].costoDia));
						ContenedorHorarios.Children.Add(leyaut);
					}

					if (estacionamiento.Pensiones[i].costoNoche > 0f)
					{
						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Center;
						leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						leyaut.Children.Add(crearYConfigurarBoton("Noche (19:00 - 7:00) " + "MXN " + estacionamiento.Pensiones[i].costoNoche.ToString("N"), "3|"+estacionamiento.Pensiones[i].costoNoche));
						ContenedorHorarios.Children.Add(leyaut);
					}
				
					if (estacionamiento.Pensiones[i].costoTodoDia > 0f)
					{
						var leyaut = new StackLayout();
						leyaut.HeightRequest = 48f;
						leyaut.HorizontalOptions = LayoutOptions.Fill;
						leyaut.VerticalOptions = LayoutOptions.Center;
						leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
						leyaut.Orientation = StackOrientation.Horizontal;
						leyaut.Margin = new Thickness(0, 0, 0, 8);

						// creamos el nombre
						leyaut.Children.Add(crearYConfigurarBoton("Todo el día " + "MXN " + estacionamiento.Pensiones[i].costoTodoDia.ToString("N"), "5|"+estacionamiento.Pensiones[i].costoTodoDia));
						ContenedorHorarios.Children.Add(leyaut);
					}
				}
			}

		}

		void OnHorarioClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				var p = (sender as FunkButton).CommandParameter.ToString().Split('|');
				padre.costoHorario = float.Parse(p[1]);
				padre.horarioSeleccionado = int.Parse(p[0]);
				Navigation.PopAsync();
			}
		}

		FunkButton crearYConfigurarBoton(string texto, string valor)
		{
			var res = new FunkButton();
			res.Text = texto;
			res.TextColor = Color.FromHex("#00BFD6");
			res.FontSize = 16;
			res.FontFamily = "BebasNeue";
			res.HorizontalOptions = LayoutOptions.FillAndExpand;
			res.VerticalOptions = LayoutOptions.Center;
			res.BackgroundColor = Color.Transparent;
			res.CommandParameter = valor;
			res.Clicked += OnHorarioClicked;

			return res;
		}

	}
}
