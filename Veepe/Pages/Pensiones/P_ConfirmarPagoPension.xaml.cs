using System;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_ConfirmarPagoPension : FunkContentPage
	{
		Estacionamiento estacionamiento;
		P_ContratarPension padre;
		Usuario usuario;

		int idPension = -1;
		int idMetodopago = -1;
		int horario = 0;
		//float costo = 0;

		public P_ConfirmarPagoPension() { }

		public P_ConfirmarPagoPension(Estacionamiento pEstacionamiento, P_ContratarPension pPadre, int pensionSeleccionada, int horario, float subtotal)
		{
			usuario = SessionManager.getLocalUser();
			estacionamiento = pEstacionamiento;
			padre = pPadre;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			lblCostoPension.Text = padre.costoHorario.ToString("F2"); // subtotal.ToString("F");

			for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
			{
				if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
				{
					lblMesesContratados.Text = estacionamiento.Pensiones[i].periodo.ToString() + " Meses";
					idPension = estacionamiento.Pensiones[i].idPension;

					lblEstacionamiento.Text = estacionamiento.Nombre;
					lblTarjeta.Text = padre.metodoPagoSeleccionado.NombreCuenta;

					// sobre el período
					DateTime ahora = DateTime.Now;
					var final = ahora.AddMonths(estacionamiento.Pensiones[i].periodo);
					lblPeriodo.Text = ahora.ToString("dd-MM-yy") + " al " + final.ToString("dd-MM-yy");

					idMetodopago = padre.metodoPagoSeleccionado.idMetodoPago;
					//costo = padre.subtotal;
				}
			}


			/*switch (padre.horarioSeleccionado)
			{
				case "Día":
					for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
					{
						if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
						{
							horario = 1;
						}
					}
					break;

				case "Noche":
					for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
					{
						if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
						{
							horario = 3;
						}
					}
					break;

				case "Todo el día":
					for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
					{
						if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
						{
							horario = 5;
						}
					}
					break;

					default:
						Console.WriteLine("Default case");
						break;
			}*/
		}


		async void OnConfirmarPagoClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				WsResponse<int> items = await WsConsumer.Consume<int>("operations", "contratarPension", "{\"idUsuario\":\"" + usuario.idUsuario + "\", \"idPension\":\"" + idPension + "\", \"idMetodopago\":\"" + idMetodopago + "\", \"horario\":\"" + padre.horarioSeleccionado + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						switch (items.datos)
						{
							case 1:
								await DisplayAlert("", "La contratación fue exitosa. Puedes consultarla en la sección Pensiones.", "Cerrar");
								await Navigation.PopToRootAsync();
								break;

							case 3:
								await DisplayAlert("Error", "Ya tienes una pensión contratada con esta configuración, por favor selecciona otra opción.", "Aceptar");
								break;

							case 5:
								await DisplayAlert("Error", "No se puede contratar la pensión ya que no existen lugares disponibles en el estacionamiento.", "Aceptar");
								break;

							default:
								await DisplayAlert("Error", items.mensaje, "Aceptar");
								break;
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al contratar la pensión. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		void OnTerminosEstacionamientoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				Navigation.PushAsync(new P_TerminosEstacionamiento(estacionamiento));
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

	}
}