﻿using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_SeleccionarFormaPagoCompartida : FunkContentPage
	{
		Usuario usuario;
		P_ContratarPensionCompartida padre;

		public P_SeleccionarFormaPagoCompartida(P_ContratarPensionCompartida pPadre)
		{
			padre = pPadre;
			usuario = SessionManager.getLocalUser();
			InitializeComponent();
			checkConnectivity();
		}

		public async void actualizar()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				// activityIndicator.IsVisible = true;

				WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeId\":\"" + usuario.veepeId + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						usuario = items.datos as Usuario;
						listaMetodos.ItemsSource = usuario._metodosPago;

						if (usuario._metodosPago.Count < 1)
						{
							await DisplayAlert("", "Para poder user Veepe necesitas al menos un método de pago.", "Aceptar");
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un error al obtener los métodos de pago. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				canEdit = true;
				// activityIndicator.IsVisible = false;
			}
		}

		async void OnMetodoSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (e.SelectedItem == null)
				{
					return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
				}

				//await Navigation.PushAsync(new P_MetodoPago(e.SelectedItem as MetodoPago));
				padre.metodoPagoSeleccionado = e.SelectedItem as MetodoPago;
				await Navigation.PopAsync();



				// es necesario desactivar?
				//((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			actualizar();
			canEdit = true;
		}


	}
}
