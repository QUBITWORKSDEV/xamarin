using System;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_SeleccionarEstacionamiento : FunkContentPage
	{
		readonly Estacionamiento estacionamiento;
		P_ContratarPensionCompartida padre;
		int _opcion = 0;


		public P_SeleccionarEstacionamiento(Estacionamiento pEstacionamiento, P_ContratarPensionCompartida pPadre, int opcion)
		{
			_opcion = opcion;
			estacionamiento = pEstacionamiento;
			padre = pPadre;

			InitializeComponent();
			checkConnectivity();

			BindingContext = this;
		}


		protected override void OnAppearing()
		{
			base.OnAppearing();
			getEstacionamientos();
		}


		public  void getEstacionamientos()
		{
			if (padre.arregloEstacionamientosParaSeleccionar.Length > 0)
			{
				listaEstacionamientos.ItemsSource = padre.arregloEstacionamientosParaSeleccionar;
			}else
			{
				mensajeSinEstacionamientos.IsVisible = true;
			}
		}

		async void OnEstacionamientoSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				var estacionamientoSeleccionado = e.SelectedItem as Estacionamiento;

				if(_opcion == 1)
				{
					if (padre.segundoEstacionamiento.idEstacionamiento != 0)
					{
						padre.estacionamientosSeleccionados.Remove(padre.segundoEstacionamiento);
						padre.listaEstacionamientosParaSeleccionar.Add(padre.segundoEstacionamiento);
					}

					padre.getEstacionamientos(estacionamientoSeleccionado);
					padre.segundoEstacionamiento = estacionamientoSeleccionado;
					padre.nombreSegundoEstacionamiento = estacionamientoSeleccionado.Nombre;
					padre.estacionamientosSeleccionados.Add(estacionamientoSeleccionado);
				}
				else
				{
					padre.getEstacionamientos(estacionamientoSeleccionado);
					padre.estacionamientosSeleccionados.Add(estacionamientoSeleccionado);
					padre.estacionamientoActualSeleccionado = estacionamientoSeleccionado;
				}

				canEdit = false;
				await Navigation.PopAsync();
			}
		}
	}
}
