﻿using System;
using Xamarin.Forms;
using FunkFramework.UI;

namespace Veepe
{
	public partial class P_SeleccionarPeriodo : FunkContentPage
	{

		readonly Estacionamiento estacionamiento;
		P_ContratarPension padre;

		public P_SeleccionarPeriodo(Estacionamiento pEstacionamiento, P_ContratarPension pPadre)
		{
			estacionamiento = pEstacionamiento;
			padre = pPadre;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
			{
				var leyaut = new StackLayout();
				leyaut.HeightRequest = 48f;
				leyaut.HorizontalOptions = LayoutOptions.Fill;
				leyaut.VerticalOptions = LayoutOptions.Center;
				leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
				leyaut.Orientation = StackOrientation.Horizontal;
				leyaut.Margin = new Thickness(0, 0, 0, 8);


				// creamos el nombre
				var nombre = new FunkButton();
				nombre.Text = estacionamiento.Pensiones[i].periodoReadable;
				nombre.TextColor = Color.FromHex("#00BFD6");
				nombre.FontSize = 16;
				nombre.FontFamily = "BebasNeue";
				nombre.HorizontalOptions = LayoutOptions.FillAndExpand;
				nombre.VerticalOptions = LayoutOptions.Center;
				nombre.BackgroundColor = Color.Transparent;
				nombre.CommandParameter = estacionamiento.Pensiones[i].idPension;

				nombre.Clicked += OnPeriodoclicked;

				leyaut.Children.Add(nombre);

				ContenedorPeriodos.Children.Add(leyaut);
			}
		}

		async void OnPeriodoclicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				padre.pensionSeleccionada = (int)((sender as FunkButton).CommandParameter);
				await Navigation.PopAsync();
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

	}
}
