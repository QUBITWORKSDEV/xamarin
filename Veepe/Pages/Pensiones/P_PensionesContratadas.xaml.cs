﻿using System;
using System.Threading.Tasks;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_PensionesContratadas : FunkContentPage
	{


		PensionContratadaCompartida[] pensionesCompartidas;


		public P_PensionesContratadas()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = this;
		}

		public async void actualizar()
		{
			if (isOffline)
				return;

			canEdit = false;
			WsResponse<PensionContratadaResponse> items = await WsConsumer.Consume<PensionContratadaResponse>("data", "getPensiones", "{\"idUsuario\":\"" + (Application.Current as App).usuario.idUsuario + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					if (items.datos.normales == null && items.datos.compartidas == null)
					{
						listaPensiones.IsVisible = false;
						listaPensionesCompartidas.IsVisible = false;
						listaPensiones_nodata.IsVisible = true;
					}
					else
					{
						bool noTiene = true;

						if (items.datos.normales != null)
						{
							noTiene &= items.datos.normales.Length == 0;
							listaPensiones.ItemsSource = items.datos.normales;
							listaPensiones.HeightRequest = items.datos.normales.Length * 64;
						}

						if (items.datos.compartidas != null)
						{

							pensionesCompartidas = new PensionContratadaCompartida[items.datos.compartidas.Length];

					
							for (int i = 0; i < items.datos.compartidas.Length; i++)
							{

								pensionesCompartidas[i] = new PensionContratadaCompartida();
								pensionesCompartidas[i].pensionados = items.datos.compartidas[i];
							}

							noTiene &= items.datos.compartidas.Length == 0;
							listaPensionesCompartidas.ItemsSource = pensionesCompartidas;
							listaPensionesCompartidas.HeightRequest = items.datos.compartidas.Length * 64;
						}

						listaPensiones.IsVisible = !noTiene;
						listaPensionesCompartidas.IsVisible = !noTiene;
						listaPensiones_nodata.IsVisible = noTiene;
					}
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al obtener la lista de estacionamientos.", "Aceptar");
				}
			}
			else
			{
				await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
			}

			canEdit = true;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			actualizar();
		}

		async void OnPensionSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				// mostrar la pantalla de detalles y pasarle la pensión seleccionada
				await Navigation.PushAsync(new P_DetallesPension(e.SelectedItem as PensionContratada));
				canEdit = false;
			}
		}

		async void OnPensionCompartidaSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (canEdit)
			{
				// mostrar la pantalla de detalles y pasarle la pensión seleccionada
				await Navigation.PushAsync(new P_DetallesPension(e.SelectedItem as PensionContratadaCompartida));
				canEdit = false;
			}
		}

	}
}
