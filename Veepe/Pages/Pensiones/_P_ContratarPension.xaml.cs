﻿using System;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_ContratarPension : FunkContentPage
	{
		// cada pensión tiene un periodo seleccionado, pero puede tener cualquier combinación de horarios
		Estacionamiento estacionamiento;
		public int pensionSeleccionada = -1;
		public int horarioSeleccionado = -1;	// 1=dia, 3=noche, 5=24hrs

		public float costoHorario = 0.0f;
		public float subtotal { get; set; }

		public MetodoPago metodoPagoSeleccionado = null;

		private bool tieneMetodoPago = false;

		public P_ContratarPension(Estacionamiento pEstacionamiento)
		{
			estacionamiento = pEstacionamiento;
			InitializeComponent();

			checkConnectivity();
			BindingContext = this;
			lblWarningNoConnectivity.BindingContext = this;

			lblNombrePension.Text = estacionamiento.Nombre;
			lblDireccionPension.Text = estacionamiento.Direccion;
		}

		public async void metodoDePago()
		{
			WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "getTieneMetodoPago", "{\"uid\":\"" + (Application.Current as App).usuario.idUsuario + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					tieneMetodoPago = items.datos;
					if (items.datos == true)
					{
						if ( metodoPagoSeleccionado==null )
							lblMetodoPago.Text = "Elegir método de pago";
					}
					else
					{
						lblMetodoPago.Text = "Agregar método de pago";
					}
				}
				else
				{
					await DisplayAlert("Error", "No tienes metodos de pago asociados.", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
		}


		public void refresh()
		{
			canEdit = false;

			if (pensionSeleccionada != -1)
			{
				// al tener una pensión seleccionad, automáticamente se tiene seleccionado el período
				for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
				{
					if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
					{
						lblPeriodo.Text = estacionamiento.Pensiones[i].periodoReadable;

						switch (horarioSeleccionado)
						{
							case 1:
								lblHorario.Text = "Día";
								subtotal = estacionamiento.Pensiones[i].costoDia;// * estacionamiento.Pensiones[i].periodo;
								lblSubtotal.Text = "MXN " + subtotal.ToString("F");
								break;

							case 3:
								lblHorario.Text = "Noche";
								subtotal = estacionamiento.Pensiones[i].costoNoche;// * estacionamiento.Pensiones[i].periodo;
								lblSubtotal.Text = "MXN " + subtotal.ToString("F");
								break;

							case 5:
								lblHorario.Text = "Todo el día";
								subtotal = estacionamiento.Pensiones[i].costoTodoDia;// * estacionamiento.Pensiones[i].periodo;
								lblSubtotal.Text = "MXN " + subtotal.ToString("F");
								break;

							default:
								lblHorario.Text = "Elegir horario";
								//lblSubtotal.Text = "MXN";
								break;
						}

						lblMetodoPago.Text = metodoPagoSeleccionado == null ? "Elegir método de pago" : metodoPagoSeleccionado.NombreCuenta;
					}
				}
			}



			/*if (horarioSeleccionado != null)
			{
				lblHorario.Text = horarioSeleccionado;


				switch (horarioSeleccionado)
				{
					case "Día":


						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = float.Parse(estacionamiento.Pensiones[i].dia);
							}
						}

						break;

					case "Noche":

						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = float.Parse(estacionamiento.Pensiones[i].noche);
							}
						}

						break;

					case "Todo el día":

						for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								costoHorario = float.Parse(estacionamiento.Pensiones[i].todoDia);
							}
						}

						break;

						//default:
						//	Console.WriteLine("Default case");
						//	break;
				}


				subtotal = costoHorario * periodoSeleccionadoN;
				lblSubtotal.Text = subtotal + " MXN";
			}
			else
			{
				lblHorario.Text = "Elegir horario";
				lblSubtotal.Text = "MXN";
			}


			if (metodoPagoSeleccionado != null)
			{

				lblMetodoPago.Text = metodoPagoSeleccionado.NombreCuenta;
			}
			else
			{
				lblMetodoPago.Text = "Elegir método de pago";
			}
			*/
		
			canEdit = true;
		}

		protected override void OnAppearing()
		{
			metodoDePago();

			activityIndicator.IsVisible = false;
			refresh();
		}


		async void OnConfirmarContratacionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (pensionSeleccionada != -1)
				{
					if (horarioSeleccionado != -1)
					{
						if (metodoPagoSeleccionado != null)
						{
							activityIndicator.IsVisible = true;

							await Navigation.PushAsync(new P_ConfirmarPagoPension(estacionamiento, this, pensionSeleccionada, horarioSeleccionado, subtotal));
						}
						else
							await DisplayAlert("Error", "Para poder continuar debes seleccionar un método de pago.", "Aceptar");
					}
					else
						await DisplayAlert("Error", "Para poder continuar debes seleccionar un horario.", "Aceptar");
				}
				else
					await DisplayAlert("Error", "Para poder continuar debes seleccionar un periodo.", "Aceptar");

				canEdit = true;
			}
		}

		async void OnCancelarContratacionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopAsync();
			}
		}

		async void OnElegirPeriodoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_SeleccionarPeriodo(estacionamiento, this));
			}
		}

		async void OnElegirHorarioClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (pensionSeleccionada != -1)
				{
					await Navigation.PushAsync(new P_SeleccionarHorario(estacionamiento, this, pensionSeleccionada));
				}
				else
				{
					await DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar un periodo.", "Aceptar");
					canEdit = true;
				}
			}
		}

		async void OnElegirFormaPagoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				if (tieneMetodoPago)
					await Navigation.PushAsync(new P_SeleccionarFormaPago(this));
				else
					await Navigation.PushAsync(new P_AgregarMetodoPago());
			}
		}

	}
}
