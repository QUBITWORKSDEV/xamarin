using Xamarin.Forms;
using System.Collections.Generic;
using FunkFramework.UI;

namespace Veepe
{
	public partial class P_SeleccionarHorarioCompartida : FunkContentPage
	{
		readonly Estacionamiento estacionamiento;
		P_ContratarPensionCompartida padre;

		public P_SeleccionarHorarioCompartida(Estacionamiento pEstacionamiento, P_ContratarPensionCompartida pPadre, int pensionSeleccionada, int opcion)
		{

			estacionamiento = pEstacionamiento;
			padre = pPadre;

			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			int idPeriodo = -1;

			if(estacionamiento.idEstacionamiento != 0) {

				if (estacionamiento.TienePensionCompartida)
				{
					List<Horario> horarios = new List<Horario>();

					for (int i = 0; i < estacionamiento.Pensiones.Length; i++)
					{
						if (opcion == 1)//primer estacionamiento
						{
							if (estacionamiento.Pensiones[i].idPension == pensionSeleccionada)
							{
								if (estacionamiento.Pensiones[i].costoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Día";
									horario.costo = estacionamiento.Pensiones[i].costoDiaCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoNocheCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Noche";
									horario.costo = estacionamiento.Pensiones[i].costoNocheCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoTodoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Todo el día";
									horario.costo = estacionamiento.Pensiones[i].costoTodoDiaCompartida;
									horarios.Add(horario);
								}
							}
						}
						else if (opcion == 2)//segundo estacionamiento
						{

							if (padre.periodoSeleccionadoN == estacionamiento.Pensiones[i].periodo)
							{
								idPeriodo = estacionamiento.Pensiones[i].idPension;

								if (estacionamiento.Pensiones[i].costoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Día";
									horario.costo = estacionamiento.Pensiones[i].costoDiaCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoNocheCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Noche";
									horario.costo = estacionamiento.Pensiones[i].costoNocheCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoTodoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Todo el día";
									horario.costo = estacionamiento.Pensiones[i].costoTodoDiaCompartida;
									horarios.Add(horario);
								}

								break;
							}
							//else
							//{
							//	DisplayAlert("Error", "No existen horarios para el periodo seleccionado", "Aceptar");
							//	break;

							//}
						}
						else//tercer o más estacionamientos
						{
							if (padre.periodoSeleccionadoN == estacionamiento.Pensiones[i].periodo)
							{
								idPeriodo = estacionamiento.Pensiones[i].idPension;

								if (estacionamiento.Pensiones[i].costoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Día";
									horario.costo = estacionamiento.Pensiones[i].costoDiaCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoNocheCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Noche";
									horario.costo = estacionamiento.Pensiones[i].costoNocheCompartida;
									horarios.Add(horario);
								}

								if (estacionamiento.Pensiones[i].costoTodoDiaCompartida != 0f)
								{
									Horario horario = new Horario();
									horario.horario = "Todo el día";
									horario.costo = estacionamiento.Pensiones[i].costoTodoDiaCompartida;
									horarios.Add(horario);
								}
							}
						}
					}

					foreach (Horario horario in horarios)
					{
						if (horario.ToString() != "")
						{

							var leyaut = new StackLayout();
							leyaut.HeightRequest = 48f;
							leyaut.HorizontalOptions = LayoutOptions.Fill;
							leyaut.VerticalOptions = LayoutOptions.Center;
							leyaut.BackgroundColor = Color.FromHex("#2C2A2D");
							leyaut.Orientation = StackOrientation.Horizontal;
							leyaut.Margin = new Thickness(0, 0, 0, 8);

							// creamos el nombre
							var nombre = new FunkButton();

							nombre.Text = horario.horario;

							if(horario.horario == "Día")
							{
								nombre.Text += " (7:00 - 19:00)";
							}
							else if(horario.horario == "Noche")
							{
								nombre.Text += " (19:00 - 7:00)";
							}
						 
							nombre.Text += " MXN ";
							nombre.Text += horario.costo.ToString("N");

							nombre.TextColor = Color.FromHex("#00BFD6");
							nombre.FontSize = 16;
							nombre.FontFamily = "BebasNeue";
							nombre.HorizontalOptions = LayoutOptions.FillAndExpand;
							nombre.VerticalOptions = LayoutOptions.Center;
							nombre.BackgroundColor = Color.Transparent;

							nombre.Clicked += delegate
							{
								if (canEdit)
								{
									if (opcion == 1)
									{
										padre.horarioSeleccionado = horario.horario;
									}
									else if (opcion == 2)
									{
										padre.horarioSeleccionadoSegunda = horario.horario;
										padre.pensionSeleccionadaSegunda = idPeriodo;

									}
									else
									{
										EstacionamientoSeleccionado est = new EstacionamientoSeleccionado();

										est.horarioSeleccionado = horario.horario;
										est.idPeriodo = idPeriodo;
										est.idEstacionamiento = estacionamiento.idEstacionamiento;
										est.Pensiones = estacionamiento.Pensiones;
										padre.pensionSeleccionadaComodin = idPeriodo;

										padre.estacionamientosHorarioSeleccionado.Add(est);

									}
									canEdit = false;
									Navigation.PopAsync();
								}
							};

							leyaut.Children.Add(nombre);
							ContenedorHorarios.Children.Add(leyaut);
						}
					}
				}
				else
				{
					DisplayAlert("Error", "No existen periodos configurados para este estacionamiento", "Aceptar");
				}

			}
			else
			{
				DisplayAlert("Error", "Para poder seleccionar un horario primero debes seleccionar un estacionamiento", "Aceptar");
			}

		}
	}

	public class Horario
	{
		public string horario { get; set; }
		public  float costo { get; set; }
	}
}