﻿using System;
using System.Threading.Tasks;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_Pensiones : FunkContentPage
	{
		Estacionamiento estacionamiento;
		bool tienePensionNormalContratada = false;

		public P_Pensiones(Estacionamiento pEstacionamiento)
		{
			estacionamiento = pEstacionamiento;
			InitializeComponent();

			checkConnectivity();

			bool tienePensionNormal = false;
			bool tienePensionCompartida = false;

			for (int i = 0; i < estacionamiento.Servicios.Length; i++)
			{
				if (estacionamiento.Servicios[i].idServicio == 1)
					tienePensionNormal = true;

				if (estacionamiento.Servicios[i].idServicio == 2)
					tienePensionCompartida = true;
			}

			if (tienePensionNormal == false)
				BotonPensionNormal.IsVisible = false;

			if (tienePensionCompartida == false)
				BotonPensionCompartida.IsVisible = false;
		}

		async Task<bool> PuedeContratarPensionNormal()
		{
			bool tienePensionAqui = false;

			WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeId\":\"" + (Application.Current as App).usuario.veepeId + "\"}");
			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					SessionManager.registerLogin( items.datos );
					(Application.Current as App).usuario = items.datos;
				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al actualizar la información del perfil. Por favor inténtalo más tarde.", "Aceptar");
				}
			}
			else
				await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

			Usuario user = (Application.Current as App).usuario;
			if (user._pensionesContratadas != null)
			{
				for (int i = 0; i < user._pensionesContratadas.Count; i++)
					tienePensionAqui |= user._pensionesContratadas[i].idEstacionamiento == estacionamiento.idEstacionamiento;
			}
			else
				tienePensionAqui = false;	// como no tiene pensiones, sí puede contratar

			return !tienePensionAqui;
		}

		async void OnPensionClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;

				if (await PuedeContratarPensionNormal())
				{
					await Navigation.PushAsync(new P_ContratarPension(estacionamiento));
				}
				else
				{
					await DisplayAlert("", "Ya cuentas con pensión en este estacionamiento.", "Cerrar");
					canEdit = true;
				}
			}
		}

		async void OnPensionCompartidaClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_ContratarPensionCompartida(estacionamiento));
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
			actualizar();
		}

		/*Este método sirve para checar que no tengamos una pensión normal ya contratada y en el caso d epensión compartida para checar que al menos haya más de un estacionamiento dado de alta*/
		public async void actualizar()
		{
			if (isOffline)
				return;

			BotonPensionNormal.IsVisible = false;
			BotonPensionCompartida.IsVisible = false;
			canEdit = false;
			activityIndicator.IsVisible = true;
			WsResponse<PensionContratadaResponse> items = await WsConsumer.Consume<PensionContratadaResponse>("data", "getPensiones", "{\"idUsuario\":\"" + (Application.Current as App).usuario.idUsuario + "\", \"idEstacionamiento\":\"" + estacionamiento.idEstacionamiento + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					if (items.datos.normales == null)
					{
						tienePensionNormalContratada = false;
					}
					else
					{
						if (items.datos.normales != null)
						{
							if (items.datos.normales.Length > 0)
							{
								tienePensionNormalContratada = true;
							}
						}
					}

					if (tienePensionNormalContratada == true)
					{
						BotonPensionNormal.IsEnabled = false;
					}

				}
				else
				{
					await DisplayAlert("Error", "Sucedió un error al obtener la lista de pensiones.", "Aceptar");
				}
			}
			else
			{
				await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Asegúrate de contar con conexión a Internet.", "Aceptar");
			}


			WsResponse<Estacionamiento[]> estacionamientos = await WsConsumer.Consume<Estacionamiento[]>("data", "getEstacionamientos", "{\"idEmpresa\":\"" + estacionamiento.idEmpresa + "\"}");

			if (estacionamientos.respuesta == "ok")
			{
				if (estacionamientos.datos.Length < 2)
				{
					BotonPensionCompartida.IsEnabled = false;
				}
			}

			canEdit = true;
			activityIndicator.IsVisible = false;
			BotonPensionNormal.IsVisible = true;
			BotonPensionCompartida.IsVisible = true;

		}


	}
}