﻿using Xamarin.Forms;

namespace Veepe
{
	public partial class P_NotificacionGenerica : ContentPage
	{

		public P_NotificacionGenerica (string pTitulo, string pMensaje, string pMensajeDismiss="Cerrar", string pIcono="", bool pOfrecerPassword=false)
		{
			InitializeComponent ();

			this.titulo.Text = pTitulo;
			this.cuerpo.Text = pMensaje;
			this.botonDismiss.Text = pMensajeDismiss;

			if ( pIcono!=null )
				this.icono.Source = ImageSource.FromResource(pIcono);

			// 2do: ofrecer recuperar la contraseña. esto es una pavada para no tener que hacer otra página para el mensaje de error cuando no se puede loggear
		}
	}
}

