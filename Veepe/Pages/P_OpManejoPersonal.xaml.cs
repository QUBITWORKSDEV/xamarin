﻿using System;
using System.Collections.ObjectModel;
using FunkFramework;
using FunkFramework.Net;
using Xamarin.Forms;

/*********************************
 * //TODO:
 * Mover Turnos y TempPersonalEle a sus propias clases
 * ******************************* */

namespace Veepe
{
	public class TempPersonalEle
	{
		public int intAsistencia { get; set; }

		public int idUsuario { get; set; }
		public string nombre { get; set; }
		public int turno { get; set; }
		public string faltas { get; set; }
		public string observaciones { get; set; }
		public bool tieneAsistencia {
			get {
				return intAsistencia == 1;
			}
			set {
				intAsistencia = (value == true) ? 1 : 0;
			}
		}
		public Color colorRow { get; set; }
	}


	public partial class P_OpManejoPersonal : FunkContentPage
	{
		#region _______________________________________________________________________________________________________________________________ atributos privados
		bool edited = false;
		Collection<TempPersonalEle> tempList = new Collection<TempPersonalEle>();
		#endregion


		#region _______________________________________________________________________________________________________________________________ constructor
		public P_OpManejoPersonal()
		{
			InitializeComponent();
			BindingContext = this;
			checkConnectivity();

			DateTime dt = DateTime.Now;
			txtFecha.Text = dt.ToString("D");
		}
		#endregion


		#region _______________________________________________________________________________________________________________________________ métodos
		/// <summary>
		/// Obtiene la info de los operadores del estacionamiento actual.</summary>
		async void actualizar()
		{
			if (isOffline)
				return;
					
			if (canEdit)
			{
				disableControls();
				WsResponse<TempPersonalEle[]> items = await WsConsumer.Consume<TempPersonalEle[]>("umanagement", "getOperadores", "{\"idEstacionamiento\":\"" + (Application.Current as App).usuario.idEstacionamiento + "\"}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos != null)
						{
							if (items.datos.Length == 0)
							{
								laLista.IsVisible = false;
								laLista_nodata.IsVisible = true;
							}
							else
							{
								tempList = new Collection<TempPersonalEle>();
								laLista_nodata.IsVisible = false;
								laLista.IsVisible = true;

								for (int i = 0; i<items.datos.Length; i++)
								{
									items.datos[i].colorRow = i % 2 == 0 ? Color.White : Color.FromHex("#F2F2F2");
									tempList.Add(items.datos[i]);
								}
		
								laLista.ItemsSource = tempList;
							}

							edited = false;
						}
					}
					else
					{
						await DisplayAlert("Error", "Sucedió un errro al obtener la lista de operadores. Por favor inténtalo más tarde.", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
			
				enableControls();
			}
		}

		/// <summary>
		/// Habilita todos los controles.</summary>
		void enableControls()
		{
			// habilitar todos los controles dentro de la lista
			canEdit = true;
			activityIndicator.IsVisible = false;
		}

		/// <summary>
		/// Deshabilita todos los controles.</summary>
		void disableControls()
		{
			// deshabilitar todos los controles dentro de la lista
			canEdit = false;
			activityIndicator.IsVisible = true;
		}
		#endregion


		#region _______________________________________________________________________________________________________________________________ listeners
		protected override void OnAppearing()
		{
			base.OnAppearing();
			actualizar();
			canEdit = true;
		}

		protected override void OnDisappearing()
		{
			if (edited)
			{
				OnGuardarClicked(this, new EventArgs());
			}

			base.OnDisappearing();
		}

		/// <summary>
		/// Atiende del evento Refreshing de la lista.</summary>
		void Handle_Refreshing(object sender, EventArgs e)
		{
			actualizar();
			laLista.IsRefreshing = false;
		}

		/// <summary>
		/// Atiende el evento toggle de los swithces de cada fila.</summary>
		void OnSwitchToggled(object sender, EventArgs e)
		{
			edited = true;
		}

		/// <summary>
		/// Atiende el evento Change del selector de utrno.</summary>
		void OnPickerChanged(object sender, EventArgs e)
		{
			edited = true;
		}

		/// <summary>
		/// Atiende el evento Changed del entry (razones).</summary>
		void OnEntryChanged(object sender, EventArgs e)
		{
			edited = true;
		}

		/// <summary>
		/// Atiende el evento Click del botón guardar -> guardamos la info de aistencias al servidor.</summary>
		async void OnGuardarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				var data = Newtonsoft.Json.JsonConvert.SerializeObject(tempList);

				// enviamos
				disableControls();

				WsResponse<bool> items = await WsConsumer.Consume<bool>("operations", "setAsistencias", "{\"idEstacionamiento\":\"" + (Application.Current as App).usuario.idEstacionamiento + "\", \"datos\":" + data + "}");

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos == true)
						{
							edited = false;
						}
						else
						{
							await DisplayAlert("", "Sucedió un error al registrar las asistencias. Por favor inténtalo nuevamente.", "Cerrar");
						}
					}
					else
					{
						await DisplayAlert("", "Sucedió un error al registrar las asistencias. Por favor inténtalo nuevamente.", "Cerrar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
			
				enableControls();
			}

		}
		#endregion
	} // fin de la clase

}
