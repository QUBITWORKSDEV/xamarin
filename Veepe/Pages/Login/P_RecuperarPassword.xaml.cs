﻿using System;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_RecuperarPassword : FunkContentPage
	{
		public P_RecuperarPassword ()
		{
			InitializeComponent ();
			checkConnectivity();
			BindingContext = this;
		}

		async void OnEnviarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				if (validarFormulario())
				{
					activityIndicator.IsVisible = true;

					WsResponse<int> items = await WsConsumer.Consume<int>("umanagement", "recover", "{\"email\":\"" + txtEmail.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							switch (items.datos)
							{
								case 1:
									// alles gut!
									await DisplayAlert("", "Enviamos un correo a tu dirección de correo con instrucciones para recuperar tu contraseña.", "Aceptar");
									await Navigation.PopAsync();
									break;

								case 0:
									await DisplayAlert("", "El correo proporcionado no existe en Veepe.", "Aceptar");
									break;

								case 3:
									await DisplayAlert("", "Sucedió un error al intentar recuperar la contraseña. Por favor inténtalo más tarde.", "Aceptar");
									break;
							}
						}
						else
						{
							// error de código
							await DisplayAlert("", "Sucedió un error al intentar recuperar la contraseña. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
					{
						// error de red
						await DisplayAlert("Error de red", "No se pudo conectar con el servidor. Por favor asegúrate de estar conectado a la red", "Aceptar");
					}
				}
				else
				{
					await DisplayAlert("", "Para continuar proporciona un email válido.", "Cerrar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin de OnEnviarClicked

		bool validarFormulario()
		{
			return txtEmail.IsValid;
		}
	} // fin de la clase P_RecuperarPassword
}

