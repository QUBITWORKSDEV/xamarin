﻿using System;

namespace Veepe
{
	public partial class P_Terminos : FunkContentPage
	{
		public P_Terminos ()
		{
			InitializeComponent ();
			checkConnectivity();
		}

		async void OnCloseClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopModalAsync();
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}
	}
}

