﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using FunkFramework;
using PushNotification.Plugin;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_InicioSesion : FunkContentPage
	{
		public P_InicioSesion()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = this;
		}

		async void OnLoginClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (validarFormulario())
				{
					WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "login", "{\"u\":\"" + txtEmail.Text + "\",\"c\":\"" + txtPassword.Text + "\",\"token\":\"" + CrossPushNotification.Current.Token + "\",\"platform\":\"" + (Device.OS == TargetPlatform.Android ? "android" : "ios") + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							Usuario aux = items.datos as Usuario;
							SessionManager.registerLogin(aux);
							(Application.Current as App).usuario = aux;

							Page next;
							if (aux.rol == Usuario.ROL_USUARIO_FINAL)
							{
								next = await (Application.Current as App).getHomeUsuario(); // new Veepe.P_Home( detailPage );
							}
							else
								next = new P_OpHome();

							await Task.Factory.StartNew(() =>
							{
								Device.BeginInvokeOnMainThread(() =>
								{
									Application.Current.MainPage = next;
								});
							});
						}
						else
						{
							if (items.mensaje == "desactivado")
							{
								await DisplayAlert("", "Tu cuenta aún no ha sido activada. Por favor haz clic en la liga que se envió a tu correo cuando te registraste para activar tu cuenta.", "Aceptar");
							}
							else if (items.mensaje == "noexiste")
							{
								await DisplayAlert("", "Tu dirección de correo no está registrada. Por favor verifícala o crea una nueva cuenta.", "Aceptar");
							}
							else
							{
								await DisplayAlert("Error", "Los datos son incorrectos. Por favor verifícalos.", "Aceptar");
							}
							canEdit = true;
							activityIndicator.IsVisible = false;
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					await DisplayAlert("Inicio de sesión", "Los datos proporcionados no son correctos.", "Cerrar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		}

		bool validarFormulario()
		{
			return (txtEmail.IsValid && txtPassword.IsValid);
		}

		async void OnRecoverPasswordClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_RecuperarPassword());
			}
		}

		void ExecFormStep(object sender, EventArgs e)
		{
			// checamos si ya está todo completo
			if (string.IsNullOrWhiteSpace(txtEmail.Text))
				txtEmail.Focus();
			else if (string.IsNullOrWhiteSpace(txtPassword.Text))
				txtPassword.Focus();
			else
				OnLoginClicked(sender, e);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}
	}
}

