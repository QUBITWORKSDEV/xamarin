﻿using System;
using System.Threading.Tasks;
using FunkFramework.FB;
using FunkFramework.Net;
using PushNotification.Plugin;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_LoginInicio : FunkContentPage
	{
		public P_LoginInicio ()
		{
			InitializeComponent();
			checkConnectivity();
			BindingContext = this;

			cmdLoginFacebook.OnLoginIntent += CmdLoginFacebook_OnLoginIntent;
			cmdLoginFacebook.OnCustomClick += CmdLoginFacebook_OnCustomClick;
			cmdLoginFacebook.OnLoginCancel += CmdLoginFacebook_OnLoginCancel;
			cmdLoginFacebook.OnLoginError += CmdLoginFacebook_OnLoginError;

			canEdit = true;
		}

		void CmdLoginFacebook_OnLoginIntent(object sender, string fbId, string fbName, string fbLastName, string fbEmail)
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				await processIntent(sender, fbId, fbName, fbLastName, fbEmail);
			});
		} // fin de CmdLoginFacebook_OnLoginIntent

		void CmdLoginFacebook_OnCustomClick()
		{
			canEdit = false;
		}

		void CmdLoginFacebook_OnLoginError(object sender, EventArgs e)
		{
			DisplayAlert("", "Lo sentimos, sucedió un error al iniciar sesión con Facebook. Por favor inténtalo más tarde.", "Aceptar");
			canEdit = true;
		}

		void CmdLoginFacebook_OnLoginCancel(object sender, EventArgs e)
		{
			DisplayAlert("", "Para iniciar sesión con Veepe debes iniciar sesión en Facebook y dar los permisos requeridos.", "Aceptar");
			canEdit = true;
		}


		async Task processIntent(object sender, string fbId, string fbName, string fbLastName, string fbEmail)
		{
			canEdit = false;
			if (string.IsNullOrEmpty(fbEmail) || string.IsNullOrEmpty(fbName))
			{
				// no se puede registrar porque no se tienen los permisos
				Device.BeginInvokeOnMainThread(() =>
				{
					DisplayAlert("", "Lo sentimos, por falta de permisos no pudimos obtener tu email de Facebook. Por favor regístrate manualmente o permite el acceso de Veepe a tu información de Facebook.", "Aceptar");
				});
			}
			else
			{
				WsResponse<FBRegisterResponse> items = await WsConsumer.Consume<FBRegisterResponse>("umanagement", "registerfb", "{\"u\":\"" + fbEmail + "\", \"c\":\"" + fbId + "\", \"nombre\":\"" + fbName + "\", \"apellido\":\"" + fbLastName + "\",\"materno\":\"\",\"token\":\"" + 
																									CrossPushNotification.Current.Token + "\",\"platform\":\"" + (Device.RuntimePlatform == Device.Android ? "android" : "ios") + "\"}");		// usamos el id de fb como contraseña interna

				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						switch (items.datos.respuesta)
						{
							case 1:
							case 0:
								if ( items.datos.respuesta == 1 )
									await DisplayAlert("", "Gracias por registrarte a Veepe. Pronto recibirás un email con tu nip temporal.", "Aceptar");
								Usuario aux = items.datos.usuario as Usuario;
								SessionManager.registerLogin(aux);
								(Application.Current as App).usuario = aux;

								Page next;
								next = await (Application.Current as App).getHomeUsuario();

								Device.BeginInvokeOnMainThread(() =>
								{
									Application.Current.MainPage = next;
								});
								break;

							/*case 0:
								await DisplayAlert("", "El email proporcionado ya está registrado. Si olvidaste tu contraseña haz clic en la opción \"¿Olvidaste tu contraseña?.\" en el inicio de sesión.", "Aceptar");
								break;*/

							case 3:
							default:
								// intentar deslogear a fb
								var cfb = DependencyService.Get<CrossFBManager>();
								cfb.LogOut();

								await DisplayAlert("", items.mensaje, "Aceptar");
								break;
						}
					}
					else
					{
						await DisplayAlert("Error", "Lo sentimos; sucedió un error inesperado al registrar tu cuenta. Inténtalo nuevamente más tarde.", "Aceptar");
						Reporter.ReportError("Error al registrar con fb. El ws respondió: respuesta:" + items.respuesta + "; mensaje:" + items.mensaje + "; " + items.datos);
					}
				}
				else
					Reporter.ReportError("Error al registrar con fb. El ws respondió: items==null");

				canEdit = true;
			}
			canEdit = true;
		}

		async Task traerInfo(string email, string pass)
		{
			WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "login", "{\"u\":\"" + email + "\",\"c\":\"" + pass + "\"}");

			if (items != null)
			{
				if (items.respuesta == "ok")
				{
					Usuario aux = items.datos as Usuario;
					SessionManager.registerLogin(aux);
					(Application.Current as App).usuario = aux;

					//await Task.Factory.StartNew(() =>
					//{
						Device.BeginInvokeOnMainThread(() =>
						{
							(App.Current as App).MainPage = new P_Home(new P_HomeDetail());
						});
					//});
				}
				else
				{
					await DisplayAlert("Error", "Los datos son incorrectos. Por favor verifícalos.", "Aceptar");
				}
			}
		}

		void OnLoginClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				Navigation.PushAsync(new P_InicioSesion());
			}
		}

		void OnRegisterClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				Navigation.PushAsync(new P_Registro());
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			(Application.Current.MainPage as NavigationPage).BarBackgroundColor = Color.FromHex("#090A19");
			//(Application.Current.MainPage as NavigationPage).BackgroundColor = Color.FromHex("#090A19");
			canEdit = true;
		}
	}

}

