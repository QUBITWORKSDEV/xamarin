﻿using System;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_Registro : FunkContentPage
	{
		public P_Registro ()
		{
			InitializeComponent ();
			checkConnectivity();
			BindingContext = this;
		}

		async void OnRegisterClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				if (validarFormulario())
				{
					// bloquear la UI
					activityIndicator.IsVisible = true;

					WsResponse<int> items = await WsConsumer.Consume<int>("umanagement", "register", "{\"u\":\"" + txtUsername.Text + "\",\"p\":\"" + txtPassword.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							switch (items.datos)
							{
								case 1:
									await DisplayAlert("", "Gracias por registrarte a Veepe. Te acabamos de enviar un correo con instrucciones para que completes tu registro.", "Aceptar");
									// vamos de regreso al home
									await Navigation.PopToRootAsync();
									break;

								case 0:
									await DisplayAlert("", "El email proporcionado ya está registrado. Si olvidaste tu contraseña usa la liga que aparece más abajo.", "Aceptar");
									break;

								case 3:
								default:
									await DisplayAlert("", "Sucedió un error al registrarte. Por favor inténtalo más tarde.", "Aceptar");
									break;
							}
						}
						else
						{
							await DisplayAlert("", "Sucedió un error al registrarte. Por favor inténtalo más tarde.", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					await DisplayAlert("", "Llena todos los campos y verifica que las contraseñas coincidan.", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin de OnRegisterClicked

		bool validarFormulario()
		{
			return (txtUsername.IsValid && txtPassword.IsValid && txtPassword.Text==txtPassword2.Text);
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			canEdit = true;
		}

		async void OnShowTerminosClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushModalAsync(new P_Terminos());
			}
		}
		
	}
}

