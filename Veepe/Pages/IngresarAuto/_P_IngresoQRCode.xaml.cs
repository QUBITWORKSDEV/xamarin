﻿using System;
using FunkFramework.Net;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_IngresoQRCode : FunkContentPage
	{
		public P_IngresoQRCode()
		{
			InitializeComponent();
			checkConnectivity();

			QRReader.OnScanResult += (result) => Device.BeginInvokeOnMainThread( () => {
				QRReader.IsScanning = false;
				txtVeepeId.Text = result.Text;
			});
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			QRReader.IsScanning = true;
			canEdit = true;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			QRReader.IsScanning = false;
		}

		async void OnAceptarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (!String.IsNullOrEmpty(txtVeepeId.Text))
				{
					WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeId\":\"" + txtVeepeId.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							// checamos si ya está ingresado en otro estacionamiento
							if (items.datos.tieneOperacionActiva)
							{
								await DisplayAlert("", "Este usuario ya tiene una operación activa.", "Cerrar");
							}
							else if (items.datos.tienePagosAtrasados)
							{
								await DisplayAlert("", "Este usuario tiene pagos atrasados. No se puede ingresar", "Cerrar");
							}
							else
								await Navigation.PushAsync(new P_SeleccionarAuto(items.datos));
						}
						else
						{
							await DisplayAlert("Error", items.mensaje, "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					await DisplayAlert("", "El VeepeId debe ser válido", "Aceptar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin de OnAceptarClicked

		void OnVeepeIdCompleted(object sender, EventArgs e)
		{
			OnAceptarClicked(sender, e);
		}

	}
}

