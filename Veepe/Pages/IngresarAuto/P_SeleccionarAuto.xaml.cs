﻿using System;
using FunkFramework;
using FunkFramework.Net;
using FunkFramework.UI;
using Xamarin.Forms;

namespace Veepe
{
	public partial class P_SeleccionarAuto : FunkContentPage
	{
		// ______________________________________________________________________ Atributos
		public Usuario usuario;
		public int idOperacion=-1;

		// ______________________________________________________________________ Constructor
		public P_SeleccionarAuto() { }

		public P_SeleccionarAuto(int pIdOp)
		{
			idOperacion = pIdOp;
			InitializeComponent();
			checkConnectivity();
			//actualizar(false);
		}

		public P_SeleccionarAuto(Usuario pUsuario)
		{
			usuario = pUsuario;

			InitializeComponent();
			checkConnectivity();
			Config();
		}

		// ______________________________________________________________________ Métodos
		void Config()
		{
			listaAutos_nodata.IsVisible = usuario._autos.Count == 0;
			listaAutos.ItemsSource = usuario._autos;

			txtNombre.BindingContext = usuario;
			txtVeepeId.BindingContext = usuario;

			listaAutos.HeightRequest = (usuario._autos.Count + 1) * 48;

			// letrero de pensiones
			if (usuario.getPensionesActivas((Application.Current as App).usuario.idEstacionamiento).Length > 0 || usuario.getPensionesCompartidasActivas((Application.Current as App).usuario.idEstacionamiento).Length > 0 )
			{
				string str = "";

				for (int i = 0; i < usuario._pensionesContratadas.Count; i++)
				{
					if (i > 0) str += "\n";
					str += "Pensión de " + usuario._pensionesContratadas[i].TipoReadable + "";
				}

				for (int i = 0; i < usuario._pensionesCompartidasContratadas.Count; i++)
				{
					for (int j = 0; j < usuario._pensionesCompartidasContratadas[i].Length; j++)
					{
						if (usuario._pensionesCompartidasContratadas[i][j].idEstacionamiento == (Application.Current as App).usuario.idEstacionamiento && usuario._pensionesCompartidasContratadas[i][j].estaActiva())
						{
							if (j > 0) str += "\n";
							str += "Pensión de " + usuario._pensionesCompartidasContratadas[i][j].TipoReadable + " compartida";
						}
					}
				}

				stackPensiones.IsVisible = true;
				lblPensiones.Text = str;
			}
			else
			{
				stackPensiones.IsVisible = false;
				lblPensiones.Text = "";
			}

		}

		public async void actualizar()
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;

				if (idOperacion==-1)
				{
					// Usamos este bloque cuando se está ingresando un auto nuevo, es decir, no se tiene una operación ya creada
					WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuario", "{\"veepeid\":\"" + usuario.veepeId + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							usuario = items.datos;
							Config();
						}
						else
						{
							await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					// Usamos este bloque cuando hay reserva y ya se tiene un operación creada
					WsResponse<Usuario> items = await WsConsumer.Consume<Usuario>("umanagement", "getUsuarioByIdOperacion", "{\"idoperacion\":\"" + idOperacion + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							usuario = items.datos;
							Config();
						}
						else
						{
							await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
			
				canEdit = true;
			}
		} // fin de actualizar()

		// ______________________________________________________________________ Listeners
		void OnElegirClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				string placa = (sender as FunkButton).CommandParameter.ToString();

				// buscamos el auto con esta placa
				bool encontrado = false;
				foreach (Auto a in usuario._autos)
				{
					if (a.Placa == placa && !encontrado)
					{
						encontrado = true;
						Navigation.PushAsync(new P_IngresarAuto(usuario, a));
						return;
					}
				}
				canEdit = true;
			}
		} // fin de OnElegirClicked

		async void OnAltaClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PushAsync(new P_OpAltaAuto(usuario));
			}
		}

		async void OnCancelarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await Navigation.PopAsync();
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			canEdit = true;
			actualizar();
		}

		void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e == null) return; // has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null; // de-select the row
		}

		void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (e == null) return; // has been set to null, do not 'process' tapped event
			((ListView)sender).SelectedItem = null; // de-select the row
		}
	}
}
