﻿using System;
using FunkFramework;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_OpAltaAuto : FunkContentPage
	{
		Usuario _usuario;

		public P_OpAltaAuto(Usuario usuario)
		{
			_usuario = usuario;
			InitializeComponent();
			checkConnectivity();

			txtNombre.BindingContext = _usuario;
			txtVeepeId.BindingContext = _usuario;
		}

		async void OnAltaClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				canEdit = false;
				activityIndicator.IsVisible = true;

				if (validarFormulario())
				{
					string marca = string.IsNullOrWhiteSpace(txtMarca.Text) ? "-" : txtMarca.Text;
					string modelo = string.IsNullOrWhiteSpace(txtModelo.Text) ? "-" : txtModelo.Text;

					WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "altaAuto", "{\"veepeId\":\"" + _usuario.veepeId + "\", \"marca\":\"" + marca + "\", \"submarca\":\"" + txtSubmarca.Text + "\", \"modelo\":\"" + modelo + "\", \"placa\":\"" + txtPlaca.Text + "\", \"color\":\"" + txtColor.Text + "\"}");

					if (items != null)
					{
						if (items.respuesta == "ok")
						{
							if (items.datos == true)
							{
								await Navigation.PopAsync();
							}
							else
							{
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
							}
						}
						else
						{
							await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						}
					}
					else
						await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");
				}
				else
				{
					// error de validación!
					await DisplayAlert("", "Debes proporcionar todos los campos", "Cerrar");
				}

				canEdit = true;
				activityIndicator.IsVisible = false;
			}
		} // fin de OnAltaClicked

		bool validarFormulario()
		{
			return txtSubmarca.IsValid && txtPlaca.IsValid && txtColor.IsValid;
		}

	}
}