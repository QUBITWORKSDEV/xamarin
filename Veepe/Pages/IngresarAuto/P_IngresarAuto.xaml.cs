﻿using FunkFramework.UI;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Net.Http;
using FunkFramework;
using FunkFramework.Json;
using FunkFramework.Net;

namespace Veepe
{
	public partial class P_IngresarAuto : FunkContentPage
	{
		// _____________________________________________________________________ atributos privados
		Auto _auto;
		Usuario _usuario;
		string[] pathFotos = new string[4];

		// _____________________________________________________________________ métodos
		public P_IngresarAuto(Usuario usuario, Auto auto)
		{
			_auto = auto;
			_usuario = usuario;

			InitializeComponent();
			BindingContext = _auto;
			lblWarningNoConnectivity.BindingContext = this;
		}

		async void OnCapturarClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				string tipo = (sender as Button).CommandParameter.ToString();
				await TakePicture(tipo);
				canEdit = true;
			}
		}

		async Task TakePicture(string tipo)
		{
			if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
			{
				await DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
			}
			else
			{
				var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
				{
					Directory = "VeepeOps",
					Name = tipo+".jpg"
				});

				if (file != null)
				{
					// ponemos la foto en la imagen correspondiente
					switch (tipo)
					{
						case "izquierda":
							pathFotos[0] = file.Path;
							fotoIzquierda.Source = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
							break;

						case "frente":
							pathFotos[1] = file.Path;
							fotoFrente.Source = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
							break;
						
						case "derecha":
							pathFotos[2] = file.Path;
							fotoDerecha.Source = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
							break;

						case "trasera":
							pathFotos[3] = file.Path;
							fotoTrasera.Source = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
							break;
					}
				}
				else
				{
					await DisplayAlert("Error", "No se pudo tomar la foto. Inténtalo de nuevo", "Cerrar");
				}
			}

		} // fin de TakePicture

		void OnObjetoToggle(object sender, EventArgs e)
		{
			if ((sender as FunkButton).BackgroundColor == Color.FromHex("#00BFD6")) // azul
			{
				// esta habilitado -> deshabilitar
				(sender as FunkButton).BackgroundColor = Color.FromHex("#2C2A2D");
			}
			else
			{
				// habilitar
				(sender as FunkButton).BackgroundColor = Color.FromHex("#00BFD6");
			}
		}

		async void OnEnviarClicked(object sender, EventArgs e)
		{
			if (isOffline)
				return;

			if (canEdit)
			{
				Activity.IsVisible = true;
				canEdit = false;
				activityIndicator.IsVisible = true;

				// sólo validamos si no hay pensiones que puedan aplicarse al horario actual
				int a = _usuario.getPensionesActivas((Application.Current as App).usuario.idEstacionamiento).Length;
				int b = _usuario.getPensionesCompartidasActivas((Application.Current as App).usuario.idEstacionamiento).Length;
				System.Diagnostics.Debug.WriteLine(a + ", " + b);
				if (a <= 0 && b <= 0)
				{
					if (pathFotos[0] == null || pathFotos[1] == null || pathFotos[2] == null || pathFotos[3] == null )
					{
						await DisplayAlert("", "Debes proporcionar todas las fotografías", "Aceptar");
						Activity.IsVisible = false;
						canEdit = true;
						activityIndicator.IsVisible = false;
						return;
					}
				}

				// registramos la operación
				var todoBien = false;
				var idOperacion = -1;
				byte[] byteData;
				ByteArrayContent bac;
				var form = new MultipartFormDataContent();

				// campos generales
				form.Add(new StringContent("" + SessionManager.getIdEstacionamiento()), "idEstacionamiento");
				form.Add(new StringContent("" + _usuario.veepeId), "veepeId");
				form.Add(new StringContent("" + _auto.idAuto), "idAuto");
				form.Add(new StringContent("" + SessionManager.getIdUsuario()), "recibio");
					// declaraciones
				form.Add(new StringContent("" + (toggleLentes.BackgroundColor == Color.FromHex("#00BFD6"))), "dejaLentes");
				form.Add(new StringContent("" + (toggleLaptop.BackgroundColor == Color.FromHex("#00BFD6"))), "dejaLaptop");
				form.Add(new StringContent("" + (toggleTablet.BackgroundColor == Color.FromHex("#00BFD6"))), "dejaTablet");
				form.Add(new StringContent("" + (toggleCelular.BackgroundColor == Color.FromHex("#00BFD6"))), "dejaCelular");
				form.Add(new StringContent(txtOtrosObjetos.Text ?? ""), "otrosObjetos");
				form.Add(new StringContent(txtComentarios.Text ?? ""), "comentarios");

				// _________________________________________ alta de operación
				string response = await WsConsumer.Post("operations", "altaOperacion", form);
				WsResponse<int> items = Jsoner.deserialize<WsResponse<int>>(response);
				if (items != null)
				{
					if (items.respuesta == "ok")
					{
						if (items.datos != -1)
						{
							todoBien = true;
							idOperacion = items.datos;
						}
						else
						{
							await DisplayAlert("Error", items.mensaje, "Aceptar");
						}
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
				{
					Reporter.ReportError("Error al registrar operación. El ws respondió: items==null");
					await DisplayAlert("Error", "Sucedió un error al registrar la operación. Por favor verifica que se haya dado de alta en la tabla de operaciones.", "Aceptar");
				}

				if (todoBien)
				{
					WsResponse<bool> items2;

					// ************ foto1 ************
					if (!string.IsNullOrWhiteSpace(pathFotos[0]))
					{
						await progressBar.ProgressTo(0.2f, 250, Easing.Linear); // Progress = 0.2f;

						// _________________________________________ alta de foto 1
						form = new MultipartFormDataContent();
						form.Add(new StringContent("" + idOperacion), "idOperacion");
						form.Add(new StringContent("" + _usuario.veepeId), "veepeId");
						form.Add(new StringContent("izquierda"), "lado");
						byteData = DependencyService.Get<ISaveAndLoad>().LoadBinary(pathFotos[0]);
						byteData = DependencyService.Get<IImageResizer>().ResizeImage(byteData, 1296f, 972f);
						bac = new ByteArrayContent(byteData);
						form.Add(new ByteArrayContent(byteData), "fotoIzquierda", "fotoIzquierda.jpg");

						lblProgreso.Text = "Subiendo fotografía izquierda...";
						response = await WsConsumer.Post("operations", "altaFotoOperacion", form);
						items2 = Jsoner.deserialize<WsResponse<bool>>(response);
						if (items2 != null)
						{
							if (items2.respuesta == "ok" && items2.datos == true)
							{
								await progressBar.ProgressTo(0.4f, 250, Easing.Linear);
							}
							else
							{
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
							}
						}
					}

					// ************ foto2 ************
					if (!string.IsNullOrWhiteSpace(pathFotos[1]))
					{
						// _________________________________________ alta de foto 2
						form = new MultipartFormDataContent();
						form.Add(new StringContent("" + idOperacion), "idOperacion");
						form.Add(new StringContent("" + _usuario.veepeId), "veepeId");
						form.Add(new StringContent("delantera"), "lado");
						byteData = DependencyService.Get<ISaveAndLoad>().LoadBinary(pathFotos[1]);
						byteData = DependencyService.Get<IImageResizer>().ResizeImage(byteData, 1296f, 972f);
						bac = new ByteArrayContent(byteData);
						form.Add(new ByteArrayContent(byteData), "fotoDelantera", "fotoDelantera.jpg");

						lblProgreso.Text = "Subiendo fotografía delantera...";
						response = await WsConsumer.Post("operations", "altaFotoOperacion", form);
						items2 = Jsoner.deserialize<WsResponse<bool>>(response);
						if (items2 != null)
						{
							if (items2.respuesta == "ok" && items2.datos == true)
							{
								await progressBar.ProgressTo(0.6f, 250, Easing.Linear);
							}
							else
							{
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
							}
						}
					}

					// ************ foto3 ************
					if (!string.IsNullOrWhiteSpace(pathFotos[2]))
					{
						// _________________________________________ alta de foto 3
						form = new MultipartFormDataContent();
						form.Add(new StringContent("" + idOperacion), "idOperacion");
						form.Add(new StringContent("" + _usuario.veepeId), "veepeId");
						form.Add(new StringContent("derecha"), "lado");
						byteData = DependencyService.Get<ISaveAndLoad>().LoadBinary(pathFotos[2]);
						byteData = DependencyService.Get<IImageResizer>().ResizeImage(byteData, 1296f, 972f);
						bac = new ByteArrayContent(byteData);
						form.Add(new ByteArrayContent(byteData), "fotoDerecha", "fotoDerecha.jpg");

						lblProgreso.Text = "Subiendo fotografía derecha...";
						response = await WsConsumer.Post("operations", "altaFotoOperacion", form);
						items2 = Jsoner.deserialize<WsResponse<bool>>(response);
						if (items2 != null)
						{
							if (items2.respuesta == "ok" && items2.datos == true)
							{
								await progressBar.ProgressTo(0.8f, 250, Easing.Linear);
							}
							else
							{
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
							}
						}
					}

					// ************ foto4 ************
					if (!string.IsNullOrWhiteSpace(pathFotos[3]))
					{
						// _________________________________________ alta de foto 4
						form = new MultipartFormDataContent();
						form.Add(new StringContent("" + idOperacion), "idOperacion");
						form.Add(new StringContent("" + _usuario.veepeId), "veepeId");
						form.Add(new StringContent("trasera"), "lado");
						form.Add(new StringContent("true"), "esFinal");
						byteData = DependencyService.Get<ISaveAndLoad>().LoadBinary(pathFotos[3]);
						byteData = DependencyService.Get<IImageResizer>().ResizeImage(byteData, 1296f, 972f);
						bac = new ByteArrayContent(byteData);
						form.Add(new ByteArrayContent(byteData), "fotoTrasera", "fotoTrasera.jpg");

						lblProgreso.Text = "Subiendo fotografía trasera...";
						response = await WsConsumer.Post("operations", "altaFotoOperacion", form);
						items2 = Jsoner.deserialize<WsResponse<bool>>(response);
						if (items2 != null)
						{
							if (items2.respuesta == "ok" && items2.datos == true)
							{
								await progressBar.ProgressTo(1f, 250, Easing.Linear);
							}
							else
							{
								await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
							}
						}
					}
				}

				// ahora cerramos la operación
				WsResponse<bool> items3 = await WsConsumer.Consume<bool>("operations", "completarAlta", "{\"idOperacion\":\"" + idOperacion + "\"}");
				if (items != null)
				{
					if (items3.respuesta == "ok")
					{
						if (items3.datos == true)
						{
							progressBar.Progress = 1.0f;
							await DisplayAlert("", "La operación fue registrada.", "Regresar al inicio");
							await Navigation.PopToRootAsync();
						}
						else
						{
							await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
						}
					}
					else
					{
						await DisplayAlert("Error", "EL SERVIDOR RESPONDIO, PERO CON UN ERROR", "Aceptar");
					}
				}
				else
					await DisplayAlert("Error", "Sucedió un error al contactar el servidor. Por favor inténtalo más tarde.", "Aceptar");

				Activity.IsVisible = false;
				canEdit = true;
				activityIndicator.IsVisible = false;
			
			}

		} // fin del método onEnviarClicked

	}
}

