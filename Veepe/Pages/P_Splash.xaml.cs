﻿using Xamarin.Forms;
using System.Threading.Tasks;

namespace Veepe
{
	public partial class P_Splash : ContentPage
	{
		public P_Splash ()
		{
			InitializeComponent ();

			var t = initi();
			t.ContinueWith(task =>
			{
				// caemos aquí si la tarea t falló
				// Error code 23
				DisplayAlert("", "Lo sentimos, sucedió un error que impide que se ejecute la aplicación. Err 23", "Cerrar");
			}, TaskContinuationOptions.OnlyOnFaulted);
		}

		async Task initi()
		{
			await Task.Delay(100);
			await (Application.Current as App).StartupSequence();
		}

	} // fin de la clase P_Splash
}

