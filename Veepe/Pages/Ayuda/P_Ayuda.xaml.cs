﻿using FunkFramework;
using FunkFramework.UI;

namespace Veepe
{
	public partial class P_Ayuda : FunkContentPage
	{
		public P_Ayuda ()
		{
			InitializeComponent ();
			checkConnectivity();

			txtVersion.Text = "v. " + GlobalSettings.currentVersion;
		}
	}
}

