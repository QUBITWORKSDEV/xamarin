﻿using System;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace Veepe
{
	public partial class P_VeepeId : FunkContentPage
	{
		public P_VeepeId ()
		{
			InitializeComponent ();
			checkConnectivity();

			BindingContext = (Application.Current as App).usuario;
			lblWarningNoConnectivity.BindingContext = this;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			var barcode = new ZXingBarcodeImageView
			{
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center
			};

			barcode.WidthRequest = 262;
			barcode.HeightRequest = 262;
			barcode.BarcodeFormat = ZXing.BarcodeFormat.QR_CODE;
			barcode.BarcodeOptions.Height = 262;
			barcode.BarcodeOptions.Width = 262;
			barcode.BarcodeValue = (Application.Current as App).usuario.veepeId;

			stack1.Children.Insert(0, barcode);
		}

		async void OnListoClicked(object sender, EventArgs e)
		{
			if (canEdit)
			{
				canEdit = false;
				await (Application.Current as App).updateHomePage();
				//((Application.Current as App).MainPage as MasterDetailPage).Detail = new NavigationPage(new P_HomeDetail());
			}
		}

	}
}

