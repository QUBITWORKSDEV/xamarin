﻿using System;
using System.ComponentModel;

namespace Veepe
{
	public class Auto : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		// ___________________________________________________ Atributos privados

		// ___________________________________________________ Atributos (y accesores)
		public int idAuto { get; set; }
		public string Marca { get; set; }
		public string Submarca { get; set; }
		public string Modelo { get; set; }
		public string Placa { get; set; }
		public string Color { get; set; }

		public string NombreCompuesto
		{
			get { return Marca + " " + Submarca + " " + Color; }
		}

		// ___________________________________________________ Constructores
		public Auto() { }

		public Auto(string pMarca, string pSubmarca, string pModelo, string pPlaca, string pColor)
		{
			Marca = pMarca;
			Submarca = pSubmarca;
			Modelo = pModelo;
			Placa = pPlaca;
			Color = pColor;
		}

		// ___________________________________________________ Métodos
	}
}