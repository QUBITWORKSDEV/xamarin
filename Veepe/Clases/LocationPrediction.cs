﻿using System.Collections.Generic;

namespace Veepe
{
	public class LocationPrediction
	{
		public string description { get; set; }
		public string id { get; set; }
		public string place_id { get; set; }
		public string reference { get; set; }
	}

	public class LocationPredictionResult
	{
		public List<LocationPrediction> predictions { get; set; }
		public string status { get; set; }
	}

	// ****************************************************************

	public class LocationDetail
	{
		public string status { get; set; }
		public LocationDetailResult result { get; set; }
	}

	public class LocationDetailResult
	{
		public LocationGeometry geometry { get; set; }
	}

	public class LocationGeometry
	{
		public LocObj location { get; set; }
	}

	public class LocObj
	{
		public double lat { get; set; }
		public double lng { get; set; }
	}

}
