﻿
namespace Veepe
{
	public class CustomResponses
	{
	}

	public class FBRegisterResponse
	{
		public int respuesta;
		public Usuario usuario;
	}

	// ********************************* usuario
	public class WsUserResponse
	{
		public string respuesta;
		public Usuario datos;
	}

	public class WsSingleOperationResponse
	{
		public string respuesta;
		public sor datos; //Operacion datos;
	}

	// ********************************* autos
	public class WsAutosResponse
	{
		public string respuesta;
		public Auto[] datos;
	}

	// ********************************* operador/operaciones
	public class WsEstacionamientosResponse
	{
		public string respuesta;
		public Estacionamiento[] datos;
	}

	public struct dsr
	{
		public EstadisticasOperadorViewModel estadisticas;
		public AutosOperacionViewModel[] operaciones;
	}
	public class WsParkingStatusResponse
	{
		public string respuesta;
		public dsr datos;
	}
}
