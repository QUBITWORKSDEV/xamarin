using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Veepe
{
	public class Usuario : INotifyPropertyChanged
	{
		bool _tieneReserva;
		//DateTime _horaReserva;
		public Reserva reserva;

		public event PropertyChangedEventHandler PropertyChanged;

		// ___________________________________________________ Atributos privados
		public ObservableCollection<Auto> _autos;
		public ObservableCollection<MetodoPago> _metodosPago;
		public ObservableCollection<PensionContratada> _pensionesContratadas;
		public ObservableCollection<PensionContratada[]> _pensionesCompartidasContratadas;

		// ___________________________________________________ Constantes públicas
		public static int ROL_USUARIO_FINAL = 8;
		public static int ROL_OPERADOR = 10;
		public static int ROL_JEFE_ESTACIONAMIENTO = 9;

		// ___________________________________________________ Atributos (y accesores)
		public int idUsuario { get; set; }
		public string veepeId { get; set; }
		public string nombre { get; set; }
		public string apellidoPaterno { get; set; }
		public string apellidoMaterno { get; set; }
		public string correo { get; set; }
		public string celular { get; set; }
		public int nip { get; set; }
		public int rol { get; set; }
		public int idEstacionamiento { get; set; }
		public string nombreEstacionamiento { get; set; }
		public int Tipo { get; set; }							// el tipo de usuario 1=autoregistrado con mail, 3=autoregistrado con fb

		public bool esTipoMail {
			get { return Tipo == 1; }
		}

		public string nombreSemicompleto
		{
			get
			{
				return nombre + " " + apellidoPaterno;
			}
		}

		public string nombreCompleto
		{
			get
			{
				return nombre + " " + apellidoPaterno + " " + apellidoMaterno;
			}
		}

		// ___________________________________________ AUTOS
		public Auto[] autos
		{
			set
			{
				_autos = new ObservableCollection<Auto>();
				for (int i = 0; i < value.Length; i++)
				{
					_autos.Add(value[i] as Auto);
				}
			}
		}

		// ___________________________________________ METODOS PAGO
		public MetodoPago[] MetodosPago
		{
			set
			{
				_metodosPago = new ObservableCollection<MetodoPago>();
				for (int i = 0; i < value.Length; i++)
				{
					_metodosPago.Add(value[i] as MetodoPago);
				}
			}
		}

		// ___________________________________________ PENSIONES
		/*public PensionContratada[] PensionesContratadas
		{
			set
			{
				_pensionesContratadas = new ObservableCollection<PensionContratada>();
				for (int i = 0; i < value.Length; i++)
				{
					_pensionesContratadas.Add(value[i] as PensionContratada);
				}
			}
		}*/
		public PensionContratadaResponse PensionesContratadas
		{
			set
			{
				// guardamos las pensiones normales
				if (value.normales != null)
				{
					_pensionesContratadas = new ObservableCollection<PensionContratada>();
					for (int i = 0; i < value.normales.Length; i++)
					{
						_pensionesContratadas.Add(value.normales[i] as PensionContratada);
					}
				}

				// guardamos las pensiones compartidas
				if (value.compartidas != null)
				{
					_pensionesCompartidasContratadas = new ObservableCollection<PensionContratada[]>();
					for (int i = 0; i < value.compartidas.Length; i++)
					{
						_pensionesCompartidasContratadas.Add(value.compartidas[i] as PensionContratada[]);
					}
				}
			}
		}


		// regresa true o false, dependiendo si tiene algún tipo de pensión en este estacionamiento. nota: no checa horario
		/*public bool tienePensionEnEstacionamiento(int idEstacionamiento)
		{
			bool res = false;

			for (int i = 0; i < _pensionesContratadas.Count; i++)
			{
				if (_pensionesContratadas[i].idEstacionamiento == idEstacionamiento)
					res = true;
			}
			for (int i = 0; i < _pensionesCompartidasContratadas.Count; i++)
			{
				if (_pensionesCompartidasContratadas[i].idEstacionamiento == idEstacionamiento)
					res = true;
			}

			return res;
		}*/


		// regresa una lista de las pensiones activas en el horario actual y dado un estacionamiento específico
		public PensionContratada[] getPensionesActivas(int idEstacionamiento=-1)
		{
			var res = new List<PensionContratada>();
			if (idEstacionamiento == -1)
				idEstacionamiento = this.idEstacionamiento;

			if (_pensionesContratadas != null)
			{
				for (int i = 0; i < _pensionesContratadas.Count; i++)
				{
					if (_pensionesContratadas[i].idEstacionamiento == idEstacionamiento && _pensionesContratadas[i].estaActiva())
						res.Add(_pensionesContratadas[i]);
				}
			}

			return res.ToArray();
		}

		public PensionContratada[][] getPensionesCompartidasActivas(int idEstacionamiento=-1)
		{
			var res = new List<PensionContratada[]>();
			if (idEstacionamiento == -1)
				idEstacionamiento = this.idEstacionamiento;

			if (_pensionesCompartidasContratadas != null)
			{
				for (int i = 0; i < _pensionesCompartidasContratadas.Count; i++)
				{
					for (int j = 0; j < _pensionesCompartidasContratadas[i].Length; j++)
					{
						if (_pensionesCompartidasContratadas[i][j].idEstacionamiento == idEstacionamiento && _pensionesCompartidasContratadas[i][j].estaActiva())
							res.Add(_pensionesCompartidasContratadas[i]);
					}
				}
			}

			return res.ToArray();
		}


		// ___________________________________________ RESERVAS
		public bool tieneReserva {
			get {
				return _tieneReserva;
			}

			set {
				_tieneReserva = value;
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("tieneReserva"));
				}
			}
		}

		/*public DateTime horaReserva
		{
			get {
				return reserva.Hora; // _horaReserva;
			}

			set {
				//_horaReserva = value;
				reserva.Hora = value;
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("horaReserva"));
				}
				
			}
		}*/

		public bool tieneOperacionActiva { get; set; }

		public bool tienePagosAtrasados { get; set; }


		// ___________________________________________________ Métodos
	}
}

