﻿using System;
namespace Veepe
{
	public class Reserva
	{
		public int IdEstacionamiento { get; set; }
		public string NombreEstacionamiento { get; set; }
		public DateTime Hora { get; set; }

		public Reserva()
		{
		}
	}
}
