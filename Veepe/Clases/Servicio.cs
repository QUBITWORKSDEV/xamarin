﻿using System;

namespace Veepe
{
	public class Servicio
	{
		// ___________________________________________________ Constantes públicas
		//public static int TIPO_SERVICIO_LAVADO = 1;

		// ___________________________________________________ Atributos privados

		// ___________________________________________________ Atributos (y accesores)
		//public int tipoServicio { get; set; }
		public int idServicio { set; get; }
		public string nombre { get; set; }
		public float monto { get; set; }
		public string observaciones { get; set; }
		public string icono { get; set; }

		// ___________________________________________________ Constructores
		public Servicio() { }

		public Servicio(int pIdServicio)
		{
			idServicio = pIdServicio;
			//tipoServicio = pTipo;
		}

		public Servicio(int pIdServicio, /*int pTipo,*/ float pMonto)
		{
			idServicio = pIdServicio;
			//tipoServicio = pTipo;
			monto = pMonto;
		}

		// ___________________________________________________ Métodos
	}
}

