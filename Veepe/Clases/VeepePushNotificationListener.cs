﻿using System;
using System.Threading.Tasks;
using FunkFramework.Net;
using Newtonsoft.Json.Linq;
using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using Xamarin.Forms;

namespace Veepe
{
	public class VeepePushNotificationListener : IPushNotificationListener
	{
		void IPushNotificationListener.OnMessage(JObject parameters, DeviceType deviceType)
		{
			if (Application.Current != null)
			{
				string s = "" + parameters[deviceType==DeviceType.iOS?"alert":"text"];
				if (s.Contains("expiró") && s.Contains("reserva"))
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						(Application.Current as App).MainPage.DisplayAlert("", "Tu reserva de estacionamiento expiró.", "Cerrar");
						(Application.Current as App).usuario.tieneReserva = false;
						(Application.Current as App).usuario.reserva = null;
					});
				}
				else if ((Application.Current as App).usuario.rol == Usuario.ROL_USUARIO_FINAL)
				{
					Task.Run(async () =>
					{
						await (Application.Current as App).updateHomePage();
					});
				}

				if ((Application.Current as App).usuario.rol != Usuario.ROL_USUARIO_FINAL)
					(Application.Current as App).playJingle();
			}
		}

		void IPushNotificationListener.OnRegistered(string Token, DeviceType deviceType)
		{
			if ((Application.Current as App).usuario != null)
			{
				string plat = (deviceType == DeviceType.iOS ? "ios" : "android");
			
				// determinamos si es estacionamiento o no
				if ((Application.Current as App).usuario.idEstacionamiento == -1)
				{
					// es un usuario
					Task.Run(async () =>
					{
						WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "registerPushToken", "{\"uid\":\"" + (Application.Current as App).usuario.idUsuario + "\",\"token\":\"" + Token + "\",\"plat\":\"" + plat + "\"}");

						if (items == null)
						{
							// no se pudo guardar el token -> guardar registro de error
							Reporter.ReportError("No se pudo guardar en el sistema el push token -" + Token + "- de uid=" + (Application.Current as App).usuario.idUsuario);
						}
					});
				}
				else
				{
					// es un operador
					Task.Run(async () =>
					{
						WsResponse<bool> items = await WsConsumer.Consume<bool>("data", "registerParkingPushToken", "{\"idEstacionamiento\":\"" + (Application.Current as App).usuario.idEstacionamiento + "\",\"token\":\"" + Token + "\",\"plat\":\"" + plat + "\"}");

						if (items == null)
						{
							// no se pudo guardar el token -> guardar registro de error
							Reporter.ReportError("No se pudo guardar en el sistema el push token de estacionamiento -" + Token + "- de uid=" + (Application.Current as App).usuario.idUsuario);
						}
					});
				}
	
			}
		}

		void IPushNotificationListener.OnUnregistered(DeviceType deviceType)
		{
		}

		void IPushNotificationListener.OnError(string message, DeviceType deviceType)
		{
		}

		bool IPushNotificationListener.ShouldShowNotification()
		{
			return true;
		}

	} // fin de la clase VeepePushNotificationListener
}
