﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;

namespace Veepe
{
	public class Operacion
	{
		// ___________________________________________________ Atributos privados
		public int idEstacionamiento { set; get; }
		public int idOperacion { set; get; }
		public int status { get; set; }

		public bool _dejaLentes { get; set; }
		public bool _dejaLaptop { get; set; }
		public bool _dejaTablet { get; set; }
		public bool _dejaCelular { get; set; }

		public string _fotoIzquierda { get; set; }
		public string _fotoDelantera { get; set; }
		public string _fotoDerecha { get; set; }
		public string _fotoTrasera { get; set; }

		// ___________________________________________________ Atributos (y accesores)
		public DateTime fechaIngreso { get; set; }
		public DateTime HoraEstimadaEntrega { get; set; }
		public bool esReserva { get; set; }
		public float tiempo { get; set; }
		public float tarifa { get; set; }
		public int idOperador { get; set; }
		public Servicio[] servicios { get; set; }
		public ImageSource fotoIzquierda { get; set; }
		public ImageSource fotoFrente { get; set; }
		public ImageSource fotoDerecha { get; set; }
		public ImageSource fotoTrasera { get; set; }
		public int dejaLentes { get { return _dejaLentes ? 1 : 0; } set { _dejaLentes = value == 1 ? true : false; } }
		public int dejaLaptop { get { return _dejaLaptop ? 1 : 0; } set { _dejaLaptop = value == 1 ? true : false; } }
		public int dejaTablet { get { return _dejaTablet ? 1 : 0; } set { _dejaTablet = value == 1 ? true : false; } }
		public int dejaCelular { get { return _dejaCelular ? 1 : 0; } set { _dejaCelular = value == 1 ? true : false; } }
		public string otrosObjetos { get; set; }
		public string comentarios { get; set; }
		public int tiempoAproximadoEntrega { get; set; }
		public float monto { get; set; }
		public int Rating { get; set; }

		public string ReadableFechaIngreso {
			get {
				return fechaIngreso.ToString("dd-MMM-yyyy HH:mm");
			}
		}

		public string readableMonto {
			get {
				return monto.ToString("F2");
			}
		}

		// ___________________________________________________ Constructores
		public Operacion() { }

		public Operacion(int pIdOperacion, int pIdEstacionamiento, bool pEsReserva)
		{
			idOperacion = pIdOperacion;
			idEstacionamiento = pIdEstacionamiento;
			esReserva = pEsReserva;
		}

		// ___________________________________________________ Métodos
		private bool caeDentro(int hora, int tipo)
		{
			bool res;

			if (tipo == 1)	// de 8 a 20
			{
				res = (hora >= 8 && hora < 20);
			}
			else if (tipo == 3)	// de 20 a 8
			{
				res = (hora >= 20 || hora < 8);
			}
			else
				res = true;

			return res;
		}

		public float getCostoAproximado( float tarifaHora, ObservableCollection<PensionContratada> pensionesContratadas = null, ObservableCollection<PensionContratada[]> pensionesCompartidasContratadas = null )
		{
			float res = 0f;

			DateTime now = DateTime.Now;
			TimeSpan t = DateTime.Now - fechaIngreso;

			// calcular el costo tomando en cuenta las pensiones
			if ( (pensionesContratadas != null && pensionesContratadas.Count > 0) || (pensionesCompartidasContratadas != null && pensionesCompartidasContratadas.Count > 0) )
			{
				int suma = 0;	// la suma mágica

				for (int i = 0; i < pensionesContratadas?.Count; i++)
				{
					if (pensionesContratadas[i].idEstacionamiento == idEstacionamiento /*(Application.Current as App).usuario.idEstacionamiento*/)
					{
						suma += pensionesContratadas[i].horario;
					}
				}
				for (int i = 0; i < pensionesCompartidasContratadas?.Count; i++)
				{
					for (int j = 0; j < pensionesCompartidasContratadas[i].Length; j++)
					{
						if (pensionesCompartidasContratadas[i][j].idEstacionamiento == idEstacionamiento /*(Application.Current as App).usuario.idEstacionamiento*/)
						{
							suma += pensionesCompartidasContratadas[i][j].horario;
						}
					}
				}

				if (suma >= 4)
				{
					// la combinación de pensiones que tiene le cubren todo el dia, ergo no se cobra nada
					return 0f;
				}
				else
				{
					/*
					 * Breve explicación del método usado para calcular el costo cuando hay pensión
					 *
					 * Se tiene un arreglo con las 24 hrs del día.
					 * 1. Se recorre el tiempo que ha estado el usuario en el estacionamiento.
					 * En cada hora se suman los minutos a la hora correspondiente de modo que el arreglo 
					 * lleva una suma de los minutos que ha estado el usuario. La primera hora y la última 
					 * son las únicas que no son 60.
					 * 2. Luego se recorren todas las pensiones que tiene el usuario en este estacionamiento 
					 * y para cada una de ellas se recorren las horas que cubre; cada una de esas horas se 
					 * pone en cero en el arreglo de modo que esas horas no se cobren.
					 * 3. Finalmente se recorre el arreglo y se van sumando las horas y se calculan las 
					 * fracciones, que nos dan el total que se debe pagar.
					 *
					 **/

					int[] acumuladorHoras = new int[24];
					DateTime contador = fechaIngreso;
					bool falta = true;
					int seguridad = 0;
					bool esElPrimero = true;

					// paso 1: contabilizamos el total bruto
					while (falta && seguridad < 1000)
					{
						if (contador == fechaIngreso)	// es el primero -> tomamos sólo los minutos
						{
							acumuladorHoras[contador.Hour] += (60-contador.Minute);
							if (contador.Year == now.Year && contador.Month == now.Month && contador.Day == now.Day && contador.Hour == now.Hour)
								falta = false;
						}
						else if (contador.Year==now.Year && contador.Month==now.Month && contador.Day==now.Day && contador.Hour==now.Hour)	// es el último -> tomamos sólo los minutos
						{
							if (!esElPrimero)
								acumuladorHoras[contador.Hour] += now.Minute;
							falta = false;
						}
						else
						{
							acumuladorHoras[contador.Hour] += 60;	// es una hora intermedia -> contar completa
						}

						esElPrimero = false;
						seguridad++;
						contador = contador.AddHours(1);
					}
					Debug.WriteLine("**->" + seguridad);

					// paso 2: ahora quitamos las horas cubiertas por las pensiones
					for (int i = 0; i < pensionesContratadas?.Count; i++)
					{
						if (pensionesContratadas[i].idEstacionamiento == idEstacionamiento)
						{
							if (pensionesContratadas[i].horario == 1)	// es de día
							{
								for (int m = 7; m <= 18; m++)
									acumuladorHoras[m] = 0;
							}
							else if (pensionesContratadas[i].horario == 3)	// es de noche
							{
								for (int m = 19; m <= 23; m++) acumuladorHoras[m] = 0;
								for (int m = 0; m <= 6; m++) acumuladorHoras[m] = 0;
							}
						}
					}
					for (int i = 0; i < pensionesCompartidasContratadas?.Count; i++)
					{
						for (int j = 0; j < pensionesCompartidasContratadas[i].Length; j++)
						{
							if (pensionesCompartidasContratadas[i][j].idEstacionamiento == idEstacionamiento)
							{
								if (pensionesCompartidasContratadas[i][j].horario == 1) // es de día
								{
									for (int m = 7; m <= 18; m++)
										acumuladorHoras[m] = 0;
								}
								else if (pensionesCompartidasContratadas[i][j].horario == 3)    // es de noche
								{
									for (int m = 19; m <= 23; m++) acumuladorHoras[m] = 0;
									for (int m = 0; m <= 6; m++) acumuladorHoras[m] = 0;
								}
							}
						}
					}

					// paso 3: hacemos la suma total
					int total = 0;
					for (int i = 0; i < 24; i++)
					{
						total += acumuladorHoras[i];
					}
					int hrs = (total / 60);
					int min = total - (hrs*60);

					System.Diagnostics.Debug.WriteLine(hrs + ":" + min);

					if (hrs < 1)
						if (min == 0)
							res = 0;
						else
							res = 1 * tarifaHora;
					else
					{
						float fraccion = min<15? .25f : ( min<30? .5f : ( min<45? .75f : 1f ) );
						res = (hrs + fraccion) * tarifaHora;
					}
				}
			}
			else
			{
				// no tiene pensiones -> cálculo normal
				float fraccion = 0f;
				if (t.Minutes < 15)
					fraccion = 0.25f;
				else if (t.Minutes < 30)
					fraccion = 0.5f;
				else if (t.Minutes < 45)
					fraccion = 0.75f;
				else
					fraccion = 1f;

				if (t.TotalHours < 1)
					res = 1 * tarifaHora;	// se cobra la primera hora, sí o sí
				else
					res = ((int)t.TotalHours + fraccion) * tarifaHora;
			}

			return res;
		}
	}
}

