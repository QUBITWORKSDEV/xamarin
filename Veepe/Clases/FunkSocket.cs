﻿using System;
using System.Text;
using System.Threading.Tasks;
using Sockets.Plugin;
using Xamarin.Forms;

namespace Veepe
{
	public class FunkSocket
	{
		TcpSocketClient client;
		bool isListening = false;
		//bool isComunicating = false;

		public FunkSocket()
		{
			client = new TcpSocketClient();
		}

		public async Task connect()
		{
			await client.ConnectAsync(GlobalSettings.socketIP, GlobalSettings.socketPort);

			isListening = true;
			await startListening();

			//await socket.WriteStream.WriteAsync(Encoding.UTF8.GetBytes("hola"), 0, 4);
		}

		public async Task disconnect()
		{
			isListening = false;
			if (client != null)
				await client.DisconnectAsync();
		}

		private async Task startListening()
		{
			while (isListening)
			{
				byte[] typeBuffer = new byte[3];
				await client.ReadStream.ReadAsync(typeBuffer, 0, 3);

				// procesamos la lectura
				switch (System.Text.Encoding.UTF8.GetString(typeBuffer, 0, 3))
				{
					case "900":
						System.Diagnostics.Debug.WriteLine(System.Text.Encoding.UTF8.GetString(typeBuffer, 0, 3));
						break;
				}
			}
		} // fin de startListening

		public async Task broadcastTest()
		{
			if (client != null)
			{
				await client.WriteStream.WriteAsync(Encoding.UTF8.GetBytes("devtest"), 0, 7);
			}
		}


	}
}

/*
 * --------------------------
 * byte typeBuffer [3]
 * tres dígitos.
 * -------------------------- 
 * dígito 1 - categoria
 * 		0 ->	operación
 * 		1 ->	veepeid
 * 		2 ->	perfil
 * 		3 ->	formas de pago
 * 		4 ->	mis autos
 * 		5 ->	pensiones
 * 		6 ->	promociones
 * 		7 ->	nil
 * 		8 ->	nil
 * 		9 ->	callbacks y dev
 * 
 * dígitos 2 y 2 - comando/operación
 * 
 * 
 * 
 * 
 * 
 * 
 * */