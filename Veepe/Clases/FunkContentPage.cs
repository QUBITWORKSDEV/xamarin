﻿using Xamarin.Forms;
using System.ComponentModel;
using System;

namespace Veepe
{
	public partial class FunkContentPage : ContentPage, INotifyPropertyChanged
	{
		protected bool _isOffline = false;
		protected bool _canEdit;

		public new event PropertyChangedEventHandler PropertyChanged;

		public bool canEdit
		{
			get { return _canEdit; }
			set
			{
				_canEdit = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("canEdit"));
			}
		}

		public bool isOffline
		{
			get { return _isOffline; }
			set
			{
				_isOffline = value;
				if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("isOffline"));
			}
		}

		public FunkContentPage()
		{
			canEdit = true;
		}

		protected void checkConnectivity()
		{
			// verificamos si hay red
			if ( (Application.Current as App)!=null )
				isOffline = !(Application.Current as App).cm.HasConnectivity();

			// iniciamos el temporizador
			Device.StartTimer(TimeSpan.FromSeconds(3), () =>
			{
				checkConnectivity();
				return false;
			});
		}

	}
}

