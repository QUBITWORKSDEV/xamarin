﻿using System;
namespace Veepe
{
	public interface IImageResizer
	{
		byte[] ResizeImage(byte[] imageData, float width, float height);
	}
}

