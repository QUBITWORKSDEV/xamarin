﻿using Xamarin.Forms;

namespace Veepe
{
	public class sor : BindableObject
	{
		public static readonly BindableProperty _operacion = BindableProperty.Create("operacion", typeof(Operacion), typeof(sor));
		public static readonly BindableProperty _auto = BindableProperty.Create("auto", typeof(Auto), typeof(sor));
		public static readonly BindableProperty _estacionamiento = BindableProperty.Create("estacionamiento", typeof(Estacionamiento), typeof(sor));

		public Operacion operacion
		{
			get
			{
				return (Operacion)GetValue(_operacion);
			}

			set
			{
				SetValue(_operacion, value);
			}
		}

		public Auto auto
		{
			get
			{
				return (Auto)GetValue(_auto);
			}

			set
			{
				SetValue(_auto, value);
			}
		}

		public Estacionamiento estacionamiento
		{
			get
			{
				return (Estacionamiento)GetValue(_estacionamiento);
			}

			set
			{
				SetValue(_estacionamiento, value);
			}
		}

	}
}
