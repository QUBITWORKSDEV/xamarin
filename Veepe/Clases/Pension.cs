﻿
namespace Veepe
{	
	public class Pension
	{
		// ___________________________________________________ Constantes públicas

		// ___________________________________________________ Atributos privados

		// ___________________________________________________ Atributos (y accesores)
		public int idPension { set; get; }
		public int periodo { get; set; }
		public string periodoReadable {
			get {
				return periodo + " Mes" + (periodo>1?"es":"");
			}
		}

		public float costoDia { get; set; }
		public float costoNoche { get; set; }
		public float costoTodoDia { get; set; }

		public float costoDiaCompartida { get; set; }
		public float costoNocheCompartida { get; set; }
		public float costoTodoDiaCompartida { get; set; }

		// ___________________________________________________ Constructores

		// ___________________________________________________ Métodos
	}
}
