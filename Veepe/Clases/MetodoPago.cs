﻿using System;
using System.ComponentModel;

namespace Veepe
{
	public class MetodoPago : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		// ___________________________________________________ Atributos privados

		// ___________________________________________________ Atributos (y accesores)
		public int idMetodoPago { get; set; }
		public string NombreCuenta { get; set; }
		public string NombreTarjeta { get; set; }
		public string Tipo { get; set; }
		public string Pseudonumero { get; set; }
		public string Token { get; set; }

		// ___________________________________________________ Constructores
		public MetodoPago() { }

		/*public MetodoPago(string pMarca, string pSubmarca, int pModelo, string pPlaca, string pColor)
		{
			Marca = pMarca;
			Submarca = pSubmarca;
			Modelo = pModelo;
			Placa = pPlaca;
			Color = pColor;
		}*/

		// ___________________________________________________ Métodos
	}
}
