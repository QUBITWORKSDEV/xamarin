using System;

namespace Veepe
{
	public class PensionContratada
	{
		public int idPension { get; set; }					// id de la metaclase de pensión contratada
		public int idPensionado { get; set; }				// id de la instancia de la pensión asociada al usuario (no es el id del usuario)

		public int idEstacionamiento { get; set; }			// el id del estacionamiento donde está contratada
		public string nombreEstacionamiento { get; set; }	// nombre del estacionamiento

		public int horario { get; set; }					// horario contratado 1=dia, 3=noche, 5=tododia
		public int tipo { get; set; }						// 1=normal, 3=compartida

		public float costo { get; set; }
		public int idMetodoPago { get; set; }				// el dia del método de pago

		public string idCompartida { get; set; }			   // el dia del método de pago

		public int status { get; set; }					 //el status de la pensión

		public DateTime fechaContratacion { get; set; }		// la fecha en que fue contratada


		/*public string imagenTipoPension{
			get
			{
				if (tipo == 1)
				{
					return "iconos_menu_pension.png";
				}
				else if (tipo == 3) {
					return "ico_servicios_pensioncompartida_a.png";
				}
				else
				{
					return "--";
				}
			}
		}*/


		public string TipoReadable {
			get {
				if (horario == 1)
					return "día";
				else if (horario == 3)
					return "noche";
				else if (horario == 5)
					return "día completo";
				else
					return "--";
			}
		}

		// regresa tru o false, dependiendo de si esta pensión es del hroario que corresponde actualmente
		public bool estaActiva()
		{
			bool res = false;
			DateTime ahora = DateTime.Now;

			if (horario == 1 && ahora.Hour >= 7 && ahora.Hour < 19)
				res = true;
			else if (horario == 3 && (ahora.Hour >= 19 || ahora.Hour < 7))
				res = true;
			else if (horario == 5)
				res = true;

			return res;
		}

	} // fin de la clase PensionContratada









	public class PensionContratadaResponse
	{
		public PensionContratada[] normales { get; set; }
		public PensionContratada[][] compartidas { get; set; }

		//public string getNombreCompartida(int pos)
		//{
		//	string nombre = "";

		//	for (int i = 0; i < compartidas[pos].Length; i++)
		//	{
		//		nombre += compartidas[pos][i].nombreEstacionamiento;


		//		nombre += ", ";
		//	}

		//	return nombre;		
		//}

	}


	public class PensionContratadaCompartida
	{

		public PensionContratada[] pensionados { get; set; }

		public string idCompartida { get; set; }

		public string nombreCompartida {

			get {

				string nombre = "";

				for (int i = 0; i < pensionados.Length; i++)
				{

					if(i < pensionados.Length - 1)
					{
						nombre += pensionados[i].nombreEstacionamiento + ", ";
					}
					else
					{
						nombre += pensionados[i].nombreEstacionamiento;
					}

					idCompartida = pensionados[i].idCompartida;
				}

				return nombre;
			   
			}
		}


		public int tipo {

			get {

				return 3;
			}
		}

	} 

}
