﻿namespace Veepe
{
	public class Estacionamiento
	{
		public int idEstacionamiento { get; set; }
		public string Nombre { get; set; }
		public string Direccion { get; set; }
		public string Zona { get; set; }
		public int Capacidad { get; set; }
		public int PensionesContratadas { get; set; }
		public int PensionesCompartidas { get; set; }
		public string HorarioServicio { get; set; }
		public string Terminos { get; set; }
		public string Operadora { get; set; }

		public double Latitud { get; set; }
		public double Longitud { get; set; }

		public float TarifaHora { get; set; }
		public float TarifaFraccion { get; set; }
		public float TarifaBoletoPerdido { get; set; }
		public float TarifaPernocta { get; set; }

		public int idEmpresa { get; set; }

		public Servicio[] Servicios { get; set; }

		public Pension[] Pensiones { get; set; }

		public bool TienePension {
			get {
				return Pensiones.Length > 0;
			}
		}

		public bool TienePensionCompartida
		{
			get
			{
				bool res = false;

				for (int i = 0; i < Servicios.Length; i++)
				{
					if (Servicios[i].idServicio == 2)
					{
						res = true;
					}
				}

				return res;
			}
		}

	}
}

